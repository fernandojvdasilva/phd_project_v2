#!/usr/local/bin/python2.7
# encoding: utf-8
'''
tools.gen_user_tickets -- Generates random ticket numbers for the users

tools.gen_user_tickets is a script that generates a csv file with ticket numbers for the users.

@author:     fernando

@copyright:  2015 Fernando J. V. da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import pandas as pd
import numpy as np

__all__ = []
__version__ = 0.1
__date__ = '2017-02-05'
__updated__ = '2017-02-05'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  All rights reserved.

  Licensed under the GPL License  

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument(dest="path", help="path to csv file downloaded from Google App Engine", metavar="path")

        # Process arguments
        args = parser.parse_args()        

        path = args.path
        
        users_data = pd.read_csv(path, engine='c')
        ticket_numbers = [i for i in range(1, users_data['num_tickets'].sum()+1)]     
        
        np.random.shuffle(ticket_numbers)
        
        print(ticket_numbers)
        
        tickets_file = open('user_tickets.csv', 'w')
        tickets_file.write('ticket_number;user_code;user_email\n')
        
        i = 0
        for index, row in users_data.iterrows():
            j = 0
            for j in range(row['num_tickets']):
                atchar_index = row['email'].find('@')
                user_code = row['email'][:atchar_index-3] + '*'*3 + '@'
                user_code += '<domínio omitido>'
                tickets_file.write('%d;%s;%s\n' % (ticket_numbers[i+j], user_code, row['email']))
            i += row['num_tickets']

        tickets_file.close()
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.gen_user_tickets_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())