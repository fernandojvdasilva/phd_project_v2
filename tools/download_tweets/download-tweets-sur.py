# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

SUR_HASHTAGS = [HASHTAG_STR + "surpreso", # Surprise (Surpreso) \
                     HASHTAG_STR + "surpresa", \
                     HASHTAG_STR + "surpreendido", \
                     HASHTAG_STR + "surpreendida", \
                     HASHTAG_STR + "espantado",\
                     HASHTAG_STR + "espantada",\
                     HASHTAG_STR + "perplexo",\
                     HASHTAG_STR + "perplexa",\
                     HASHTAG_STR + "perplexidade",\
                     HASHTAG_STR + "quemdiria"
                ]

class MainPage(webapp2.RequestHandler):

    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(SUR_HASHTAGS, 'SUR')


app = webapp2.WSGIApplication([
  ('/download-tweets-sur', MainPage)
], debug=True)
