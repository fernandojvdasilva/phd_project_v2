# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

FEAR_HASHTAGS = [HASHTAG_STR + "medo", # Fear (Medo) \
                     HASHTAG_STR + "commedo", \
                     HASHTAG_STR + "empanico",\
                     HASHTAG_STR + "aterrorizado",\
                     HASHTAG_STR + "aterrorizada",\
                     HASHTAG_STR + "terrível",\
                     HASHTAG_STR + "terrivel",\
                     HASHTAG_STR + "apavorado",\
                     HASHTAG_STR + "apavorada",\
                     HASHTAG_STR + "espanto",\
                     HASHTAG_STR + "susto",\
                     HASHTAG_STR + "assustado",\
                     HASHTAG_STR + "assustada",\
                     HASHTAG_STR + "receio",\
                     HASHTAG_STR + "receoso",\
                     HASHTAG_STR + "receosa"
                     ]

class MainPage(webapp2.RequestHandler):

    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(FEAR_HASHTAGS, 'FEA')


app = webapp2.WSGIApplication([
  ('/download-tweets-fear', MainPage)
], debug=True)
