# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date, timedelta
import datetime
import tweepy
import webapp2
import time
from google.appengine.ext import ndb

from upload_tweets import TweetCrowd
from upload_annotations import TweetAnnotationCrowd

class MainPage(webapp2.RequestHandler):

	def post(self):
		pass

	@ndb.transactional
	def set_num_annot(self, obj, num):
		obj.num_annot = num
		obj.put()


	def get(self):	
		
	
		self.response.out.write('Querying annotations...<br>')
		
		qry = TweetCrowd.query().filter(TweetCrowd.num_annot > 0)
		
		tweets = qry.fetch(1000000)
		
		self.response.out.write('Len(tweet_annots) = %d<br>' % len(tweets))		
		
		for t in tweets:
			qry2 = TweetAnnotationCrowd.query().filter(TweetAnnotationCrowd.tweet_id == t.tweet_id)
			real_count = qry2.count()
			
			annotations = qry2.fetch(1000)
			annotators_ids = []
			for annot in annotations:
				if not annot.annotator_id in annotators_ids:
					annotators_ids.append(annot.annotator_id)
				else:
					self.response.out.write('Repeated annotation tweet id %d, annotator %d<br>' % (annot.tweet_id, annot.annotator_id))
					annot.key.delete()
					real_count -= 1
			
			if real_count != t.num_annot:
				self.response.out.write('Tweet %d wrong:%d, correct:%d<br>' % (t.tweet_id, t.num_annot, real_count))
				self.set_num_annot(t, real_count)				
		
		
app = webapp2.WSGIApplication([
  ('/fix-num-annot', MainPage)
], debug=True)		
