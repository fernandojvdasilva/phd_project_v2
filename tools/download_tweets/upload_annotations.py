from google.appengine.ext import ndb

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

import json
from json.encoder import JSONEncoder

import sys
import os

sys.path.append(os.getcwd())

from upload_tweets import *
from add_user import UserAnnotator 

class TweetAnnotationCrowd(ndb.Model):           
    tweet_id = ndb.IntegerProperty()
    annotator_id = ndb.IntegerProperty()    
    joy_vs_sadness = ndb.StringProperty()
    trust_vs_disgust = ndb.StringProperty()    
    surprise_vs_antecip = ndb.StringProperty()
    anger_vs_fear = ndb.StringProperty()
    created_date = ndb.DateTimeProperty(auto_now_add=True)

class AnnotationMessage(messages.Message):
    filename = messages.StringField(1)
    tweet_id = messages.IntegerField(2, required=True)
    annotator_id = messages.IntegerField(3, required=True)
    annotation = messages.StringField(4)

class AnnotationResultResponse(messages.Message):
    result_msg = messages.StringField(1)
    num_tickets = messages.IntegerField(2, required=True)
    next_ticket_count = messages.IntegerField(3, required=True)


@endpoints.api(name='tweetsAnnot', version='1')
class TweetsUploadAnnotations(remote.Service):

    UPLOAD_ANNOTATION_METHOD_RESOURCE = endpoints.ResourceContainer(AnnotationMessage)

    @ndb.transactional
    def increment_num_annot(self, obj):        
        obj.num_annot += 1
        obj.put()
        
    @ndb.transactional
    def decrement_num_alloc(self, obj):
        obj.num_alloc -= 1
        obj.put()

    @ndb.transactional
    def register_ticket(self, obj):
        obj.next_ticket_count -= 1
        if obj.next_ticket_count == 0:
            obj.num_tickets += 1
            obj.next_ticket_count = 20
        obj.put()


    @endpoints.method(UPLOAD_ANNOTATION_METHOD_RESOURCE, AnnotationResultResponse,
                      path='upload_annotation', http_method='POST',
                      name='upload.annotation')
    def upload_annotation(self, request):
        
        try:            
            annotation = json.loads(request.annotation)
        except:
            # if it isn't a correct json object, then we reject it.
            return AnnotationResultResponse(result_msg="OK")
        
        if len([k for k in ['joy_vs_sadness', 'trust_vs_disgust', \
                        'surprise_vs_antecip', 'anger_vs_fear'] if k in annotation.keys()]) < 4:
            return AnnotationResultResponse(result_msg="OK")
        
        # Retrieve and increment user's number of tickets
        qry3 = UserAnnotator.query().filter(UserAnnotator.user_id == request.annotator_id)
        obj = qry3.fetch(1)
        
        ticket_count = 20
        try:             
            self.register_ticket(obj[0])           
            ticket_count = obj[0].next_ticket_count
            num_tickets = obj[0].num_tickets 
        except:
            return AnnotationResultResponse(result_msg="ERROR:User unknown",\
                                            num_tickets=0, next_ticket_count=20)
        
        qry2 = TweetAnnotationCrowd.query().filter(ndb.AND(TweetAnnotationCrowd.tweet_id == request.tweet_id,
                                                      TweetAnnotationCrowd.annotator_id == request.annotator_id))
        
        is_allocated_annotation = False
        if qry2.count() == 1:
            is_allocated_annotation = True
            tweet_annotation = qry2.fetch(1)[0]
            tweet_annotation.joy_vs_sadness = annotation['joy_vs_sadness']
            tweet_annotation.trust_vs_disgust = annotation['trust_vs_disgust']
            tweet_annotation.anger_vs_fear = annotation['anger_vs_fear']
            tweet_annotation.surprise_vs_antecip = annotation['surprise_vs_antecip']
        else:                
            tweet_annotation = TweetAnnotationCrowd(tweet_id=request.tweet_id,
                                            annotator_id=request.annotator_id,
                                            joy_vs_sadness = annotation['joy_vs_sadness'],
                                            trust_vs_disgust = annotation['trust_vs_disgust'],
                                            surprise_vs_antecip = annotation['surprise_vs_antecip'],
                                            anger_vs_fear = annotation['anger_vs_fear'])
        
        qry = TweetCrowd.query().filter(TweetCrowd.tweet_id == request.tweet_id)
        obj = qry.fetch(1)
        
        try:
            self.increment_num_annot(obj[0])
        except:
            return AnnotationResultResponse(result_msg=("ERROR: No tweet in database whose id is %d" % request.tweet_id),\
                                            num_tickets=0, next_ticket_count=20)
        
        if is_allocated_annotation:
            self.decrement_num_alloc(obj[0])        
        
        try:
            tweet_annotation.put()
        except:
            return AnnotationResultResponse(result_msg="ERROR:Storage", \
                                            num_tickets=0, next_ticket_count=20)



        return AnnotationResultResponse(result_msg="OK",\
                                        num_tickets=num_tickets, next_ticket_count=ticket_count)



#APPLICATION = endpoints.api_server([TweetsUploadAnnotations, TweetsUpload, GetTweetAnnot])
