# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

ANGER_HASHTAGS = [HASHTAG_STR + "raiva", # Anger (Raiva) \
                     HASHTAG_STR + "comraiva", \
                     HASHTAG_STR + "furia", \
                     HASHTAG_STR + "fúria", \
                     HASHTAG_STR + "furioso",\
                     HASHTAG_STR + "furiosa",\
                     HASHTAG_STR + "irritado",\
                     HASHTAG_STR + "irritada",\
                     HASHTAG_STR + "irritação",\
                     HASHTAG_STR + "aborrecido",\
                     HASHTAG_STR + "aborrecida",\
                     HASHTAG_STR + "incomodo",\
                     HASHTAG_STR + "incômodo",\
                     HASHTAG_STR + "incomodado",\
                     HASHTAG_STR + "incomodada"
                     ]

class MainPage(webapp2.RequestHandler):
    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(ANGER_HASHTAGS, 'ANG')


app = webapp2.WSGIApplication([
  ('/download-tweets-anger', MainPage)
], debug=True)
