echo "Tension:"
grep -c 'tenso\|aflição\|aflito\|agustiado\|angústia\|ansioso\|ansiedade\|apreensivo\|apreensão\|estressado\|estresse\|stress\|stressado\|inqueto\|inquietação\|inquietude\|medo\|nervoso\|nervosismo\|preocupado\|preocupação\|tranquilo\|ameno\|calma\|calmaria\|equilíbrio\|equilibrado\|harmonia\|paz\|quieto\|quietação\|quietude\|sereno\|serenidade\|silêncio\|silencioso\|sossegado\|sossego\|suavidade\|suave\|certeza\|confiança\|crença\|descanso\|despreocupado\|despreocupação\|esperança\|esperançoso\|otimismo\|otimista\|pacato\|repouso\|estável\|estabilidade\|normalidade\|regularidade\|seguro\|segurança\|nervoso\|irritação\|irritado\|tumultuado\|impaciente\|apressado\|precipitado\|irritado\|agitado\|frenético\|inquieto\|ancioso' tweets_db_20140617.csv

echo "Depression:"
grep -c 'triste\|aborrecido\|desgostoso\|melancólico\|pesaroso\|horrível\|nebuloso\|obscuro\|sombrio\|aflito\|angustiante\|lamentável\|lastimável\|desencorajado\|só\|solitário\|sozinho\|abatido\|deprimido\|desanimado\|infeliz' tweets_db_20140617.csv

echo "Confusion:"
grep -c 'confuso\|confusão\|anarquia\|desalinhado\|desarrumado\|desordem\|caos\|misturado\|salada\|trapalhada\|ambíguo\|ambiguidade\|desorientado\|dúvida\|duvidoso\|enganado\|engano\|erro\|errado\|imprecisão\|impreciso\|incerto\|indefinido\|hesitar\|hesitação\|inibido\|inibição\|perplexo\|perplexidade\|perturbado\|perturbação\|desnorteado\|indeciso\|indecisão\|instável\|instabilidade\|bambo\|abalado\|flácido\|frouxo\|incerto\|oscilante\|oscilação\|vacilante\|inseguro\|competente\|apto\|bom\|capacitado\|capaz\|eficiente\|entendido\|habilidoso\|hábil\|idôneo\|inteligente\|apropriado\|oportuno\|legítimo\|eficaz' tweets_db_20140617.csv

echo "Vigor:"
grep -c 'animado\|bem disposto\|contente\|divertido\|entusiasmado\|entusiasmo\|feliz\|felicidade\|vibrante\|vivaz\|dinâmico\|confiante\|esperançoso\|esperança\|enérgico\|alegre\|cheio de disposição' tweets_db_20140617.csv

echo "Fatigue:"
grep -c 'exausto\|esgotado\|fatigado\|sem energia\|cansado\|estourado' tweets_db_20140617.csv

echo "Hostility:"
grep -c 'irritado\|stressado\|estressado\|rabugento\|raivoso\|raiva\|indignado\|indignação\|mal humorado\|rabujento\|aborrecido\|furioso' tweets_db_20140617.csv

echo "EMOTICONS:"

echo "Depression:"
grep -c ':-(\|:(\|=(\|:''(\|;(\|;-(' tweets_db_20140617.csv

echo "Confusion:"
grep -c ':-o\|:o\|=o\|:-O\|:O\|=O\|:-S\|:S\|:-s\|:s\|=S\|=s' tweets_db_20140617.csv

echo "Vigor:"
grep -c ':-)\|:)\|=)\|:-D\|:D\|=D' tweets_db_20140617.csv

echo "Hostility:"
grep -c ':-@\|:@\|=@' tweets_db_20140617.csv

#echo "Num Capitalized Words:"
#grep -c -w '[A-Z]\{2,30\}' tweets_db_20140617.csv



