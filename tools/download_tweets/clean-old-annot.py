# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date, timedelta
import datetime
import tweepy
import webapp2
import time
from google.appengine.ext import ndb

from upload_annotations import TweetAnnotationCrowd
from upload_tweets import TweetCrowd


class MainPage(webapp2.RequestHandler):

	def post(self):
		pass

	@ndb.transactional
	def decrement_num_alloc(self, obj):
		obj.num_alloc -= 1
		obj.put()


	def get(self):	
		
		last_hour=datetime.datetime.now() - timedelta(seconds=360)
		
		self.response.out.write('Querying annotations...<br>')
		#qry = TweetAnnotationCrowd.query().filter(TweetAnnotationCrowd.created_date <= last_hour)
		qry = TweetAnnotationCrowd.query().filter(TweetAnnotationCrowd.joy_vs_sadness != 'JOY', \
                                                  TweetAnnotationCrowd.joy_vs_sadness != 'SAD', \
                                                  TweetAnnotationCrowd.joy_vs_sadness !='neutral', \
                                                  TweetAnnotationCrowd.joy_vs_sadness !='dontknow')
		
		
		#qry = TweetAnnotationCrowd.query().filter(TweetAnnotationCrowd.created_date <= yesterday)
		
		tweet_annots = qry.fetch(1000000)
		
		self.response.out.write('Len(tweet_annots) = %d<br>' % len(tweet_annots))		
		
		annots_to_delete = []
		for t in tweet_annots:     		
			if t.created_date <= last_hour:
				self.response.out.write('t.tweet_id = %s<br>' % t.tweet_id)
				self.response.out.write('t.joy_vs_sadness = %s<br>' % t.joy_vs_sadness)
				self.response.out.write('t.created_date = %s<br>' % str(t.created_date))
				self.response.out.write('============== <br>')
				
				qry2 = TweetCrowd.query().filter(TweetCrowd.tweet_id == t.tweet_id)
				self.decrement_num_alloc(qry2.fetch(1)[0])				
				
				try:
					self.response.out.write('t.key = %s <br>' % str(t.key))
					t.key.delete()
				except Exception as ex:
				    self.response.out.write('exception = %s <br>' % str(ex))
        
		
		
		
app = webapp2.WSGIApplication([
  ('/clean-old-annot', MainPage)
], debug=True)		
