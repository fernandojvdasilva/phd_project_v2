# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date, timedelta
import datetime
import tweepy
import webapp2
import time
from google.appengine.ext import ndb

from upload_tweets import TweetCrowd


class MainPage(webapp2.RequestHandler):

	def post(self):
		pass

	@ndb.transactional
	def reset_num_alloc(self, obj):
		obj.num_alloc = 0
		obj.put()


	def get(self):	
		
	
		self.response.out.write('Querying annotations...<br>')
		
		qry = TweetCrowd.query().filter(TweetCrowd.num_alloc < 0)
		
		tweets = qry.fetch(1000000)
		
		self.response.out.write('Len(tweet_annots) = %d<br>' % len(tweets))		
		
		for t in tweets:           
			self.reset_num_alloc(t)				
		
		
app = webapp2.WSGIApplication([
  ('/fix-neg-alloc', MainPage)
], debug=True)		
