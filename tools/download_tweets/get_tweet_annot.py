from google.appengine.ext import ndb

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

import sys
import os

import random
from random import shuffle

sys.path.append(os.getcwd())

from upload_tweets import TweetCrowd
from upload_annotations import TweetAnnotationCrowd

from json.encoder import JSONEncoder


class GetTweetMessage(messages.Message):
    annotator_id = messages.IntegerField(1, required=True)    

class GetTweetResultResponse(messages.Message):
    text = messages.StringField(1)
    tweet_id = messages.IntegerField(2, required=True)
    result_msg = messages.StringField(3)


@endpoints.api(name='getTweetAnnot', version='1')
class GetTweetAnnot(remote.Service):    


    GET_TWEET_METHOD_RESOURCE = endpoints.ResourceContainer(GetTweetMessage)

    @ndb.transactional
    def increment_num_alloc(self, obj):        
        obj.num_alloc += 1
        obj.put()

    @endpoints.method(GET_TWEET_METHOD_RESOURCE, GetTweetResultResponse,
                      path='get_tweet', http_method='POST',
                      name='get.tweet')
    def get_tweet(self, request):
        
        found_tweet = False

        tried_2 = False
        tried_1 = False
        tried_0 = False
        tried_more = False
        
        # Obtain tweets already annotated by this annotator
        qry2 = TweetAnnotationCrowd.query().filter(TweetAnnotationCrowd.annotator_id == request.annotator_id)
        num_results2 = qry2.count()
        
        already_annot_tweet_ids = []
        tweet_annots = qry2.fetch(1000000)
        for t in tweet_annots:
            already_annot_tweet_ids.append(t.tweet_id)
       
        while (not found_tweet) and (not (tried_1 and tried_2 and tried_0 and tried_more)):
            num_results = 0        
        
            if not tried_2:
                qry = TweetCrowd.query().filter(ndb.OR(
                                                       ndb.AND(TweetCrowd.num_annot == 2, TweetCrowd.num_alloc == 0),
                                                       ndb.OR(
                                                              ndb.AND(TweetCrowd.num_annot == 0, TweetCrowd.num_alloc == 2),
                                                              ndb.AND(TweetCrowd.num_annot == 1, TweetCrowd.num_alloc == 1)
                                                              )                                                                                              
                                                       )
                                                )
                num_results = qry.count()
                tried_2 = True
            
            if num_results == 0 and not tried_1:
                qry = TweetCrowd.query().filter(ndb.OR(
                                                   ndb.AND(TweetCrowd.num_annot == 1, TweetCrowd.num_alloc == 0),
                                                   ndb.AND(TweetCrowd.num_annot == 0, TweetCrowd.num_alloc == 1)                                  
                                                   )
                                                )
                num_results = qry.count()
                tried_1 = True
                
                
            if num_results == 0 and not tried_0:
                qry = TweetCrowd.query().filter(ndb.AND(TweetCrowd.num_annot == 0, TweetCrowd.num_alloc == 0))
                
                num_results = qry.count()
                tried_0 = True
                
            if num_results == 0 and not tried_more:
                qry = TweetCrowd.query().filter(ndb.OR(TweetCrowd.num_annot >= 2, TweetCrowd.num_alloc >= 2))
            
                num_results = qry.count()
                tried_more = True
            
            if num_results > 0:
                tweet_crowd = None    
                                
                if num_results2 == 0:
                    rand_index = random.randint(0, num_results-1)
                     
                    tweet_crowd = qry.fetch(1, offset=rand_index)[0]
                    found_tweet = True
                     
                else:                                                      
                    rand_index = random.randint(0, num_results-1)                      
                    selected_tweets = qry.fetch(100, offset=rand_index)
                    shuffle(selected_tweets)
                    for t in selected_tweets:
                        if not t.tweet_id in already_annot_tweet_ids:
                            tweet_crowd = t
                            found_tweet = True
                            break
                    
        
        if tweet_crowd is None:
            return GetTweetResultResponse(text='', tweet_id=0, \
                                          result_msg=("OK:ALL-ANNOTATED num_results=%d tried_1=%s tried_2=%s tried_0=%s tried_more=%s" % \
                                                      (num_results, tried_1, tried_2, tried_0, tried_more)))
                            
                            
        # Create a new annotation record
        tweet_annotation = TweetAnnotationCrowd(tweet_id=tweet_crowd.tweet_id,
                                            annotator_id=request.annotator_id,
                                            joy_vs_sadness = None,
                                            trust_vs_disgust = None,
                                            surprise_vs_antecip = None,
                                            anger_vs_fear = None)
        
        self.increment_num_alloc(tweet_crowd)
        
        try:
            tweet_annotation.put()
        except:
            return GetTweetResultResponse(result_msg="ERROR:Storage")

        return GetTweetResultResponse(text=tweet_crowd.text, tweet_id=tweet_crowd.tweet_id, result_msg=("OK num_results=%d tried_1=%s tried_2=%s tried_0=%s tried_more=%s" % \
                                                      (num_results, tried_1, tried_2, tried_0, tried_more)))