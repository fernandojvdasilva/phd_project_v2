# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
import tweepy
from datetime import date, timedelta
import time

REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token"
AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize?oauth_token="
ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token"

CONSUMER_KEY = "I5gWeLJzsSoziOWrag3IsQ"
CONSUMER_SECRET = "U8SltPLIbgWE4anqWbDNmyuobDjclkVzscKjeGM"

OAUTH_TOKEN = "1795037040-F0NemBzc67QUSgmFrOgUKkQOhbJJyjw1IDwtCT5"
OAUTH_TOKEN_SECRET = "CrG7z5SXOJypbWUjA8XPxjbRVM7YdSQ5vzkNCbdk2Ox9P"


HASHTAG_STR = "%23"

JOY_HASHTAGS = [HASHTAG_STR + "feliz", # JOY (Alegria) \
					 HASHTAG_STR + "contente", \
					 HASHTAG_STR + "alegre", \
					HASHTAG_STR + "alegria", \
					HASHTAG_STR + "felicidade",\
                     HASHTAG_STR + "encantado",\
                     HASHTAG_STR + "encanto",\
                     HASHTAG_STR + "prazeroso",\
                     HASHTAG_STR + "delicia",\
                     HASHTAG_STR + "delicioso"
                ]

SAD_HASHTAGS = [	 HASHTAG_STR + "triste", # SADNESS (Tristeza) \
					 HASHTAG_STR + "chateado", \
                     HASHTAG_STR + "chateada", \
					 HASHTAG_STR + "magoado", \
                     HASHTAG_STR + "magoada", \
					 HASHTAG_STR + "desanimo", \
                     HASHTAG_STR + "desanimado",\
                     HASHTAG_STR + "desanimada",\
                     HASHTAG_STR + "melancolico",\
                     HASHTAG_STR + "melancólico",\
                     HASHTAG_STR + "melancolia",\
                     HASHTAG_STR + "deprimido",\
                     HASHTAG_STR + "deprimida",\
                     HASHTAG_STR + "depressao",\
                     HASHTAG_STR + "depressão",\
                     HASHTAG_STR + "deprimente",\
                     HASHTAG_STR + "dor",\
                     HASHTAG_STR + "dolorido",\
                     HASHTAG_STR + "dolorida",\
                     HASHTAG_STR + "doendo",\
                     HASHTAG_STR + "sofrimento",\
                     HASHTAG_STR + "sofrendo",\
                     HASHTAG_STR + "aflito",\
                     HASHTAG_STR + "aflita",\
                     HASHTAG_STR + "aflicao",\
                     HASHTAG_STR + "aflição",\
                     HASHTAG_STR + "pesar",\
                     HASHTAG_STR + "pesames",\
                     HASHTAG_STR + "pêsames", \
                     HASHTAG_STR + "luto"
                     ]

FEAR_HASHTAGS = [HASHTAG_STR + "medo", # Fear (Medo) \
                     HASHTAG_STR + "commedo", \
                     HASHTAG_STR + "empanico",\
                     HASHTAG_STR + "aterrorizado",\
                     HASHTAG_STR + "aterrorizada",\
                     HASHTAG_STR + "terrível",\
                     HASHTAG_STR + "terrivel",\
                     HASHTAG_STR + "apavorado",\
                     HASHTAG_STR + "apavorada",\
                     HASHTAG_STR + "espanto",\
                     HASHTAG_STR + "susto",\
                     HASHTAG_STR + "assustado",\
                     HASHTAG_STR + "assustada",\
                     HASHTAG_STR + "receio",\
                     HASHTAG_STR + "receoso",\
                     HASHTAG_STR + "receosa"
                     ]

ANGER_HASHTAGS = [HASHTAG_STR + "raiva", # Anger (Raiva) \
                     HASHTAG_STR + "comraiva", \
                     HASHTAG_STR + "furia", \
                     HASHTAG_STR + "fúria", \
                     HASHTAG_STR + "furioso",\
                     HASHTAG_STR + "furiosa",\
                     HASHTAG_STR + "irritado",\
                     HASHTAG_STR + "irritada",\
                     HASHTAG_STR + "irritação",\
                     HASHTAG_STR + "aborrecido",\
                     HASHTAG_STR + "aborrecida",\
                     HASHTAG_STR + "incomodo",\
                     HASHTAG_STR + "incômodo",\
                     HASHTAG_STR + "incomodado",\
                     HASHTAG_STR + "incomodada"
                     ]

TRUST_HASHTAGS = [HASHTAG_STR + "confianca", # Trust (Confiança) \
                     HASHTAG_STR + "confiança", \
                     HASHTAG_STR + "confio", \
                     HASHTAG_STR + "confiavel", \
                     HASHTAG_STR + "confiável", \
                     HASHTAG_STR + "admiro", \
                     HASHTAG_STR + "admirável", \
                     HASHTAG_STR + "admiravel", \
                     HASHTAG_STR + "admiração", \
                     HASHTAG_STR + "admiracao", \
                     HASHTAG_STR + "idolo", \
                     HASHTAG_STR + "meuidolo"
                  ]

DISG_HASHTAGS = [HASHTAG_STR + "desgosto", # Disgust (Desgosto) \
                     HASHTAG_STR + "aversao", \
                     HASHTAG_STR + "aversão", \
                     HASHTAG_STR + "antipatia", \
                     HASHTAG_STR + "antipatico", \
                     HASHTAG_STR + "antipático", \
                     HASHTAG_STR + "antipatica", \
                     HASHTAG_STR + "antipática", \
                     HASHTAG_STR + "antipatico", \
                     HASHTAG_STR + "repugnante", \
                     HASHTAG_STR + "repugnancia", \
                     HASHTAG_STR + "repugnância", \
                     HASHTAG_STR + "nojento", \
                     HASHTAG_STR + "nojenta"
                      ]

SUR_HASHTAGS = [HASHTAG_STR + "surpreso", # Surprise (Surpreso) \
                     HASHTAG_STR + "surpresa", \
                     HASHTAG_STR + "surpreendido", \
                     HASHTAG_STR + "surpreendida", \
                     HASHTAG_STR + "espantado",\
                     HASHTAG_STR + "espantada",\
                     HASHTAG_STR + "perplexo",\
                     HASHTAG_STR + "perplexa",\
                     HASHTAG_STR + "perplexidade",\
                     HASHTAG_STR + "quemdiria"
                ]

ANTI_HASHTAGS = [HASHTAG_STR + "expectativa", # Anticipation (Antecipação) \
                     HASHTAG_STR + "naexpectativa",\
                     HASHTAG_STR + "curioso",\
                     HASHTAG_STR + "curiosa",\
                     HASHTAG_STR + "atento",\
                     HASHTAG_STR + "atenta",\
                     HASHTAG_STR + "interessado",\
                     HASHTAG_STR + "interessada",\
                     HASHTAG_STR + "interessante",\
                     HASHTAG_STR + "deolho",\
                     HASHTAG_STR + "monitorando",\
                     HASHTAG_STR + "observando",\
                     HASHTAG_STR + "analisando",\
                     HASHTAG_STR + "previsao",\
                     HASHTAG_STR + "previsão"
                 ]

EMOTIONS_HASHTAGS = [ JOY_HASHTAGS, SAD_HASHTAGS, FEAR_HASHTAGS, ANGER_HASHTAGS,
                      TRUST_HASHTAGS, DISG_HASHTAGS, SUR_HASHTAGS, ANTI_HASHTAGS ]

yesterday=date.today() - timedelta(days=1)

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
api = tweepy.API(auth)

for terms in EMOTIONS_HASHTAGS:
    for term in terms:
        try:
            for tweet in tweepy.Cursor(api.search,
                           q=term,
                           since=yesterday.strftime('%Y-%m-%d'),
                           until=date.today().strftime('%Y-%m-%d'),
                           include_entities=True,
                           lang="pt").items():
                print tweet
        except:
            print "\n\n==> term=%s with ERROR\n\n" % (term.encode('utf-8'))

    print "Waiting 15 minutes for the next search on Twitter..."
    time.sleep(15 * 60)

#r = requests.get(url=s_url, auth=oauth)
#r = requests.get(url="https://api.twitter.com/1.1/search/tweets.json?q=" + SEARCH_EMOTIONS + "&lang=pt", auth=oauth)
#tweets = str(r.json())

#print tweets
