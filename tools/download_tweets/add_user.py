from google.appengine.ext import ndb

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

import sys
import os
import random

sys.path.append(os.getcwd())

from json.encoder import JSONEncoder

class UserAnnotator(ndb.Model):           
    user_id = ndb.IntegerProperty()        
    email = ndb.StringProperty()
    gender = ndb.StringProperty()    
    age = ndb.StringProperty()
    student_degree = ndb.StringProperty()
    student_area = ndb.StringProperty()
    stock_exp = ndb.StringProperty()
    stock_technical = ndb.StringProperty()
    num_tickets = ndb.IntegerProperty()
    next_ticket_count = ndb.IntegerProperty()

class UserMessage(messages.Message):
    email = messages.StringField(1)
    gender = messages.StringField(2)    
    age = messages.StringField(3)
    student_degree = messages.StringField(4)
    student_area = messages.StringField(5)
    stock_exp = messages.StringField(6)
    stock_technical = messages.StringField(7)    

class UserResultResponse(messages.Message):
    user_id = messages.IntegerField(1)
    result_msg = messages.StringField(2)


@endpoints.api(name='addUser', version='1')
class AddUser(remote.Service):

    ADD_USER_METHOD_RESOURCE = endpoints.ResourceContainer(UserMessage)

    @ndb.transactional
    def add_user_transact(self, request):        
        rand_user_id = random.randint(0, 10000)        
        
        key_user = ndb.Key(UserAnnotator, rand_user_id)    
        user = UserAnnotator(key = key_user)
        user.user_id = rand_user_id
        user.email = request.email
        user.gender = request.gender
        user.age = request.age
        user.student_degree = request.student_degree
        user.student_area = request.student_area
        user.stock_exp = request.stock_exp
        user.stock_technical = request.stock_technical
        user.num_tickets = 0
        user.next_ticket_count = 20
        
        user.put()
        
        return rand_user_id


    @endpoints.method(ADD_USER_METHOD_RESOURCE, UserResultResponse,
                      path='add_user', http_method='POST',
                      name='add.user')
    def add_user(self, request):                                    
        
        qry = UserAnnotator.query().filter(UserAnnotator.email == request.email)
                            
        if qry.count() > 0:
            return UserResultResponse(result_msg="ERROR:E-mail existente", user_id=-1)
                        
        user_inserted = False
        num_tries = 10
        while not user_inserted and num_tries > 0:
            try:
                new_user_id = self.add_user_transact(request)
                user_inserted = True
            except:
                num_tries -= 1
                
        if num_tries < 0:
            return UserResultResponse(result_msg="ERROR:Storage", user_id=None)
        else:
            return UserResultResponse(result_msg="OK", user_id=new_user_id)


#APPLICATION = endpoints.api_server([TweetsUpload])
