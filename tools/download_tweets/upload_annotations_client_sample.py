import pprint

from apiclient.discovery import build

def main():
  # Build a service object for interacting with the API.
  api_root = 'https://ibov-twitter.appspot.com/_ah/api'
  api = 'tweetsAnnot'
  version = '1'
  discovery_url = '%s/discovery/v1/apis/%s/%s/rest' % (api_root, api, version)
  service = build(api, version, discoveryServiceUrl=discovery_url)


  body_data = {
               "filename": "20140328-449543868028313601l-011.json",
               "annotator_id": "11",
               "annotation": "{\"trust_vs_disgust\": \"neutral\", \"joy_vs_sadness\": \"neutral\", \"surprise_vs_antecip\": \"neutral\", \"anger_vs_fear\": \"neutral\"}",
               "tweet_id": "449543868028313601"
              }


  # Fetch all greetings and print them out.
  response = service.upload().annotation(body=body_data).execute()
  pprint.pprint(response)

if __name__ == '__main__':
  main()