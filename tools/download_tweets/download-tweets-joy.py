# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

JOY_HASHTAGS = [HASHTAG_STR + "feliz", # JOY (Alegria) \
					 HASHTAG_STR + "contente", \
					 HASHTAG_STR + "alegre", \
					HASHTAG_STR + "alegria", \
					HASHTAG_STR + "felicidade",\
                     HASHTAG_STR + "encantado",\
                     HASHTAG_STR + "encanto",\
                     HASHTAG_STR + "prazeroso",\
                     HASHTAG_STR + "delicia",\
                     HASHTAG_STR + "delicioso"
                ]

class MainPage(webapp2.RequestHandler):

    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(JOY_HASHTAGS, 'JOY')


app = webapp2.WSGIApplication([
  ('/download-tweets-joy', MainPage)
], debug=True)
