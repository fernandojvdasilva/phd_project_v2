import tweepy

from datetime import date, timedelta

from google.appengine.ext import ndb

class TweetJsonEmoTaged(ndb.Model):
	tweet_id = ndb.IntegerProperty()
	date = ndb.DateTimeProperty(auto_now_add=False)
	content = ndb.TextProperty()
	tag = ndb.StringProperty()


REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token"
AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize?oauth_token="
ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token"

CONSUMER_KEY = "I5gWeLJzsSoziOWrag3IsQ"
CONSUMER_SECRET = "U8SltPLIbgWE4anqWbDNmyuobDjclkVzscKjeGM"

OAUTH_TOKEN = "1795037040-F0NemBzc67QUSgmFrOgUKkQOhbJJyjw1IDwtCT5"
OAUTH_TOKEN_SECRET = "CrG7z5SXOJypbWUjA8XPxjbRVM7YdSQ5vzkNCbdk2Ox9P"


def download_auto_tagged_tweets(terms, emo_tag):
    yesterday=date.today() - timedelta(days=1)

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    api = tweepy.API(auth)


    for term in terms:
        try:
            for tweet in tweepy.Cursor(api.search,
                           q=term,
                           since=yesterday.strftime('%Y-%m-%d'),
                           until=date.today().strftime('%Y-%m-%d'),
                           include_entities=True,
                           lang="pt").items():
                tweet_emo_tagged = TweetJsonEmoTaged(date=tweet.created_at, content=str(tweet), tweet_id=tweet.id, tag=emo_tag)
                tweet_emo_tagged.put()
        except:
            continue