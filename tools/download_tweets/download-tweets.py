# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date, timedelta
import tweepy
import webapp2
import time

from google.appengine.ext import ndb

class TweetJson(ndb.Model):
	tweet_id = ndb.IntegerProperty()
	date = ndb.DateTimeProperty(auto_now_add=False)
	stock = ndb.StringProperty()
	content = ndb.TextProperty()


REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token"
AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize?oauth_token="
ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token"

CONSUMER_KEY = "I5gWeLJzsSoziOWrag3IsQ"
CONSUMER_SECRET = "U8SltPLIbgWE4anqWbDNmyuobDjclkVzscKjeGM"

OAUTH_TOKEN = "1795037040-F0NemBzc67QUSgmFrOgUKkQOhbJJyjw1IDwtCT5"
OAUTH_TOKEN_SECRET = "CrG7z5SXOJypbWUjA8XPxjbRVM7YdSQ5vzkNCbdk2Ox9P"


STOCK_CODES = [ "ABEV3", \
	        "AEDU3", \
		"ALLL3", \
		"BBAS3", \
		"BBDC3", \
		"BBDC4", \
		"BBSE3", \
		"BISA3", \
		"BRAP4", \
		"BRFS3", \
		"BRKM5", \
		"BRML3", \
		"BRPR3", \
		"BVMF3", \
		"CCRO3", \
		"CESP6", \
		"CIEL3", \
		"CMIG4", \
		"CPFE3", \
		"CPLE6", \
		"CRUZ3", \
		"CSAN3", \
		"CSNA3", \
		"CTIP3", \
		"CYRE3", \
		"DASA3", \
		"DTEX3", \
		"ECOR3", \
		"ELET3", \
		"ELET6", \
		"ELPL4", \
		"EMBR3", \
		"ENBR3", \
		"ESTC3", \
		"EVEN3", \
		"FIBR3", \
		"GFSA3", \
		"GGBR4", \
		"GOAU4", \
		"GOLL4", \
		"HGTX3", \
		"HYPE3", \
		"ITSA4", \
		"ITUB4", \
		"JBSS3", \
		"KLBN11",\
		"KLBN4", \
		"KROT3", \
		"LAME4", \
		"LIGT3", \
		"LLXL3", \
		"LREN3", \
		"MRFG3", \
		"MRVE3", \
		"NATU3", \
		"OIBR4", \
		"PCAR4", \
		"PDGR3", \
		"PETR3", \
		"PETR4", \
		"QUAL3", \
		"RENT3", \
		"RSID3", \
		"SANB11", \
		"SBSP3", \
		"SUZB5", \
		"TBLE3", \
		"TIMP3", \
		"UGPA3", \
		"USIM5", \
		"VALE3", \
		"VALE5", \
		"VIVT4"]


class MainPage(webapp2.RequestHandler):

	def post(self):
		pass

	def get(self):	
		tweets = ""

		yesterday=date.today() - timedelta(days=1)

		auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
	        auth.set_access_token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
		api = tweepy.API(auth)
		for term in STOCK_CODES:
			for tweet in tweepy.Cursor(api.search,
					           q=term,
						   since=yesterday.strftime('%Y-%m-%d'),
						   until=date.today().strftime('%Y-%m-%d'),
					           include_entities=True,
						   count=100,
					           lang="pt").items():
				tweet_json = TweetJson(date=tweet.created_at, content=str(tweet), tweet_id=tweet.id, stock=term)
	 		    	tweet_json.put()

app = webapp2.WSGIApplication([
  ('/download-tweets', MainPage)
], debug=True)		
