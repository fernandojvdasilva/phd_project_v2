# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

ANTI_HASHTAGS = [HASHTAG_STR + "expectativa", # Anticipation (Antecipação) \
                     HASHTAG_STR + "naexpectativa",\
                     HASHTAG_STR + "curioso",\
                     HASHTAG_STR + "curiosa",\
                     HASHTAG_STR + "atento",\
                     HASHTAG_STR + "atenta",\
                     HASHTAG_STR + "interessado",\
                     HASHTAG_STR + "interessada",\
                     HASHTAG_STR + "interessante",\
                     HASHTAG_STR + "deolho",\
                     HASHTAG_STR + "monitorando",\
                     HASHTAG_STR + "observando",\
                     HASHTAG_STR + "analisando",\
                     HASHTAG_STR + "previsao",\
                     HASHTAG_STR + "previsão"
                 ]

class MainPage(webapp2.RequestHandler):
    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(ANTI_HASHTAGS, 'ANT')


app = webapp2.WSGIApplication([
  ('/download-tweets-anti', MainPage)
], debug=True)
