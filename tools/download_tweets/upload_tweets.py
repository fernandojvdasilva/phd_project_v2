from google.appengine.ext import ndb

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

import sys
import os

sys.path.append(os.getcwd())

from json.encoder import JSONEncoder

class TweetCrowd(ndb.Model):           
    tweet_id = ndb.IntegerProperty()        
    text = ndb.StringProperty()
    num_annot = ndb.IntegerProperty()    
    num_alloc = ndb.IntegerProperty()

class TweetMessage(messages.Message):
    text = messages.StringField(1)
    tweet_id = messages.IntegerField(2, required=True)

class TweetResultResponse(messages.Message):
    result_msg = messages.StringField(1)


@endpoints.api(name='tweetsUpload', version='1')
class TweetsUpload(remote.Service):

    UPLOAD_TWEET_METHOD_RESOURCE = endpoints.ResourceContainer(TweetMessage)

    @endpoints.method(UPLOAD_TWEET_METHOD_RESOURCE, TweetResultResponse,
                      path='upload_tweet', http_method='POST',
                      name='upload.tweet')
    def upload_tweet(self, request):                    
        tweet = TweetCrowd(tweet_id=request.tweet_id,
                            text=request.text,
                            num_annot=0,
                            num_alloc=0)
        try:
            tweet.put()
        except:
            return TweetResultResponse(result_msg="ERROR:Storage")

        return TweetResultResponse(result_msg="OK")


#APPLICATION = endpoints.api_server([TweetsUpload])
