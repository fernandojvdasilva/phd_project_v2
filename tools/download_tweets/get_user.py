from google.appengine.ext import ndb

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

import sys
import os
import random

sys.path.append(os.getcwd())

from json.encoder import JSONEncoder

from add_user import UserAnnotator

class GetUserMessage(messages.Message):
    email = messages.StringField(1)

class GetUserResultResponse(messages.Message):
    user_id = messages.IntegerField(1)
    result_msg = messages.StringField(2)
    num_tickets = messages.IntegerField(3, required=True)
    next_ticket_count = messages.IntegerField(4, required=True)    


@endpoints.api(name='getUser', version='1')
class GetUser(remote.Service):

    GET_USER_METHOD_RESOURCE = endpoints.ResourceContainer(GetUserMessage)


    @endpoints.method(GET_USER_METHOD_RESOURCE, GetUserResultResponse,
                      path='get_user', http_method='POST',
                      name='get.user')
    def get_user(self, request):                                    
        try:
            qry = UserAnnotator.query().filter(UserAnnotator.email == request.email)
                            
            if qry.count() == 1:
                user_annotator = qry.fetch(1)[0]
                num_tickets = qry.fetch
                next_ticket_count = messages.IntegerField(3, required=True)

                
                return GetUserResultResponse(result_msg="OK", user_id=user_annotator.user_id, \
                                             num_tickets=user_annotator.num_tickets, \
                                             next_ticket_count=user_annotator.next_ticket_count)
            elif qry.count() == 0:
                return GetUserResultResponse(result_msg="OK:No-user", user_id=-1, \
                                             num_tickets=0, \
                                             next_ticket_count=50)
            else:
                return GetUserResultResponse(result_msg="ERROR:Repeated Email", user_id=-1, \
                                             num_tickets=0, \
                                             next_ticket_count=50)
        except:            
            return GetUserResultResponse(result_msg="ERROR:Unknown", user_id=-1, \
                                         num_tickets=0, \
                                         next_ticket_count=50)
            


#APPLICATION = endpoints.api_server([TweetsUpload])
