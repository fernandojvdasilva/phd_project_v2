# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
import requests
from requests_oauthlib import OAuth1
from urlparse import parse_qs
from datetime import date, timedelta


REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token"
AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize?oauth_token="
ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token"

CONSUMER_KEY = "I5gWeLJzsSoziOWrag3IsQ"
CONSUMER_SECRET = "U8SltPLIbgWE4anqWbDNmyuobDjclkVzscKjeGM"

OAUTH_TOKEN = "1795037040-F0NemBzc67QUSgmFrOgUKkQOhbJJyjw1IDwtCT5"
OAUTH_TOKEN_SECRET = "CrG7z5SXOJypbWUjA8XPxjbRVM7YdSQ5vzkNCbdk2Ox9P"


STOCK_CODES = [ "ABEV3", \
	        "AEDU3", \
		"ALLL3", \
		"BBAS3", \
		"BBDC3", \
		"BBDC4", \
		"BBSE3", \
		"BISA3", \
		"BRAP4", \
		"BRFS3", \
		"BRKM5", \
		"BRML3", \
		"BRPR3", \
		"BVMF3", \
		"CCRO3", \
		"CESP6", \
		"CIEL3", \
		"CMIG4", \
		"CPFE3", \
		"CPLE6", \
		"CRUZ3", \
		"CSAN3", \
		"CSNA3", \
		"CTIP3", \
		"CYRE3", \
		"DASA3", \
		"DTEX3", \
		"ECOR3", \
		"ELET3", \
		"ELET6", \
		"ELPL4", \
		"EMBR3", \
		"ENBR3", \
		"ESTC3", \
		"EVEN3", \
		"FIBR3", \
		"GFSA3", \
		"GGBR4", \
		"GOAU4", \
		"GOLL4", \
		"HGTX3", \
		"HYPE3", \
		"ITSA4" ]

STOCK_CODES2 = 	[ "ITUB4", \
		"JBSS3", \
		"KLBN11", \
		"KLBN4", \
		"KROT3", \
		"LAME4", \
		"LIGT3", \
		"LLXL3", \
		"LREN3", \
		"MRFG3", \
		"MRVE3", \
		"NATU3", \
		"OIBR4", \
		"PCAR4", \
		"PDGR3", \
		"PETR3", \
		"PETR4", \
		"QUAL3", \
		"RENT3", \
		"RSID3", \
		"SANB11", \
		"SBSP3", \
		"SUZB5", \
		"TBLE3", \
		"TIMP3", \
		"UGPA3", \
		"USIM5", \
		"VALE3", \
		"VALE5", \
		"VIVT4"]

SEARCH_STOCK_CODES = ""
for code in STOCK_CODES:
	SEARCH_STOCK_CODES = "%s%s%%20OR%%20" % (SEARCH_STOCK_CODES, code)

SEARCH_STOCK_CODES = SEARCH_STOCK_CODES[0:len(SEARCH_STOCK_CODES)-8]

SEARCH_STOCK_CODES2 = ""
for code in STOCK_CODES2:
	SEARCH_STOCK_CODES2 = "%s%s%%20OR%%20" % (SEARCH_STOCK_CODES2, code)

SEARCH_STOCK_CODES2 = SEARCH_STOCK_CODES2[0:len(SEARCH_STOCK_CODES2)-8]

yesterday=date.today() - timedelta(days=1)

DATE_FILTER = "%%20since%%3A%s%%20until%%3A%s" % (yesterday.strftime('%Y-%m-%d'), date.today().strftime('%Y-%m-%d'))

def setup_oauth():
    """Authorize your app via identifier."""
    # Request token
    oauth = OAuth1(CONSUMER_KEY, client_secret=CONSUMER_SECRET)
    r = requests.post(url=REQUEST_TOKEN_URL, auth=oauth)
    credentials = parse_qs(r.content)

    resource_owner_key = credentials.get('oauth_token')[0]
    resource_owner_secret = credentials.get('oauth_token_secret')[0]

    # Authorize
    authorize_url = AUTHORIZE_URL + resource_owner_key

    verifier = raw_input('Please input the verifier: ')
    oauth = OAuth1(CONSUMER_KEY,
                   client_secret=CONSUMER_SECRET,
                   resource_owner_key=resource_owner_key,
                   resource_owner_secret=resource_owner_secret,
                   verifier=verifier)

    # Finally, Obtain the Access Token
    r = requests.post(url=ACCESS_TOKEN_URL, auth=oauth)
    credentials = parse_qs(r.content)
    token = credentials.get('oauth_token')[0]
    secret = credentials.get('oauth_token_secret')[0]

    return token, secret


def get_oauth():
    oauth = OAuth1(CONSUMER_KEY,
                client_secret=CONSUMER_SECRET,
                resource_owner_key=OAUTH_TOKEN,
                resource_owner_secret=OAUTH_TOKEN_SECRET)
    return oauth

oauth = get_oauth()

tweets = ""

r = requests.get(url="https://api.twitter.com/1.1/search/tweets.json?q=" + SEARCH_STOCK_CODES + DATE_FILTER + "&lang=pt", auth=oauth)
tweets = str(r.json())

r = requests.get(url="https://api.twitter.com/1.1/search/tweets.json?q=" + SEARCH_STOCK_CODES2 + DATE_FILTER + "&lang=pt", auth=oauth)

tweets = tweets + str(r.json())

print tweets
