# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date, timedelta
import datetime
import tweepy
import webapp2
import time
from google.appengine.ext import ndb

from upload_annotations import TweetAnnotationCrowd
from upload_tweets import TweetCrowd 

class MainPage(webapp2.RequestHandler):

	def post(self):
		pass

	@ndb.transactional
	def decrement_num_annot(self, obj):        
		obj.num_annot -= 1
		obj.put()


	def get(self):	
		
		ID_BAN = 2387 # danilolima.1@hotmail.com
		
	
		self.response.out.write('Querying annotations...<br>')
		
		qry2 = TweetAnnotationCrowd.query().filter(TweetAnnotationCrowd.annotator_id == ID_BAN)				
		
		
		qry = TweetCrowd.query().filter(TweetCrowd.num_annot > 0)
		tweets = qry.fetch(1000000)
		
		self.response.out.write('ID_BAN = %d<br>' % ID_BAN)
								
	
		annotations = qry2.fetch(1000)
		
		self.response.out.write('Len(annotations) = %d<br>' % len(annotations))
		
		tweet_ids = []
		for annot in annotations:			
			tweet_ids.append(annot.tweet_id)						
			
			self.response.out.write('Removing annotation tweet id %d, annotator %d<br>' % (annot.tweet_id, annot.annotator_id))
			annot.key.delete()
			
		for t in tweets:
			if t.tweet_id in tweet_ids:
				self.response.out.write('Adjusting tweet annot counter tweet id %d<br>' % (t.tweet_id))
				self.decrement_num_annot(t)
		
		
app = webapp2.WSGIApplication([
  ('/ban-user', MainPage)
], debug=True)		
