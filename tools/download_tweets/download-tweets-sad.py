# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

SAD_HASHTAGS = [	 HASHTAG_STR + "triste", # SADNESS (Tristeza) \
					 HASHTAG_STR + "chateado", \
                     HASHTAG_STR + "chateada", \
					 HASHTAG_STR + "magoado", \
                     HASHTAG_STR + "magoada", \
					 HASHTAG_STR + "desanimo", \
                     HASHTAG_STR + "desanimado",\
                     HASHTAG_STR + "desanimada",\
                     HASHTAG_STR + "melancolico",\
                     HASHTAG_STR + "melancólico",\
                     HASHTAG_STR + "melancolia",\
                     HASHTAG_STR + "deprimido",\
                     HASHTAG_STR + "deprimida",\
                     HASHTAG_STR + "depressao",\
                     HASHTAG_STR + "depressão",\
                     HASHTAG_STR + "deprimente",\
                     HASHTAG_STR + "dor",\
                     HASHTAG_STR + "dolorido",\
                     HASHTAG_STR + "dolorida",\
                     HASHTAG_STR + "doendo",\
                     HASHTAG_STR + "sofrimento",\
                     HASHTAG_STR + "sofrendo",\
                     HASHTAG_STR + "aflito",\
                     HASHTAG_STR + "aflita",\
                     HASHTAG_STR + "aflicao",\
                     HASHTAG_STR + "aflição",\
                     HASHTAG_STR + "pesar",\
                     HASHTAG_STR + "pesames",\
                     HASHTAG_STR + "pêsames", \
                     HASHTAG_STR + "luto"
                     ]

class MainPage(webapp2.RequestHandler):

    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(SAD_HASHTAGS, 'SAD')


app = webapp2.WSGIApplication([
  ('/download-tweets-sad', MainPage)
], debug=True)
