# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date, timedelta
import datetime
import tweepy
import webapp2
import time
from google.appengine.ext import ndb

from upload_tweets import TweetCrowd
from upload_annotations import TweetAnnotationCrowd
from add_user import UserAnnotator

class MainPage(webapp2.RequestHandler):

	def post(self):
		pass
	

	@ndb.transactional
	def set_num_tickets(self, obj, num_annot):
		num_tickets = num_annot / 20
		next_ticket_count = 20 - (num_annot % 20)
		
		obj.num_tickets = num_tickets
		obj.next_ticket_count = next_ticket_count
		obj.put()


	def get(self):	
		
	
		self.response.out.write('Querying annotations...<br>')
		
		qry = UserAnnotator.query().filter(UserAnnotator.user_id > 0)
		
		users = qry.fetch(1000000)			
		
		for u in users:
			qry2 = TweetAnnotationCrowd.query().filter(TweetAnnotationCrowd.annotator_id == u.user_id)
			num_annot = qry2.count()
			
			self.set_num_tickets(u, num_annot)
		
app = webapp2.WSGIApplication([
  ('/fix-num-tickets', MainPage)
], debug=True)		
