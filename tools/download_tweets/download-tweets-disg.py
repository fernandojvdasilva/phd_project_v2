# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

DISG_HASHTAGS = [HASHTAG_STR + "desgosto", # Disgust (Desgosto) \
                     HASHTAG_STR + "aversao", \
                     HASHTAG_STR + "aversão", \
                     HASHTAG_STR + "antipatia", \
                     HASHTAG_STR + "antipatico", \
                     HASHTAG_STR + "antipático", \
                     HASHTAG_STR + "antipatica", \
                     HASHTAG_STR + "antipática", \
                     HASHTAG_STR + "antipatico", \
                     HASHTAG_STR + "repugnante", \
                     HASHTAG_STR + "repugnancia", \
                     HASHTAG_STR + "repugnância", \
                     HASHTAG_STR + "nojento", \
                     HASHTAG_STR + "nojenta", \
                     HASHTAG_STR + "nojo", \
                     HASHTAG_STR + u"despresivel", \
                     HASHTAG_STR + u"despresível"
                      ]

class MainPage(webapp2.RequestHandler):

    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(DISG_HASHTAGS, 'DIS')


app = webapp2.WSGIApplication([
  ('/download-tweets-disg', MainPage)
], debug=True)
