# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date, timedelta
import datetime
import tweepy
import webapp2
import time
from google.appengine.ext import ndb

from upload_tweets import TweetCrowd 
from add_user import UserAnnotator

class MainPage(webapp2.RequestHandler):

	def post(self):
		pass

	def get(self):	
				
	
		self.response.out.write('Querying stats...<br>')
		
		qry = UserAnnotator.query().filter(ndb.OR(UserAnnotator.next_ticket_count < 20, \
												UserAnnotator.num_tickets > 0))
		
		num_active_annotators = qry.count()
		
		self.response.out.write('There are %d active annotators<br>' % num_active_annotators)
		
				
		qry2 = TweetCrowd.query().filter(TweetCrowd.num_annot >= 3)
		
		num_useful_tweets = qry2.count()
						
		self.response.out.write('There are %d tweets with 3 or more annotations<br>' % num_useful_tweets)
		
		
		
app = webapp2.WSGIApplication([
  ('/annotation_stats', MainPage)
], debug=True)		
