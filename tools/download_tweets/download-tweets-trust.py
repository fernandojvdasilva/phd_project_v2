# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from download_utils import *
import webapp2

HASHTAG_STR = "%23"

TRUST_HASHTAGS = [HASHTAG_STR + "confianca", # Trust (Confiança) \
                     HASHTAG_STR + "confiança", \
                     HASHTAG_STR + "confio", \
                     HASHTAG_STR + "confiavel", \
                     HASHTAG_STR + "confiável", \
                     HASHTAG_STR + "admiro", \
                     HASHTAG_STR + "admirável", \
                     HASHTAG_STR + "admiravel", \
                     HASHTAG_STR + "admiração", \
                     HASHTAG_STR + "admiracao", \
                     HASHTAG_STR + "idolo", \
                     HASHTAG_STR + "meuidolo"
                  ]

class MainPage(webapp2.RequestHandler):

    def post(self):
        pass

    def get(self):
        download_auto_tagged_tweets(TRUST_HASHTAGS, 'TRU')


app = webapp2.WSGIApplication([
  ('/download-tweets-trust', MainPage)
], debug=True)
