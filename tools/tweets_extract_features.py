#!/usr/local/bin/python2.7
# encoding: utf-8
'''
tools.tweets_extract_features -- extract features from tweets for machine learning experiments

tools.tweets_extract_features reads tweets and converts into a .csv with their features.


@author:     Fernando J. V. da Silva

@copyright:  2015 Fernando J. V. da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
@deffield    updated: Updated
'''
import pickle

import sys
import os
import string
import numpy as np

import nltk
from sklearn.feature_extraction.text import TfidfVectorizer

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from tweets.tweet_corpus import TweetCorpus
from tweets_annotator_manual.tweet_annotation_manual import TweetAnnotationManual
from tweets_annotator_manual.tweets_annotator_config import AnnotatorConfig

__all__ = []
__version__ = 0.1
__date__ = '2015-08-06'
__updated__ = '2015-08-06'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

CMD_EXIT = 'e'
CMD_PREVIOUS = 'p'
CMD_NEXT = 'n' 

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg
    
stemmer = nltk.stem.RSLPStemmer() # Using Portuguese

def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def tokenize(text):
    stopwords = nltk.corpus.stopwords.words('portuguese')
    u_stopwords = [unicode(w, "utf-8") for w in stopwords]
    tokens = nltk.word_tokenize(text)
    tokens = [t for t in tokens if t not in u_stopwords]
    stems = stem_tokens(tokens, stemmer)
    return stems    


header = ''

def get_class_headers(votes):
    global header

    if header != '':
        return header

    for annotation in votes.keys():
        for tag in votes[annotation].keys():
            if tag == 'neutral':
                continue
            header += tag + ','

    return header

def extract_classes(manual_annotations, config):
    # We consider the most annotated tags only
    votes = {}
    for annotation_config in config['annotations']:
        votes[annotation_config['name']] = {}
        for options in annotation_config['options']:
            votes[annotation_config['name']][options['tag']] = 0
        votes[annotation_config['name']]['neutral'] = 0

    for annotation in manual_annotations:
        for tag in annotation.tags.keys():
            if tag == 'sarcasm': # We aren't using sarcasm here...
                continue

            if annotation.tags[tag] == 'remove': # This tweet must be discarded!! =========
                return None

            if annotation.tags[tag] != 'dontknow':
                votes[tag][annotation.tags[tag]] += 1

    header = get_class_headers(votes)

    all_neutral = True
    classes = [] # classes in binary features. Each feature is an emotion. The last one is for neutral
    for annotation in votes.keys():
        choosen_annotation = ''
        max_num_votes = 0
        for tag in votes[annotation].keys():
            if votes[annotation][tag] > max_num_votes:
                max_num_votes = votes[annotation][tag]
                choosen_annotation = tag

        if choosen_annotation == 'neutral':
            classes.extend([0,0])
        else:
            all_neutral = False
            for tag in votes[annotation].keys():
                if tag == 'neutral':
                    continue
                if tag == choosen_annotation:
                    classes.append(1)
                else:
                    classes.append(0)

    if all_neutral:
        classes.append(1)
    else:
        classes.append(0)

    return classes

def save_to_csv(out_dir, token_dict, emotions):
    csv_file = open("%s/tweets_annotated.csv" % out_dir, 'w')
    header = 'tweet_id,text,TRU,DIS,JOY,SAD,ANT,SUR,ANG,FEA,NEUTRAL\n'
    csv_file.write(header)

    for k,emo in zip(token_dict.keys(), emotions):
        line = '%ul,%s,' % (k, token_dict[k])
        for e in emo:
            line += '%d,' % e
        line += '\n'
        csv_file.write(line.encode('utf-8'))

    csv_file.close()


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  Copyright 2015 Fernando J. V. da Silva. All rights reserved.

  Licensed under GPL
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

#    try:
    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-p", "--path", dest="path", help="Corpus path" )
    parser.add_argument("-o", "--out", dest="out", help="Output directory" )
    parser.add_argument("-c", "--config", dest="config", help="Config file path" )
    parser.add_argument('-V', '--version', action='version', version=program_version_message)
    

    # Process arguments
    args = parser.parse_args()

    out_dir = args.out

    path = args.path
    config_path = args.config

    config = AnnotatorConfig()
    config.load(config_path)

    tweet_corpus = TweetCorpus()
    tweet_corpus.load_corpus(path)
    
    token_dict = {}

    all_classes = []
    while tweet_corpus.next_tweet() != False:
        manual_annotations = TweetAnnotationManual.get_annotations_by_tweet(path, tweet_corpus.curr_tweet)

        if len(manual_annotations) == 0:
            continue

        classes = extract_classes(manual_annotations, config)

        if classes is None:
            continue

        all_classes.append(classes)

        lowers = tweet_corpus.curr_tweet['text'].lower()
        no_punctuation = [c for c in lowers if c not in string.punctuation]
        no_punctuation = ''.join(no_punctuation)

        token_dict[tweet_corpus.curr_tweet['id']] = no_punctuation
        
    tfidf = TfidfVectorizer(tokenizer=tokenize)

    tfs = tfidf.fit_transform(token_dict.values())

    save_to_csv(out_dir, token_dict, all_classes)

    with open("%s/tweets_tfidf_features.pkl" % out_dir, 'wb') as handle:
        pickle.dump(tfs, handle)

    with open("%s/tweets_tfidf_classes.pkl" % out_dir, 'wb') as handle:
        pickle.dump(np.array(all_classes), handle)

    print "Classes indexes are:\n"
    print header

    print "Done!"

    return 0
#     except KeyboardInterrupt:
#         ### handle keyboard interrupt ###
#         return 0
#     except Exception, e:
#         if DEBUG or TESTRUN:
#             raise(e)
#         indent = len(program_name) * " "
#         sys.stderr.write(program_name + ": " + repr(e) + "\n")
#         sys.stderr.write(indent + "  for help use --help")
#         return 2

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.tweets_annotation_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
