#!/usr/local/bin/python2.7
# encoding: utf-8
'''
tools.tweets_annotation -- shortdesc

tools.tweets_annotation is a description

It defines classes_and_methods

@author:     Fernando J. V. da Silva

@copyright:  2015 Fernando J. V. da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
@deffield    updated: Updated
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import tweets_annotator_manual
from tweets_annotator_manual.tweets_annotator_service import TweetsAnnotatorService
from tweets.tweet_local import TweetLocal
from dateutil import parser as dateparser
from sys import stdin
from time import sleep

__all__ = []
__version__ = 0.1
__date__ = '2015-03-04'
__updated__ = '2015-03-04'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

CMD_EXIT = 'e'
CMD_PREVIOUS = 'p'
CMD_NEXT = 'n' 

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg
    
def print_tweet(tweet_annotator, lang):
    if os.name == 'posix':
        clear_screen = lambda : os.system('clear')
    else:
        clear_screen = lambda : os.system('cls')
    
    clear_screen()

    print '======  Manual Annotation application for NLP research purposes =====================\n'

    print 'Restam %d Tweets para serem anotados\n' % tweet_annotator.get_num_remaining_tweets_to_annotate()

    print 'ID: %ul' % tweet_annotator.corpus.curr_tweet['id']
    print 'Date: %.02d/%.02d/%.04d' % (tweet_annotator.corpus.curr_tweet['date'].day, \
                                         tweet_annotator.corpus.curr_tweet['date'].month, \
                                         tweet_annotator.corpus.curr_tweet['date'].year)    
    try:
        print tweet_annotator.corpus.curr_tweet['text']
    except:
        sys.stdout.write(tweet_annotator.corpus.curr_tweet['text'])
    print '\n'
    i = 1
    for annotation in tweet_annotator.curr_annotation['options']:
        if tweet_annotator.corpus.curr_tweet.manual_annotation != None and \
        annotation['tag'] in tweet_annotator.corpus.curr_tweet.manual_annotation.tags.values():
            selected_char = '-->'
        else:
            selected_char = '   '
        print "%s %d - %s\n" % (selected_char, i, annotation[lang])
        i += 1
            
    
    if tweet_annotator.corpus.curr_tweet.manual_annotation != None and \
    'neutral' in tweet_annotator.corpus.curr_tweet.manual_annotation.tags.values():
        selected_char = '-->'
    else:
        selected_char = '   '
    print "%s %d - %s\n" % (selected_char, i, tweet_annotator.config['neutral_text'][lang])
    cmd_neutral = i
    i += 1
        
    if tweet_annotator.curr_annotation['dontknow'] == 'TRUE':
        if tweet_annotator.corpus.curr_tweet.manual_annotation != None and \
        'dontknow' in tweet_annotator.corpus.curr_tweet.manual_annotation.tags.values():
            selected_char = '-->'
        else:
            selected_char = '   '
        print "%s %d - %s\n" % (selected_char, i, tweet_annotator.config['dontknow_text'][lang])
        cmd_dont_know = i
        i += 1
        
    if tweet_annotator.curr_annotation['remove'] == 'TRUE':
        if tweet_annotator.corpus.curr_tweet.manual_annotation != None and \
        'remove' in tweet_annotator.corpus.curr_tweet.manual_annotation.tags.values():
            selected_char = '-->'
        else:
            selected_char = '   '
        cmd_remove = i
        print "%s %d - %s\n" % (selected_char, i, tweet_annotator.config['remove_text'][lang])

    print "\n\nHelp: %c - Previous Annotation/Tweet; %c - Next Annotation/Tweet; %c - Exit\n\n" % \
            (CMD_PREVIOUS, CMD_NEXT, CMD_EXIT)
        
    return cmd_neutral, cmd_dont_know, cmd_remove

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  Copyright 2015 Fernando J. V. da Silva. All rights reserved.

  Licensed under GPL
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

#    try:
    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-l", "--lang", dest="lang", help="Language used")
    parser.add_argument("-a", "--annotator_id", dest="annotator_id", help="ID of the annotator")
    parser.add_argument("-p", "--path", dest="path", help="Corpus path" )
    parser.add_argument("-c", "--config", dest="config", help="Config file path" )
    parser.add_argument('-V', '--version', action='version', version=program_version_message)
    

    # Process arguments
    args = parser.parse_args()

    path = args.path
    lang = args.lang
    annotator_id = args.annotator_id
    config_file = args.config

    #sys.setdefaultencoding('utf-8')

    tweets_annotator = TweetsAnnotatorService(path, lang, config_file)
    tweets_annotator.setup_annotation_env(annotator_id)
    
    cmd = None

    tweets_annotator.remove_repeated_tweets()
    are_there_tweets = tweets_annotator.begin_annotation()


    while  cmd != CMD_EXIT and are_there_tweets:
        
        cmd_neutral, cmd_dont_know, cmd_remove = print_tweet(tweets_annotator, lang) 
        
        cmd = stdin.readline()
        cmd = cmd[0]
        if cmd == CMD_EXIT:
            break
        if cmd == CMD_PREVIOUS:
            if not tweets_annotator.prev_annotation():
                tweets_annotator.prev_tweet()
        elif cmd == CMD_NEXT:
            if not tweets_annotator.next_annotation():
                are_there_tweets = tweets_annotator.next_tweet()
        elif int(cmd) == cmd_neutral:
            tweets_annotator.annotate_tweet(\
                        tweets_annotator.curr_annotation['name'], \
                                            'neutral')
            print_tweet(tweets_annotator, lang)
            if not tweets_annotator.next_annotation():
                are_there_tweets = tweets_annotator.next_tweet()
        elif int(cmd) == cmd_dont_know:
            tweets_annotator.annotate_tweet(\
                        tweets_annotator.curr_annotation['name'], \
                        'dontknow')
            print_tweet(tweets_annotator, lang)
            if not tweets_annotator.next_annotation():
                are_there_tweets = tweets_annotator.next_tweet()
        elif int(cmd) == cmd_remove:
            tweets_annotator.annotate_tweet('remove', 'remove')
            print_tweet(tweets_annotator, lang)
            are_there_tweets = tweets_annotator.next_tweet()
        else:
            tweets_annotator.annotate_tweet(\
                            tweets_annotator.curr_annotation['name'], \
                            tweets_annotator.curr_annotation['options'][int(cmd)-1]['tag'])
            print_tweet(tweets_annotator, lang)
            if not tweets_annotator.next_annotation():
                are_there_tweets = tweets_annotator.next_tweet()
                                
        sleep(1)

    if not are_there_tweets:
        print u"Você completou as anotações! Muito Obrigado! Por favor, compacte a pasta onde esse aplicativo se encontra e " \
              u"envie para fernandojvdasilva@gmail.com"

    print u"Enviando anotações para o servidor (Pode levar algum tempo, por favor aguarde...)"
    tweets_annotator.upload_annotations()
    print u"Anotações enviadas!"

    print u"Aperte qualquer tecla para sair..."
    stdin.readline()

    return 0
#     except KeyboardInterrupt:
#         ### handle keyboard interrupt ###
#         return 0
#     except Exception, e:
#         if DEBUG or TESTRUN:
#             raise(e)
#         indent = len(program_name) * " "
#         sys.stderr.write(program_name + ": " + repr(e) + "\n")
#         sys.stderr.write(indent + "  for help use --help")
#         return 2

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.tweets_annotation_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
