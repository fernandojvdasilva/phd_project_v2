'''
tools.tweets_upload_gae -- Uploads tweets from local corpus into the cloud

tools.tweets_upload_gae is a simple tool for uploading tweets' contents into
the Google App Engine (TweetCrowd entity).


@author:     fernando

@copyright:  All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
'''

import sys
import os
from apiclient.discovery import build

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from tweets.tweet_corpus import *

__all__ = []
__version__ = 0.1
__date__ = '2016-06-04'
__updated__ = '2016-06-04'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def send_tweet_to_gae(text, tweet_id):

        api_root = 'https://ibov-twitter.appspot.com/_ah/api'
        api = 'tweetsUpload'        
        version = '1'
        discovery_url = '%s/discovery/v1/apis/%s/%s/rest' % (api_root, api, version)
        
        sys.stdout.write(discovery_url)
        
        body_data = {
                     "text": text,                     
                     "tweet_id": tweet_id
                     }


        service = build(api, version, discoveryServiceUrl=discovery_url)


        # Fetch all greetings and print them out.
        upload_ok = False
        num_tries = 0
        while not upload_ok or num_tries == 10:
            try:
                response = service.upload().tweet(body=body_data).execute()
                upload_ok = True
                num_tries = 0
            except Exception as ex:                
                upload_ok = False
                num_tries += 1
                if num_tries == 10:
                    sys.stderr.write('Num tries exceeded\n')
                    sys.stderr.write(repr(e) + "\n")
                
        
        
            




def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  All rights reserved.

  Licensed under the GPL License  

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-p", "--path", dest="path", help="Corpus path" )
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        
        # Process arguments
        args = parser.parse_args()
        
        corpus_path = args.path
        
        corpus = TweetCorpus()
        corpus.load_corpus(corpus_path)
        
        curr_tweet = corpus.first_tweet()
        
        sys.stdout.write("Uploading tweets...\n")
        while curr_tweet != False:
            sys.stdout.write("---> %d - %s" % (curr_tweet['id'], curr_tweet['text']))
            send_tweet_to_gae(curr_tweet['text'], curr_tweet['id'])
            curr_tweet = corpus.next_tweet()
        
        
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        #if DEBUG or TESTRUN:
        #    raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    #if DEBUG:
    #    sys.argv.append("-h")
    #    sys.argv.append("-v")
    #    sys.argv.append("-r")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.tweets_upload_gae_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())