
from nltk.draw import TreeView
from nltk.draw import TreeWidget
from nltk.draw.util import CanvasFrame
from nltk import Tree
import os

def ignoreCharNotSupportedByTkinter(text):
    char_list = [text[j] if ord(text[j]) in range(65536) else '*' for j in range(len(text))]
    text=''
    for j in char_list:
        text=text+j
                    
    return text


def showTree(treestr, figure_name):
    # Ignore characters not supported by Tkinter (visualization only)
    treestr = ignoreCharNotSupportedByTkinter(treestr)        
    
    t = Tree.fromstring(treestr)

    TreeView(t)
    
    cf = CanvasFrame()
    tc = TreeWidget(cf.canvas(),t)
    cf.add_widget(tc,10,10) # (10,10) offsets
    cf.print_to_file("%s.ps" % figure_name)
    os.system('convert %s.ps %s.png' % (figure_name, figure_name))
    cf.destroy()


showTree("(ROOT ||T|| (STOP this) (STOP is) (NOT n't) (STOP a) (EW great POS p.p.) (EW day POS p.p.) (STOP for) (EW playing POS p.p.) (STOP the) (EW harp POS p.p.))", 'agarwal-tree')
#showTree('(S N VP)', 'grammar-sample-tree')
'''showTree('(ROOT (N Feriado)(STOP (PREP com))(STOP (PROADJ meu))(WE (N amor)(EM Positive Anticipation Joy Sadness Trust))(EXC (! !))(HASH .)(U .))', 'sample-tweet')
showTree('(WE (N amor)(EM Positive Anticipation Joy Sadness Trust))', 'we-subtree')
showTree('(ET (. 😨)(EM Disgust Fear Sadness))', 'et-subtree')
showTree('(STOP (ART um))', 'stop-subtree')
showTree('(NOT (ADV nunca))', 'not-subtree')
showTree('(EXC (! !))', 'exc-subtree')
showTree('(INT (? ?))', 'int-subtree')
showTree('(T ($ $))', 't-subtree')'''
