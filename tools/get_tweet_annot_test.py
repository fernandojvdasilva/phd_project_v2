'''
Tool for testing getting tweets from the server for annotation.

@author:     fernando

@copyright:  All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
'''

import sys
import os
from apiclient.discovery import build
import pprint

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from tweets.tweet_corpus import *

__all__ = []
__version__ = 0.1
__date__ = '2016-06-27'
__updated__ = '2016-06-27'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def get_tweet_annot(annotator_id):

        api_root = 'https://ibov-twitter.appspot.com/_ah/api'
        api = 'getTweetAnnot'        
        version = '1'
        discovery_url = '%s/discovery/v1/apis/%s/%s/rest' % (api_root, api, version)
        
        sys.stdout.write(discovery_url)
        
        body_data = {
                     "annotator_id": annotator_id
                     }


        service = build(api, version, discoveryServiceUrl=discovery_url)

        try:
            response = service.get().tweet(body=body_data).execute()
            upload_ok = True
            num_tries = 0
            pprint.pprint(response)
        except Exception as ex:                
            sys.stderr.write("\n" + repr(ex) + "\n")
                

        
            




def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  All rights reserved.

  Licensed under the GPL License  

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-a", "--annotator_id", dest="annotator_id", help="Annotator ID" )
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        
        # Process arguments
        args = parser.parse_args()
        
        annotator_id = args.annotator_id
        
        get_tweet_annot(annotator_id)
        
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        #if DEBUG or TESTRUN:
        #    raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    #if DEBUG:
    #    sys.argv.append("-h")
    #    sys.argv.append("-v")
    #    sys.argv.append("-r")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.tweets_upload_gae_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())