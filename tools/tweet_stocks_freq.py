#!/usr/local/bin/python2.7
# encoding: utf-8
'''
tools.tweet_stocks_freq -- shortdesc

tools.tweet_stocks_freq is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2015 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from tweets import tweet_corpus, tweet_local
from tweets.tweet_corpus import TweetCorpus

__all__ = []
__version__ = 0.1
__date__ = '2015-01-23'
__updated__ = '2015-01-23'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

STOCK_CODES = [ "ABEV3", \
            "AEDU3", \
        "ALLL3", \
        "BBAS3", \
        "BBDC3", \
        "BBDC4", \
        "BBSE3", \
        "BISA3", \
        "BRAP4", \
        "BRFS3", \
        "BRKM5", \
        "BRML3", \
        "BRPR3", \
        "BVMF3", \
        "CCRO3", \
        "CESP6", \
        "CIEL3", \
        "CMIG4", \
        "CPFE3", \
        "CPLE6", \
        "CRUZ3", \
        "CSAN3", \
        "CSNA3", \
        "CTIP3", \
        "CYRE3", \
        "DASA3", \
        "DTEX3", \
        "ECOR3", \
        "ELET3", \
        "ELET6", \
        "ELPL4", \
        "EMBR3", \
        "ENBR3", \
        "ESTC3", \
        "EVEN3", \
        "FIBR3", \
        "GFSA3", \
        "GGBR4", \
        "GOAU4", \
        "GOLL4", \
        "HGTX3", \
        "HYPE3", \
        "ITSA4", \
        "ITUB4", \
        "JBSS3", \
        "KLBN11",\
        "KLBN4", \
        "KROT3", \
        "LAME4", \
        "LIGT3", \
        "LLXL3", \
        "LREN3", \
        "MRFG3", \
        "MRVE3", \
        "NATU3", \
        "OIBR4", \
        "PCAR4", \
        "PDGR3", \
        "PETR3", \
        "PETR4", \
        "QUAL3", \
        "RENT3", \
        "RSID3", \
        "SANB11", \
        "SBSP3", \
        "SUZB5", \
        "TBLE3", \
        "TIMP3", \
        "UGPA3", \
        "USIM5", \
        "VALE3", \
        "VALE5", \
        "VIVT4"]


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by user_name on %s.
  Copyright 2015 organization_name. All rights reserved.

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument(dest="corpus_path", 
                            help="Corpus path", metavar="corpus_path")        
        
        parser.add_argument(dest="out_path", 
                            help="Directory in which the output CSV reports will be stored at the end", 
                            metavar="out_path")
                
        # Process arguments
        args = parser.parse_args()
        
        corpus_path = args.corpus_path
        out_path = args.out_path
        
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

        
    tweet_corpus = TweetCorpus()
    tweet_corpus.load_corpus(corpus_path)
    
    count_tweets_day = {}
    count_tweets_stock = {}
    
    sys.stdout.write('Counting tweets per day... \n')
    while tweet_corpus.next_tweet() != False:
        for stock_code in STOCK_CODES:
          if stock_code in tweet_corpus.curr_tweet['text']:                    
            date_str = str(tweet_corpus.curr_tweet['date'].date())
            
            if not date_str in count_tweets_day.keys():
                count_tweets_day[date_str] = []
            
            if not stock_code in count_tweets_day[date_str]:    
                count_tweets_day[date_str].append(stock_code)
                
            if not stock_code in count_tweets_stock.keys():
                count_tweets_stock[stock_code] = 0
                                            
            count_tweets_stock[stock_code] += 1        
             
    
    # Print stocks per day CSV file
    sys.stdout.write('Saving stocks per day report...\n')
    
    out_csv_stocks_day_filepath = out_path + '/stocks_per_day.csv'
    out_csv_stocks_day = open(out_csv_stocks_day_filepath, 'w')
    
    for count_tweet in count_tweets_day.iteritems():
            out_csv_stocks_day.write("%s;%d\n" % (count_tweet[0], len(count_tweet[1])))                
    
    out_csv_stocks_day.close()
    
    # Print total tweets per stock
    sys.stdout.write('Saving total tweets per stock report...\n')
    
    out_csv_total_stocks_filepath = out_path + '/tweets_per_stock.csv'
    out_csv_total_stocks = open(out_csv_total_stocks_filepath, 'ws')
    
    for count_tweet in count_tweets_stock.iteritems():
        out_csv_total_stocks.write("%s;%d\n" % (count_tweet[0], count_tweet[1]))
    
    out_csv_total_stocks.close()

    return 0

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.tweet_stocks_freq_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())