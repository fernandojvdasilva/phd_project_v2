



# Open the check tree format bug script's file and whenever it finds 'ERROR: ', then reads
# the previous line and store it in the blacklist
def find_bad_trees(log_file_path):
	log_file = open(log_file_path, 'r')
	result = set()

	print("reading log file...")

	log_line = log_file.readline()
	last_line = ''
	penult_line = ''	
	while log_line:
		if log_line.find('ERROR:') == 0:
			result.add(penult_line[penult_line.find('|BT|'):])

		penult_line = last_line
		last_line = log_line
		log_line = log_file.readline()


	log_file.close()

	print("%d bad trees found" % len(result))

	return result


def read_bad_trees(file_path, bad_trees):
	dataset_file = open(file_path, 'r')
	tmp_dataset_file = open(file_path + '.tmp', 'w')

	curr_tree = dataset_file.readline()
	while curr_tree:
		if not curr_tree[curr_tree.find('|BT|'):] in bad_trees:
			tmp_dataset_file.write(curr_tree)
		curr_tree = dataset_file.readline()

	tmp_dataset_file.close()
	dataset_file.close()


def rewrite_good_trees(file_path):
	dataset_file = open(file_path, 'w')
	tmp_dataset_file = open(file_path + '.tmp', 'r')	

	curr_tree = tmp_dataset_file.readline()
	while curr_tree:
		dataset_file.write(curr_tree)
		curr_tree = tmp_dataset_file.readline()
	
	tmp_dataset_file.close()
	dataset_file.close()



# Open dataset files and delete blacklisted samples
def delete_bad_trees(label, bad_trees):
	file_paths = ['../datasets/tweets_%s_svmlight_%s_vs_%s.txt' % (label, emo[0], emo[1]) \
	for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]]
	file_paths.extend(['../datasets/tweets_%s_svmlight_%s_vs_%s_neutral.txt' % (label, emo[0], emo[1]) \
	for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]])

	for file_path in file_paths:
		print("Reading bad trees from %s" % file_path)
		read_bad_trees(file_path, bad_trees)
		print("Rewriting good trees to the original file")		
		rewrite_good_trees(file_path)
		print("File %s fixed!" % file_path)



def ignore_bad_trees(log_file_path, label):	
	bad_trees = find_bad_trees(log_file_path)
	delete_bad_trees(label, bad_trees)


ignore_bad_trees('../logs/check_tree_format_bug_test_set.log', 'auto-tagged-emolex_test')
ignore_bad_trees('../logs/check_tree_format_bug_test_set_stock.log', 'stocks_emolex')




