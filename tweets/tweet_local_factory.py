'''
Created on 30/08/2014

@author: fernando
'''
from tweet_local import TweetLocal
from tweet_local_field_parser import *

class TweetLocalFactory(object):
    '''
    This class is responsable for creating
    TweetLocal objects from a given .csv
    file.
    '''

    @staticmethod
    def create_tweet_local(tweet_str):
        result = None

        fields_parser = [TweetLocalTextFieldParser("text", "is_quote_status"), \
                         TweetLocalIdFieldParser("id", "favorite_count"), \
                         TweetLocalUserIdFieldParser("user_id", "_api"), \
                         TweetLocalFollowersFieldParser("followers", "protected"), \
                         TweetLocalFriendsFieldParser("friends", ""), \
                         TweetLocalDateFieldParser("date", "")]
        
        props = {}
        for field in fields_parser:
            props[field.field_name] = field.parse_field(tweet_str)
         
        result = TweetLocal(props)
                
        return result
        