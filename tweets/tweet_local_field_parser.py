'''
Created on 01/09/2014

@author: fernando
'''
import datetime

class TweetLocalFieldParser(object):
    '''
    Superclass indicating common behavior for field parser
    '''

    def __init__(self, field_name, next_field_name):
        self.field_name = field_name
        self.next_field_name = next_field_name
    
    def parse_field(self, tweet_str):
        pass
    
    def extract_field(self, tweet_str, key, delimiter):
        key_pos = tweet_str.find(key)
        delimiter_pos = tweet_str.find(delimiter, key_pos)
        
        result = tweet_str[key_pos+len(key):delimiter_pos]
        
        return result
        
    
class TweetLocalTextFieldParser(TweetLocalFieldParser):
    '''
    Retrieve the Tweet Text field.
    '''

    TWEET_LOCAL_TEXT_KEY = 'text='
    TWEET_LOCAL_TEXT_DELIMITER = "', "
    TWEET_LOCAL_TEXT_ALT_DELIMITER = '", '
        
    def parse_field(self, tweet_str):
        key_pos = tweet_str.find(TweetLocalTextFieldParser.TWEET_LOCAL_TEXT_KEY)
        
        ''' Some tweets are surrounded by \' and others by " '''
        if tweet_str[key_pos+len(TweetLocalTextFieldParser.TWEET_LOCAL_TEXT_KEY)+1] == '"':
            delimiter_pos = tweet_str.find(TweetLocalTextFieldParser.TWEET_LOCAL_TEXT_ALT_DELIMITER + self.next_field_name, \
                                       key_pos)
        else: 
            delimiter_pos = tweet_str.find(TweetLocalTextFieldParser.TWEET_LOCAL_TEXT_DELIMITER + self.next_field_name, \
                                       key_pos)
        
        
        result = eval(tweet_str[key_pos+\
                        len(TweetLocalTextFieldParser.TWEET_LOCAL_TEXT_KEY):\
                        delimiter_pos+1])
        
        return result

        
class TweetLocalIdFieldParser(TweetLocalFieldParser):
    '''
    Retrieve the Tweet ID
    '''        
    
    TWEET_LOCAL_ID_KEY = ' id='
    TWEET_LOCAL_ID_DELIMITER = ', '
    
    def parse_field(self, tweet_str):
        result = long(self.extract_field(tweet_str, \
                        TweetLocalIdFieldParser.TWEET_LOCAL_ID_KEY, \
                        TweetLocalIdFieldParser.TWEET_LOCAL_ID_DELIMITER))
        
        return result
        

class TweetLocalUserIdFieldParser(TweetLocalFieldParser):
    '''
    Retrieve the author's User Id
    '''    
    TWEET_LOCAL_AUTHOR_KEY='author=User('
    TWEET_LOCAL_USER_ID_KEY=' id='
    TWEET_LOCAL_USER_ID_DELIMITER= ', '
    
    def parse_field(self, tweet_str):
        author_key_pos = tweet_str.find(\
                TweetLocalUserIdFieldParser.TWEET_LOCAL_AUTHOR_KEY)
        
        ''' The author's User id is the first id field after the author entry
        '''
        user_id_pos = tweet_str.find( \
                TweetLocalUserIdFieldParser.TWEET_LOCAL_USER_ID_KEY,
                author_key_pos)
        
        delimiter_pos = tweet_str.find(\
                TweetLocalUserIdFieldParser.TWEET_LOCAL_USER_ID_DELIMITER,
                user_id_pos)
        
        result = long(tweet_str[ \
                user_id_pos + \
                len(TweetLocalUserIdFieldParser.TWEET_LOCAL_USER_ID_KEY): \
                delimiter_pos])
        
        return result
    
    
class TweetLocalFollowersFieldParser(TweetLocalFieldParser):
    '''
    Retrieve the number of followers of the author
    '''        
    
    TWEET_LOCAL_FOLLOWERS_KEY = ' followers_count='
    TWEET_LOCAL_FOLLOWERS_DELIMITER = ', '
    
    def parse_field(self, tweet_str):
        result = int(self.extract_field(tweet_str, \
                TweetLocalFollowersFieldParser.TWEET_LOCAL_FOLLOWERS_KEY, \
                TweetLocalFollowersFieldParser.TWEET_LOCAL_FOLLOWERS_DELIMITER))
        
        return result
    
class TweetLocalFriendsFieldParser(TweetLocalFieldParser):
    TWEET_LOCAL_FRIENDS_KEY = ' friends_count='
    TWEET_LOCAL_FRIENDS_DELIMITER = ', '
    
    def parse_field(self, tweet_str):
        result = int(self.extract_field(tweet_str, \
                TweetLocalFriendsFieldParser.TWEET_LOCAL_FRIENDS_KEY, \
                TweetLocalFriendsFieldParser.TWEET_LOCAL_FRIENDS_DELIMITER))
        
        return result
    
class TweetLocalDateFieldParser(TweetLocalFieldParser):
    TWEET_LOCAL_DATE_KEY = ' created_at='
    TWEET_LOCAL_DATE_DELIMITER = '), '
    
    def parse_field(self, tweet_str):
        key_pos = tweet_str.rfind(
                TweetLocalDateFieldParser.TWEET_LOCAL_DATE_KEY)
        
        delimiter_pos = tweet_str.find(
                        TweetLocalDateFieldParser.TWEET_LOCAL_DATE_DELIMITER, \
                        key_pos)
                
        result = eval(tweet_str[key_pos + \
                len(TweetLocalDateFieldParser.TWEET_LOCAL_DATE_KEY): \
                delimiter_pos+1])
        
        return result