'''
Created on 30/08/2014

@author: fernando
'''
import os
from tweets.tweet_local import TweetLocal

class TweetCorpus(object):
    '''
    This class represents the corpus, containing
    several tweets.
    '''            


    def __init__(self):
        '''
        Constructor
        '''
        self.tweets = []
        self.tweet_paths = []
        self.curr_tweet = TweetLocal({})
        self.curr_tweet_index = 0

    def get_tweets(self):
        return self.__tweets


    def set_tweets(self, value):
        self.__tweets = value


    def del_tweets(self):
        del self.__tweets

    tweets = property(get_tweets, set_tweets, del_tweets, "tweets's that compose the corpus")
    
    def load_corpus(self, corpus_dir):
        self.corpus_dir = corpus_dir

        self.tweet_paths = []

        for data_dir in os.listdir("%s/%s" % (corpus_dir, "data")):
            for tweet_file in os.listdir("%s/%s/%s" % (corpus_dir, "data", data_dir)): 
                self.tweet_paths.append("%s/%s/%s/%s" % (corpus_dir, "data", data_dir, tweet_file))

    def get_curr_tweet_path(self):
        return self.tweet_paths[self.curr_tweet_index]

    def first_tweet(self):
        self.curr_tweet_index = 0        
        self.curr_tweet.load(self.tweet_paths[0])            
        return self.curr_tweet
    
    def next_tweet(self):
        self.curr_tweet_index += 1
        
        if self.curr_tweet_index == len(self.tweet_paths):
            return False
                                
        self.curr_tweet.load(self.tweet_paths[self.curr_tweet_index])        
        return self.curr_tweet        
    
    def prev_tweet(self):
        self.curr_tweet_index -= 1
        
        if self.curr_tweet_index < 0:
            return False
                        
        self.curr_tweet.load(self.tweet_paths[self.curr_tweet_index])        
        return self.curr_tweet
    
    def last_tweet(self):
        self.curr_tweet_index = len(self.tweet_paths)-1
        self.curr_tweet.load(self.tweet_paths[self.curr_tweet_index])
        return self.curr_tweet
    
    
    def create_dirs(self, output_dir):
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        
        if output_dir[-1] != '/':
            output_dir += '/'
        
        data_dir = output_dir + 'data'
        metadata_dir = output_dir + 'metadata'
        annotation_dir = output_dir + 'annotation'
        manual_annotation_dir = annotation_dir + '/manual'
        auto_annotation_dir = annotation_dir + '/auto'
        
        if not os.path.exists(data_dir):
            os.mkdir(data_dir)
        
        if not os.path.exists(metadata_dir):
            os.mkdir(metadata_dir)
            
        if not os.path.exists(annotation_dir):
            os.mkdir(annotation_dir)
            
        if not os.path.exists(manual_annotation_dir):
            os.mkdir(manual_annotation_dir)
        
        if not os.path.exists(auto_annotation_dir):
            os.mkdir(auto_annotation_dir)
                
        return data_dir, metadata_dir, annotation_dir
        
            
    def save(self, output_dir):
        if len(self.tweets) > 0:
            data_dir, metadata_dir, annotation_dir = \
                self.create_dirs(output_dir)
            
            for tweet in self.tweets:
                data_month_dir = "%s/%d-%.2d" % \
                                (data_dir, \
                                 tweet.get_props()['date'].year, \
                                 tweet.get_props()['date'].month)
                
                if not os.path.exists(data_month_dir):
                    os.mkdir(data_month_dir)
                
                
                tweet.persist(data_month_dir)
    
        