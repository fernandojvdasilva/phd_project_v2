'''
Created on 30/08/2014

@author: fernando
'''
import json
import os
import sys
from json.encoder import JSONEncoder
import datetime
from dateutil import parser as dateparser

class TweetLocalEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        else:
            return super(TweetLocalEncoder, self).default(obj)

class TweetLocal(object):
    '''
    This class represents a tweet stored locally as
    part of corpus for experiments 
    '''

    def __init__(self, properties):
        '''
        Constructor
        '''
        self.props = properties
        self.manual_annotation = None

    def get_props(self):
        return self.__props


    def set_props(self, value):
        self.__props = value


    def del_props(self):
        del self.__props

    
    def json_filename(self):
        filename = "%d%.2d%.2d-%ul.json" % (self.props['date'].year, \
                                        self.props['date'].month, \
                                        self.props['date'].day, \
                                        self.props['id'])
        return filename
        
    @staticmethod
    def decode_json(dict):
        json_dict = {}
        json_dict['user_id'] = long(dict['user_id'])
        json_dict['text'] = dict['text']
        json_dict['followers'] = int(dict['followers'])
        json_dict['date'] = dateparser.parse(dict['date'])
        json_dict['friends'] = int(dict['friends'])
        json_dict['id'] = long(dict['id'])
        
        return json_dict 
    
    def load(self, json_path):
        json_file = open(json_path)
        result = True
        
        if json_file:
            json_str = json_file.read()        
            self.props = json.loads(json_str, object_hook=TweetLocal.decode_json)                
        else:
            result = False
        
        self.manual_annotation = None    
        return result
                    
    
    def persist(self, json_path, overwrite=False):
        if json_path[-1] != '/':
            json_path += '/'
        
        json_path += self.json_filename()
        
        if overwrite or not os.path.exists(json_path):
            '''json_str = json.dumps(self.props, indent=4, sort_keys=True)'''
            json_str = TweetLocalEncoder().encode(self.props)
                
            json_file = open(json_path, 'w')
            json_file.write(json_str)
    props = property(get_props, set_props, del_props, "props's docstring")
            
    
    def __getitem__(self, key):
        return self.props[key]