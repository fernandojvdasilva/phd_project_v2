# encoding: utf-8
import re
import hashlib
import os

spammers_list = [u'umrei']


class TweetsFilterRepeated:

    def __init__(self, corpus):
        self.corpus = corpus
        self.repeated_paths = []
        self.repeated_ids = []

    def find_repeated(self):

        tweet_hashes = []

        tweet = self.corpus.first_tweet()
        while tweet != False:            
            spam = False
            
            # Remove any tweet written by a spammer
            for spammer in spammers_list:                
                if spammer in tweet['text'].lower():
                    self.repeated_paths.append(self.corpus.get_curr_tweet_path())
                    self.repeated_ids.append(tweet['id'])
                    spam = True
                    break
            
            # Remove URL and RT marks then calculate the hash
            if not spam:
                no_url_and_rt_tweet = re.sub(r'https?:\/\/.*\/.*', '', tweet['text'], flags=re.MULTILINE)
                no_url_and_rt_tweet = re.sub(r'rt @.+:', '', no_url_and_rt_tweet, flags=re.MULTILINE)
    
                md5_hasher = hashlib.md5()
                md5_hasher.update(no_url_and_rt_tweet.encode('utf-8'))
    
                tweet_hash = int(md5_hasher.hexdigest(), 16)
    
                if tweet_hash in tweet_hashes:
                    self.repeated_paths.append(self.corpus.get_curr_tweet_path())
                    self.repeated_ids.append(tweet['id'])
                else:
                    tweet_hashes.append(tweet_hash)

            tweet = self.corpus.next_tweet()

        return self.repeated_ids

    def delete_repeated(self):
        for path in self.repeated_paths:
            os.remove(path)