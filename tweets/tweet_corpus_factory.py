'''
Created on 30/08/2014

@author: fernando
'''

import csv
from tweet_corpus import TweetCorpus
from tweet_local_factory import TweetLocalFactory
from tweet_local import TweetLocal

CSV_TWEET_JSON_PREFIX = 'Status('
CSV_TWEET_JSON_POSTFIX = ')'

class TweetCorpusFactory(object):
    '''
    Creates the tweet corpus from a csv file
    The csv file format is the one retrieved from the 
    bulk data stored at google engine.
    In this case, there are three fields:
        - content: the whole tweet in Json format
        - date: the date in which the tweet was collected
        - tweet_id: the id of the tweet
        - key: the id of the entry on google engine's database
        - stock: the stock mentioned by the tweet
    '''
    @staticmethod
    def create_tweet_corpus(csv_file_path):                
        csv_file = open(csv_file_path)
        
        if csv_file != None:
            result = TweetCorpus()
        
            csv_dialect = csv.excel()
            csv_dialect.delimiter = ','
            csv_dialect.quotechar = '"'
            
            csv_reader = csv.reader(csv_file, dialect=csv_dialect)
            
            skip_header_row = True
            for row in csv_reader:                
                if skip_header_row:
                    skip_header_row = False
                    continue                
                tweet_str = row[0][len(CSV_TWEET_JSON_PREFIX):\
                                  len(row[0]) - len(CSV_TWEET_JSON_POSTFIX)]                
                tweet_local = TweetLocalFactory.create_tweet_local(tweet_str)
                result.get_tweets().append(tweet_local)
                
        else:
            result = None
        
        return result