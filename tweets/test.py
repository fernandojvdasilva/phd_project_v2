# coding: utf-8
'''
Created on 05/09/2014

@author: fernando
'''
import unittest

from tweet_local_factory import TweetLocalFactory
from tweet_local import TweetLocal

import datetime

class Test(unittest.TestCase):


    def testTweetLocalFactory(self):
        
        csv_strings = []
        expected_values = []
        
        csv_strings.append( "Status(contributors=None, truncated=False, \
        text=u'Ativo c/ vol Financeiro Superior \xe0 sua MM21-16h: ITUB4 JBSS3 JHSF3 KLBN4 LAME3 LIGT3 LUPA3 MDIA3 MMXM11 MMXM3 PETR3 PETR4 PFRM3 PIBB11 QUAL3', \
        in_reply_to_status_id=None, id=476082430437519361L, favorite_count=0, \
        _api=<tweepy.api.API object at 0x10cf11d0>, author=User(follow_request_sent=False, \
        profile_use_background_image=True, id=86310384, _api=<tweepy.api.API object at 0x10cf11d0>, \
        verified=False, profile_text_color=u'333333', \
        profile_image_url_https=u'https://pbs.twimg.com/profile_images/378800000434298112/de660b6e374bc6d0f19074bca0a711af_normal.jpeg', \
        profile_sidebar_fill_color=u'EFEFEF', is_translator=False, geo_enabled=True, \
        entities={u'url': {u'urls': [{u'url': u'http://t.co/amKYDXOE11', \
        u'indices': [0, 22], u'expanded_url': u'http://www.wallinvest.com.br', \
        u'display_url': u'wallinvest.com.br'}]}, u'description': {u'urls': \
        [{u'url': u'http://t.co/amKYDXOE11', u'indices': [87, 109], \
        u'expanded_url': u'http://www.wallinvest.com.br', \
        u'display_url': u'wallinvest.com.br'}]}}, followers_count=371, protected=False, \
        location=u'Vit\xf3ria / ES / Brasil', default_profile_image=False, id_str=u'86310384', \
        lang=u'pt', utc_offset=-10800, statuses_count=10295, \
        description=u'Autor: do livro Aprenda a Investir em Bolsa de Valores - Ed. Ci\xeancia Moderna e \
        do site http://t.co/amKYDXOE11\r\nForma\xe7\xe3o: Engenharia El\xe9trica e Direito', \
        friends_count=432, profile_link_color=u'009999', \
        profile_image_url=u'http://pbs.twimg.com/profile_images/378800000434298112/de660b6e374bc6d0f19074bca0a711af_normal.jpeg', notifications=False, profile_background_image_url_https=u'https://abs.twimg.com/images/themes/theme14/bg.gif', profile_background_color=u'131516', profile_background_image_url=u'http://abs.twimg.com/images/themes/theme14/bg.gif', name=u'Dr Wall', is_translation_enabled=False, profile_background_tile=True, favourites_count=15, screen_name=u'WallFigueiredo', url=u'http://t.co/amKYDXOE11', created_at=datetime.datetime(2009, 10, 30, 13, 20, 20), contributors_enabled=False, time_zone=u'Brasilia', profile_sidebar_border_color=u'EEEEEE', default_profile=False, following=False, listed_count=16), retweeted=False, coordinates=None, entities={u'symbols': [], u'user_mentions': [], u'hashtags': [], u'urls': []}, in_reply_to_screen_name=None, in_reply_to_user_id=None, retweet_count=0, id_str=u'476082430437519361', favorited=False, source_url=u'http://twitter.com', user=User(follow_request_sent=False, profile_use_background_image=True, id=86310384, _api=<tweepy.api.API object at 0x10cf11d0>, verified=False, profile_text_color=u'333333', profile_image_url_https=u'https://pbs.twimg.com/profile_images/378800000434298112/de660b6e374bc6d0f19074bca0a711af_normal.jpeg', profile_sidebar_fill_color=u'EFEFEF', is_translator=False, geo_enabled=True, entities={u'url': {u'urls': [{u'url': u'http://t.co/amKYDXOE11', u'indices': [0, 22], u'expanded_url': u'http://www.wallinvest.com.br', u'display_url': u'wallinvest.com.br'}]}, u'description': {u'urls': [{u'url': u'http://t.co/amKYDXOE11', u'indices': [87, 109], u'expanded_url': u'http://www.wallinvest.com.br', u'display_url': u'wallinvest.com.br'}]}}, followers_count=371, protected=False, location=u'Vit\xf3ria / ES / Brasil', default_profile_image=False, id_str=u'86310384', lang=u'pt', utc_offset=-10800, statuses_count=10295, description=u'Autor: do livro Aprenda a Investir em Bolsa de Valores - Ed. Ci\xeancia Moderna e do site http://t.co/amKYDXOE11\r\nForma\xe7\xe3o: Engenharia El\xe9trica e Direito', friends_count=432, profile_link_color=u'009999', profile_image_url=u'http://pbs.twimg.com/profile_images/378800000434298112/de660b6e374bc6d0f19074bca0a711af_normal.jpeg', \
        notifications=False, profile_background_image_url_https=u'https://abs.twimg.com/images/themes/theme14/bg.gif', profile_background_color=u'131516', profile_background_image_url=u'http://abs.twimg.com/images/themes/theme14/bg.gif', name=u'Dr Wall', is_translation_enabled=False, \
        profile_background_tile=True, favourites_count=15, screen_name=u'WallFigueiredo', \
        url=u'http://t.co/amKYDXOE11', created_at=datetime.datetime(2009, 10, 30, 13, 20, 20), \
        contributors_enabled=False, time_zone=u'Brasilia', profile_sidebar_border_color=u'EEEEEE', \
        default_profile=False, following=False, listed_count=16), geo=None, in_reply_to_user_id_str=None, \
        lang=u'pt', created_at=datetime.datetime(2014, 6, 9, 19, 24, 31), in_reply_to_status_id_str=None, \
        place=None, source=u'Twitter Web Client', metadata={u'iso_language_code': u'pt', u'result_type': u'recent'})'")

        expected_values.append({'text':eval("u'Ativo c/ vol Financeiro Superior \xe0 sua MM21-16h: ITUB4 JBSS3 JHSF3 KLBN4 LAME3 LIGT3 LUPA3 MDIA3 MMXM11 MMXM3 PETR3 PETR4 PFRM3 PIBB11 QUAL3'"), \
                                'id':476082430437519361, \
                                'user_id':86310384, \
                                'followers':371, \
                                'friends':432, \
                                'date':datetime.datetime(2014, 6, 9, 19, 24, 31)})

        csv_strings.append("Status(contributors=None, truncated=False, \
        text=u\"$VALE5 - Eike Batista's MMX Mineracao e Metalicos Joins Ibovespa Index http://t.co/fvhLie0skC\", \
        in_reply_to_status_id=None, id=463388063209885696L, favorite_count=0, _api=<tweepy.api.API object at 0x10bb6270>, \
        author=User(follow_request_sent=False, profile_use_background_image=True, id=80084273, \
        _api=<tweepy.api.API object at 0x10bb6270>, verified=False, profile_text_color=u'3C3940', \
        profile_image_url_https=u'https://pbs.twimg.com/profile_images/1196309978/DSC00609_1632x1224_normal.jpg', \
        profile_sidebar_fill_color=u'95E8EC', is_translator=False, geo_enabled=True, \
        entities={u'description': {u'urls': []}}, followers_count=52, protected=False, location=u'DF', \
        default_profile_image=False, id_str=u'80084273', lang=u'pt', utc_offset=-10800, \
        statuses_count=3497, description=u'', friends_count=82, profile_link_color=u'0099B9', \
        profile_image_url=u'http://pbs.twimg.com/profile_images/1196309978/DSC00609_1632x1224_normal.jpg', \
        notifications=False, profile_background_image_url_https=u'https://abs.twimg.com/images/themes/theme1/bg.png', \
        profile_background_color=u'0099B9', profile_background_image_url=u'http://abs.twimg.com/images/themes/theme1/bg.png', \
        name=u'Alessandro Oliveira', is_translation_enabled=False, profile_background_tile=False, \
        favourites_count=15, screen_name=u'aleoc', url=None, created_at=datetime.datetime(2009, 10, 5, 17, 56, 54), \
        contributors_enabled=False, time_zone=u'Brasilia', profile_sidebar_border_color=u'5EBCDB', \
        default_profile=False, following=False, listed_count=0), retweeted=False, coordinates=None, \
        entities={u'symbols': [], u'user_mentions': [], u'hashtags': [], u'urls': \
        [{u'url': u'http://t.co/fvhLie0skC', u'indices': [71, 93], u'expanded_url': \
        u'http://br.advfn.com/noticias/DJN/2014/artigo/62079713?adw=1126416', u'display_url': \
        u'br.advfn.com/noticias/DJN/2\u2026'}]}, in_reply_to_screen_name=None, in_reply_to_user_id=None, \
        retweet_count=0, id_str=u'463388063209885696', favorited=False, source_url=u'http://www.advfn.com', \
        user=User(follow_request_sent=False, profile_use_background_image=True, id=80084273, \
        _api=<tweepy.api.API object at 0x10bb6270>, verified=False, profile_text_color=u'3C3940', \
        profile_image_url_https=u'https://pbs.twimg.com/profile_images/1196309978/DSC00609_1632x1224_normal.jpg', \
        profile_sidebar_fill_color=u'95E8EC', is_translator=False, geo_enabled=True, \
        entities={u'description': {u'urls': []}}, followers_count=52, protected=False, location=u'DF', \
        default_profile_image=False, id_str=u'80084273', lang=u'pt', utc_offset=-10800, statuses_count=3497, \
        description=u'', friends_count=82, profile_link_color=u'0099B9', \
        profile_image_url=u'http://pbs.twimg.com/profile_images/1196309978/DSC00609_1632x1224_normal.jpg', \
        notifications=False, profile_background_image_url_https=u'https://abs.twimg.com/images/themes/theme1/bg.png', \
        profile_background_color=u'0099B9', profile_background_image_url=u'http://abs.twimg.com/images/themes/theme1/bg.png', \
        name=u'Alessandro Oliveira', is_translation_enabled=False, profile_background_tile=False, \
        favourites_count=15, screen_name=u'aleoc', url=None, created_at=datetime.datetime(2009, 10, 5, 17, 56, 54), \
        contributors_enabled=False, time_zone=u'Brasilia', profile_sidebar_border_color=u'5EBCDB', \
        default_profile=False, following=False, listed_count=0), geo=None, in_reply_to_user_id_str=None, \
        possibly_sensitive=False, lang=u'pt', created_at=datetime.datetime(2014, 5, 5, 18, 41, 38), \
        in_reply_to_status_id_str=None, place=None, source=u'ADVFN News Alert', \
        metadata={u'iso_language_code': u'pt', u'result_type': u'recent'})")

        expected_values.append({'text':eval("u\"$VALE5 - Eike Batista's MMX Mineracao e Metalicos Joins Ibovespa Index http://t.co/fvhLie0skC\""), \
                                'id':463388063209885696L, \
                                'user_id':80084273, \
                                'followers':52, \
                                'friends':82, \
                                'date':datetime.datetime(2014, 5, 5, 18, 41, 38)})

        for tweet_str, expected in zip(csv_strings, expected_values):

            tweet_local = TweetLocalFactory.create_tweet_local(tweet_str)
            
            self.assertFalse(tweet_local == None, "TweetLocal created an empty object\n")
            
            self.assertEqual(tweet_local.props['text'], \
                             expected['text'], \
                             "Parsed text is incorrect! Value obtained is %s\n" % \
                             tweet_local.props['text'])
            
            self.assertEqual(tweet_local.props['id'], \
                            expected['id'],
                            "Parsed Tweet ID is incorrect! Value obtained is %d\n" % \
                            tweet_local.props['id'])
            
            self.assertEqual(tweet_local.props['user_id'],
                             expected['user_id'] , \
                             "Parsed Tweet User ID is incorrect! Value obtained is %d\n" % \
                             tweet_local.props['user_id'])
            
            self.assertEqual(tweet_local.props['followers'], \
                             expected['followers'], \
                             "Parsed Tweet User Followers is incorrect! Value obtained is %d\n" % \
                             tweet_local.props['followers'])
            
            self.assertEqual(tweet_local.props['friends'], \
                             expected['friends'], \
                             "Parsed Tweet User Friends is incorrect! Value obtained is %d\n" % \
                             tweet_local.props['friends'])
    
            self.assertEqual(tweet_local.props['date'], \
                             expected['date'], \
                             "Tweet Date is incorrect!\n")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testTweetLocalFactory']
    unittest.main()