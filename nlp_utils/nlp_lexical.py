'''
Created on 24/02/2016

@author: fernando
'''

import nltk
import string

pt_stemmer = nltk.stem.RSLPStemmer() # Using Portuguese
def lex_stem(tokens, stemmer):
    stemmed = []
    for item in tokens:
        if len(item) > 0:
            stemmed.append(stemmer.stem(item))
    return stemmed

def lex_tokenize(sentence):    
    return nltk.word_tokenize(sentence, 'portuguese')

def lex_stopwords_removal(tokens):
    stopwords = nltk.corpus.stopwords.words('portuguese')
    u_stopwords = [w for w in stopwords]
    tokens = [t for t in tokens if t not in u_stopwords]
    
    return tokens

def lex_remove_punctuations(token):   
    no_punctuation = [c for c in token if c not in string.punctuation]
    no_punctuation = ''.join(no_punctuation)
    return no_punctuation