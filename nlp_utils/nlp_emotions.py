# encoding: utf-8
'''
Created on 23/03/2016

@author: fernando
'''
from nlp_utils.EmoticonTagger.tagger import EmoticonTagger
from nlp_utils.NRCEmolexTagger.tagger import NRCEmolexTagger

HASHTAG_STR = u"#"


JOY_HASHTAGS = [HASHTAG_STR + u"feliz", # JOY (Alegria) \
                     HASHTAG_STR + u"contente", \
                     HASHTAG_STR + u"alegre", \
                    HASHTAG_STR + u"alegria", \
                    HASHTAG_STR + u"felicidade",\
                     HASHTAG_STR + u"encantado", # "encanto" and "encantado" usually refer to places, not emotions. \
                     HASHTAG_STR + u"encanto",\
                     HASHTAG_STR + u"prazeroso", # "prazeroso" didn't find reasonable matches \
                     HASHTAG_STR + u"delicia", # "delicia" and "delicioso" brings too many pornographic tweets \
                     HASHTAG_STR + u"delicioso"
                ]

SAD_HASHTAGS = [     HASHTAG_STR + u"triste", # SADNESS (Tristeza) \
                     HASHTAG_STR + u"chateado", \
                     HASHTAG_STR + u"chateada", \
                     HASHTAG_STR + u"magoado", \
                     HASHTAG_STR + u"magoada", \
                     HASHTAG_STR + u"desanimo", \
                     HASHTAG_STR + u"desanimado",\
                     HASHTAG_STR + u"desanimada",\
                     HASHTAG_STR + u"melancolico",\
                     HASHTAG_STR + u"melancólico",\
                     HASHTAG_STR + u"melancolia",\
                     HASHTAG_STR + u"deprimido",\
                     HASHTAG_STR + u"deprimida",\
                     HASHTAG_STR + u"depressao", # "Depressão" is often meant as something related to comedy due to \
                     HASHTAG_STR + u"depressão", # some internet "memes" and not always as emotional tweets \
                     HASHTAG_STR + u"deprimente",\
                     HASHTAG_STR + u"dor", # "Dor" (pain) doesn't seem to mean sadness, as there is a lot of people posting on pain for tatoos. \
                     HASHTAG_STR + u"dolorido",\
                     HASHTAG_STR + u"dolorida",\
                     HASHTAG_STR + u"doendo",\
                     HASHTAG_STR + u"sofrimento", # the same for "sofrimento" \
                     HASHTAG_STR + u"sofrendo",\
                     HASHTAG_STR + u"aflito", # "Aflito" is more related to expectation and sometimes fear, but doesn't show sadness. \
                     HASHTAG_STR + u"aflita",\
                     HASHTAG_STR + u"aflicao",\
                     HASHTAG_STR + u"aflição",\
                     HASHTAG_STR + u"pesar",\
                     HASHTAG_STR + u"pesames",\
                     HASHTAG_STR + u"pêsames", \
                     HASHTAG_STR + u"luto"
                     ]

FEAR_HASHTAGS = [HASHTAG_STR + u"medo", # Fear (Medo) \
                     HASHTAG_STR + u"commedo", \
                     HASHTAG_STR + u"empanico",\
                     HASHTAG_STR + u"aterrorizado",\
                     HASHTAG_STR + u"aterrorizada",\
                     HASHTAG_STR + u"terrível", # "Terrível" sometimes refer to something bad or with bad behaviour, but no usually to fear \
                     HASHTAG_STR + u"terrivel",\
                     HASHTAG_STR + u"apavorado",\
                     HASHTAG_STR + u"apavorada",\
                     HASHTAG_STR + u"espanto", # "Espanto" most of times means surprise but has some occurrencies with disgust \
                     HASHTAG_STR + u"susto",\
                     HASHTAG_STR + u"assustado",\
                     HASHTAG_STR + u"assustada",\
                     HASHTAG_STR + u"receio",\
                     HASHTAG_STR + u"receoso",\
                     HASHTAG_STR + u"receosa"
                     ]

ANGER_HASHTAGS = [HASHTAG_STR + u"raiva", # Anger (Raiva) \
                     HASHTAG_STR + u"comraiva", \
                     HASHTAG_STR + u"furia",   # "Furia" doesn't show any angry tweets \
                     HASHTAG_STR + u"fúria", \
                     HASHTAG_STR + u"furioso", # There are many tweets in which "furioso" is used as a slang for something "awesome" \
                     HASHTAG_STR + u"furiosa",\
                     HASHTAG_STR + u"irritado",\
                     HASHTAG_STR + u"irritada",\
                     HASHTAG_STR + u"irritação",\
                     HASHTAG_STR + u"aborrecido",\
                     HASHTAG_STR + u"aborrecida",\
                     HASHTAG_STR + u"incomodo", # "Incômodo" doesn't seem to be related to anger. \
                     HASHTAG_STR + u"incômodo",\
                     HASHTAG_STR + u"incomodado",\
                     HASHTAG_STR + u"incomodada"
                     ]

TRUST_HASHTAGS = [   # Trust (Confiança) \
                     HASHTAG_STR + u"confianca", # Removing #confiança (trust) because there is more non-emotional
                                                   # tweets which refer to the Brazilian soccer team Confiança and
                                                   # motivation tweets than real emotional tweets with this hashtag.
                     HASHTAG_STR + u"confiança", \
                     HASHTAG_STR + u"confio", \
                     HASHTAG_STR + u"confiavel", \
                     HASHTAG_STR + u"confiável", \
                     HASHTAG_STR + u"admiro", \
                     HASHTAG_STR + u"admirável", \
                     HASHTAG_STR + u"admiravel", \
                     HASHTAG_STR + u"admiração", \
                     HASHTAG_STR + u"admiracao", \
                     HASHTAG_STR + u"idolo", \
                     HASHTAG_STR + u"meuidolo"
                  ]

DISG_HASHTAGS = [HASHTAG_STR + u"desgosto", # Disgust (Desgosto) \
                     HASHTAG_STR + u"aversao", \
                     HASHTAG_STR + u"aversão", \
                     HASHTAG_STR + u"antipatia", \
                     HASHTAG_STR + u"antipatico", \
                     HASHTAG_STR + u"antipático", \
                     HASHTAG_STR + u"antipatica", \
                     HASHTAG_STR + u"antipática", \
                     HASHTAG_STR + u"antipatico", \
                     HASHTAG_STR + u"repugnante", \
                     HASHTAG_STR + u"repugnancia", \
                     HASHTAG_STR + u"repugnância", \
                     HASHTAG_STR + u"nojento", \
                     HASHTAG_STR + u"nojenta", \
                     HASHTAG_STR + u"nojo", \
                     HASHTAG_STR + u"despresivel",  # typo here: the correct is desprezível \
                     HASHTAG_STR + u"despresível", \
                      ]

SUR_HASHTAGS = [HASHTAG_STR + u"surpreso", # Surprise (Surpreso) \
                     HASHTAG_STR + u"surpresa", \
                     HASHTAG_STR + u"surpreendido", \
                     HASHTAG_STR + u"surpreendida", \
                     HASHTAG_STR + u"espantado",\
                     HASHTAG_STR + u"espantada",\
                     HASHTAG_STR + u"perplexo",\
                     HASHTAG_STR + u"perplexa",\
                     HASHTAG_STR + u"perplexidade",\
                     HASHTAG_STR + u"quemdiria",\
                     HASHTAG_STR + u"susto", \
                     HASHTAG_STR + u"assustado",\
                     HASHTAG_STR + u"assustada"
                ]

# Only "previsão" (prevision) really appear to bring previsions

ANTI_HASHTAGS = [    HASHTAG_STR + u"expectativa", # Anticipation (Antecipação) \
                     HASHTAG_STR + u"naexpectativa",\
                     HASHTAG_STR + u"curioso",\
                     HASHTAG_STR + u"curiosa",\
                     HASHTAG_STR + u"atento",\
                     HASHTAG_STR + u"atenta",\
                     HASHTAG_STR + u"interessado",\
                     HASHTAG_STR + u"interessada",\
                     HASHTAG_STR + u"interessante",\
                     HASHTAG_STR + u"deolho",\
                     HASHTAG_STR + u"monitorando",\
                     HASHTAG_STR + u"observando",\
                     HASHTAG_STR + u"analisando",\
                     HASHTAG_STR + u"previsao",\
                     HASHTAG_STR + u"previsão"
                 ]

def emo_remove_hashtags(text):
    for emo_hashtags in [JOY_HASHTAGS, SAD_HASHTAGS, \
                         TRUST_HASHTAGS, DISG_HASHTAGS, \
                         FEAR_HASHTAGS, ANGER_HASHTAGS, \
                         SUR_HASHTAGS, ANTI_HASHTAGS]:
        for hashtag in emo_hashtags:
            text = text.replace(hashtag, '')
            
    return text


def emo_tag_words(words):
    tagger = NRCEmolexTagger()
        
    tags = tagger.tagWordList(words, ['pt'])
    
    return tags


def emo_tag_emoticons(words):
    tagger = EmoticonTagger()
    
    tags = tagger.tagWordList(words)
    
    return tags