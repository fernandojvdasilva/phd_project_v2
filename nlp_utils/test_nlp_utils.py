'''
Created on 10 de jan de 2018

@author: fernando
'''
import unittest

from nlp_utils.corpus_analysis import fleiss_kappa
from nlp_utils.nlp_syntactic import syn_pos_tag
from nlp_utils.nlp_lexical import  lex_tokenize
from nlp_utils.nlp_semantic import *
import pandas as pd
from twisted.positioning import _sentence


class Test(unittest.TestCase):


    def testFleissKappa(self):
        d = {'TRU': [3, 1, 0, 2, 0, 0], 'DIS': [0, 2, 1, 0, 3, 0], 'neutral': [0, 0, 2, 1, 0, 3]}                
        df = pd.DataFrame(data=d)
        
        kappa = fleiss_kappa(df, 3, debug=True)
        print("kappa = %f" % kappa)
        
        assert(kappa != 0)

    def testSynPOSTag(self):
        sent = 'Os candidatos classificáveis dos cursos de Sistemas de Informação poderão ocupar as vagas remanescentes do Curso de Engenharia de Software.'
        
        tokens = lex_tokenize(sent)
        
        tags = syn_pos_tag(tokens, 'pt')
        
        for t in tags:
            for t_ in t:
                print(t_)                           
        
        assert(tags != None)

    def testSenTokenize(self):                
        import pprint
        samples = ['impressionante como tudo muda em 1 ano, pensamentos \
            planos, amizades, amores... mudanças são necessárias :d #feliz',
                    'até agora não me conformo que perdi queen onteeeeeeeeem, \
                    ah véi #chateada',
                    'Fevereiro 2014: Ainda não é o momento de entrar: VALE5, CSNA3 ou PETR4 \
                    Tendência e resultados favoráveis: ESTC3, PSSA3, EMBR3 e WEGE3.'
                    ]

        for s in samples:            
            tokens = sem_tokenize_stock_corpus(s)
            pprint.pprint(s)
            pprint.pprint(tokens)

            assert(tokens != None)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSynPOSTag']
    unittest.main()