'''
Created on 16/03/2016

@author: fernando
'''
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer

def df_tfidf_train_test_split(df, tfidf, rate):
    indexes = np.random.rand(len(df)) < rate
    
    X_train = tfidf[indexes]
    Y_train = df[indexes]
    
    X_test = tfidf[~indexes]
    Y_test = df[~indexes]
    
    return X_train, Y_train, X_test, Y_test

def df_train_test_split(df, rate):
    indexes = np.random.rand(len(df) < rate)
    
    df_train = df[indexes]
    df_test = df[~indexes]
    
    return df_train, df_test, indexes



def df_tfidf_cut_random(df, tfidf, size):
    indexes = np.random.randint(len(df), size=size)

    cut_df = df[indexes]
    cut_tfidf = tfidf[indexes]
    
    return cut_df, cut_tfidf

def df_cut_random(df, size):
    indexes = np.random.randint(len(df), size=size)
    
    cut_df = df[indexes]    
    
    return cut_df
    

def bow_vectorize(dataframe, dim_name, tokenizer_func):
    tfidf = TfidfVectorizer(tokenizer=tokenizer_func)
    tfs = tfidf.fit_transform(dataframe[dim_name])
    return tfidf, tfs