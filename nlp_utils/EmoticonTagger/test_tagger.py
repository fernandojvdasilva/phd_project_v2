'''
Created on 8 de jun de 2017

@author: fernando
'''
import unittest

from nlp_utils.EmoticonTagger.tagger import EmoticonTagger


class Test(unittest.TestCase):


    def testEmoticonTagger(self):
        tagger = EmoticonTagger()
        
        sample = 'rt da gi passando na tl a essa hora 😨 #medo 🏃🏃'
        res = tagger.tagWordList(sample.split(' '))        
        assert(res != None)
        
        sample = 'oi sexta  #feriadosohpraquempode #pqeutotrabalhando #chateada 😔  #boatarde #instagood #instapicture… https://t.co/rclxr1g0wn'
        res = tagger.tagWordList(sample.split(' '))        
        assert(res != None)
        
        sample = 'que feijão mais gostoso 😋😋😋 #cariocando #carioquice #errejota #feijão  #delícia #todosujo… https://t.co/pyzneoguks'
        res = tagger.tagWordList(sample.split(' '))                                    
        assert(res != None)

        sample = 'vizinho das plantas foi embora 😯 #luto #acabouotráfico #acaboubriganamadruga'
        res = tagger.tagWordList(sample.split(' '))                                    
        assert(res != None)

        sample = 'feriado com meu amor!❤ 😎 🍀💏🙏 #melhorcompanhia #amodemais #quandodeusquer #feliz #nossasenhoraabençoe https://t.co/ajcllvkkwi'
        res = tagger.tagWordList(sample.split(' '))                                    
        assert(res != None)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testEmoticonTagger']
    unittest.main()