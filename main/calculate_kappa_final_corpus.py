#!/usr/local/bin/python2.7
# encoding: utf-8
'''
main.tweets_crowd_csv_to_dataseries -- Convert CSV downloaded from tweets annotated by crowd-sourcing experiment into dataseries

main.tweets_crowd_csv_to_dataseries is a tool that convert CSV files downloaded from Google App Engine
and annotated by the crowd sourcing annotation experiment into a dataseries used by machine learning algorithms

@author:     fernando

@copyright:  2018 Fernando Vieira da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com

'''

import sys
import os

import pandas as pd
import pprint

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

from nlp_utils.corpus_analysis import *

__all__ = []
__version__ = 0.1
__date__ = '2018-09-17'
__updated__ = '2018-09-17'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg
    
  
def list_r_format(dataframe, num_raters):
    
        #raters_categories = [[] for _ in range(dataframe.shape[1] * num_raters)]
        raters_categories = [[] for _ in range(num_raters)]
    
        for index, row in dataframe.iterrows():
            i_col = 0      
            start_i = 0      
            for col in list(dataframe):
                i = start_i
                
                ''' KappaGUI Format
                for j in range(start_i):
                    raters_categories[(j*dataframe.shape[1])+i_col].append(0)
                
                while i < num_raters:
                    if i-start_i < row[col]:
                        raters_categories[(i*dataframe.shape[1])+i_col].append(1)
                    else:
                        raters_categories[(i*dataframe.shape[1])+i_col].append(0)
                                    
                    i += 1
                '''
                
                ''' irr Format '''
                while i < num_raters:
                    if i-start_i < row[col]:
                        raters_categories[i].append(i_col)
                    i += 1
                
                start_i += row[col]    
                i_col += 1
                
        r_format_str = '''
        scores <- data.frame(                
        ''' + ','.join(['c(' + ','.join([str(v) for v in s]) + ')\n' \
                                for s in raters_categories]) +  \
                                ')'      
        
        
        return r_format_str
                

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by fernando on %s.
  Copyright 2018 Fernando Vieira da Silva. All rights reserved.
  

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        
        parser.add_argument("-a", "--annots", dest="annots", help="Full path to the CSV file containing annotations (TweetAnnotationCrowd)" )
        parser.add_argument("-t", "--tweets", dest="tweets", help="Full path to the CSV file containing tweets stored in Google App Engine (TweetCrowd)" )                
        parser.add_argument("-i", "--final", dest="final", help="Full path to the CSV file containing the final corpus" )
        #parser.add_argument("-u", "--users", dest="users", help="Full path to the CSV file containing user profiles stored in Google App Engine (UserAnnotator)" )                
        parser.add_argument("-o", "--out", dest="out", help="Output directory" )

        # Process arguments
        args = parser.parse_args()

        annots_path = args.annots
        tweets_path = args.tweets
        final_path = args.final
        
        out_dir = args.out
                
        
        annots = pd.read_csv(annots_path)
        tweets = pd.read_csv(tweets_path)
        
        # Select tweets with 3 or more annotations only
        tweets = tweets.loc[tweets['num_annot'] >= 3]
        
        # Only tweets included in the final corpus
        final_corpus = pd.read_csv(final_path, engine='c', sep=';')
        
        
        final_corpus_subsets = {}
        
        final_corpus_subsets['trust_vs_disgust'] = final_corpus.loc[final_corpus['TRU'] > -1]
        
        final_corpus_subsets['joy_vs_sadness'] = final_corpus.loc[final_corpus['JOY'] > -1]
        
        final_corpus_subsets['surprise_vs_antecip'] = final_corpus.loc[final_corpus['SUR'] > -1]
        
        final_corpus_subsets['anger_vs_fear'] = final_corpus.loc[final_corpus['ANG'] > -1]
                                   
        
        # Select these tweets' annotations
        annots = annots.loc[annots['tweet_id'].isin(tweets['tweet_id'])]
        
        annotated_tweets = {}
        
        agreement_lists = {'trust_vs_disgust': {'TRU':[], 'DIS': [], 'neutral': [], 'dontknow': []},
                     'joy_vs_sadness': {'JOY':[], 'SAD': [], 'neutral': [], 'dontknow':[]},
                     'surprise_vs_antecip': {'ANT':[], 'SUR': [], 'neutral': [], 'dontknow': []},
                     'anger_vs_fear': {'ANG':[], 'FEA': [], 'neutral': [], 'dontknow': []}}
        
        sys.stdout.write("Reading annotations...\n")
        for index, tweet in tweets.iterrows():
            tweet_annots = annots.loc[annots['tweet_id'] == tweet['tweet_id']]            
            
            votes = {'trust_vs_disgust': {'TRU':0, 'DIS': 0, 'neutral': 0, 'dontknow': 0},
                     'joy_vs_sadness': {'JOY':0, 'SAD': 0, 'neutral': 0, 'dontknow': 0},
                     'surprise_vs_antecip': {'ANT':0, 'SUR': 0, 'neutral': 0, 'dontknow': 0},
                     'anger_vs_fear': {'ANG':0, 'FEA': 0, 'neutral': 0, 'dontknow': 0}}
            
            classes = []
            confidences = {}
            for index, tweet_annot in tweet_annots.iterrows():
                for key in votes.keys():
                    votes[key][tweet_annot[key]] += 1
                    
            all_neutral = True
            any_dontknow = False
            all_agree = True
            for annotation in votes.keys():
                choosen_annotation = ''
                max_num_votes = 0
                tie = False
                tie_num_votes = 0
                for tag in votes[annotation].keys():
                    if votes[annotation][tag] > max_num_votes:                        
                        max_num_votes = votes[annotation][tag]
                        choosen_annotation = tag
                        '''
                        if not full_agreement is None and full_agreement and \
                            (votes[annotation][tag] + votes[annotation]['dontknow']) == tweet['num_annot'] and \
                            votes[annotation]['dontknow'] <= 1:
                            all_agree = all_agree and True
                        else:
                            all_agree = False
                        '''
                        all_agree = False
                    elif votes[annotation][tag] == max_num_votes:
                        tie = True
                        tie_num_votes = max_num_votes
                        
                if tie and tie_num_votes == max_num_votes:
                    choosen_annotation = 'dontknow'
        
                if choosen_annotation == 'neutral':                    
                    classes.extend([0,0])
                elif choosen_annotation == 'dontknow':
                    if tie and tie_num_votes == max_num_votes:
                        classes.extend([-2, -2])
                    else:
                        classes.extend([-1,-1])
                    any_dontknow = True                
                else:
                    all_neutral = False
                    for tag in votes[annotation].keys():
                        if tag in ['neutral', 'dontknow']:
                            continue
                        if tag == choosen_annotation:
                            classes.append(1)
                        else:
                            classes.append(0)
                            
                confidences[annotation] = votes[annotation][choosen_annotation] / tweet['num_annot']
                    
            if any_dontknow:
                classes.append(-1)
            else:
                if all_neutral:
                    classes.append(1)
                else:
                    classes.append(0)

            
            annotated_tweets[tweet['tweet_id']] = {'text': tweet['text'], 'classes': classes, \
                                                   'confidences': confidences, 'num_annot': tweet['num_annot']}
            
            
            # Appending votes for calculating Fleiss' Kappa agreement later
            for k in votes.keys():
                if str(tweet['tweet_id'])+'l' in final_corpus_subsets[k]['tweet_id'].values:                
                    for k_ in votes[k].keys():
                        agreement_lists[k][k_].append(votes[k][k_])
                
        
        sys.stdout.write("Calculating Fleiss' Kappa agreement...\n")
        
        r_file = open('experiments/calculate_kappa.r.tmp', 'w')
        
        for k in agreement_lists.keys():
            dataframe = pd.DataFrame(agreement_lists[k])                        
            
            # Remove everything with less than 3 annotations
            dataframe['sum'] = dataframe.sum(axis=1)                        
            dataframe = dataframe.loc[dataframe['sum'] >= 3]
            
            # Remove 'dontknow' dimension
            dataframe = dataframe.drop('dontknow', axis=1)
            
            # Update the sum without 'dontknow' dimension
            dataframe = dataframe.drop('sum', axis=1)
            dataframe['sum'] = dataframe.sum(axis=1)            
                                    
            # We calculate the agreement for the subset of 4 annotators
            dataframe_4annots = pd.DataFrame(dataframe)            
            dataframe_4annots = dataframe_4annots.loc[dataframe_4annots['sum'] == 4]
            dataframe_4annots = dataframe_4annots.drop('sum', 1)
            
            # We calculate the agreement for the subset of 2 annotators
            dataframe_2annots = pd.DataFrame(dataframe)            
            dataframe_2annots = dataframe_2annots.loc[dataframe_2annots['sum'] == 2]
            dataframe_2annots = dataframe_2annots.drop('sum', 1)
            
            dataframe = dataframe.loc[dataframe['sum'] == 3]
            dataframe = dataframe.drop('sum', 1)
            
            r_file.write('# %s, 3 annotators\n' % k)
            r_file.write(list_r_format(dataframe, 3))
            
            kappa, k_max, k_min, k_nor = fleiss_kappa(dataframe, 3)
            
            sys.stdout.write("Kappa for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], kappa))
            sys.stdout.write("Kappa Max. for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], k_max))
            sys.stdout.write("Kappa Min. for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], k_min))
            sys.stdout.write("Kappa Normal for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], k_nor))
            
            
            
            prop_annot, k_annot = fleiss_kappa_ex(dataframe, 3)
            
            sys.stdout.write("Proportion per category for %s (3 annotators, %d samples):\n" % \
                             (k, dataframe.shape[0]))
            pprint.pprint(prop_annot)
            
            sys.stdout.write("kappa per category for %s (3 annotators, %d samples):\n" % \
                             (k, dataframe.shape[0]))
            pprint.pprint(k_annot)
            
            
            r_file.write('# %s, 4 annotators\n' % k)
            r_file.write(list_r_format(dataframe_4annots, 4))
            kappa, k_max, k_min, k_nor = fleiss_kappa(dataframe_4annots, 4)
            
            sys.stdout.write("Kappa for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], kappa))
            sys.stdout.write("Kappa Max. for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], k_max))
            sys.stdout.write("Kappa Min. for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], k_min))
            sys.stdout.write("Kappa Normal for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], k_nor))
            
            prop_annot, k_annot = fleiss_kappa_ex(dataframe_4annots, 4)

            sys.stdout.write("Proportion per category for %s (4 annotators, %d samples):\n" % \
                             (k, dataframe_4annots.shape[0]))
            pprint.pprint(prop_annot)
            
            sys.stdout.write("Kappa per category for %s (4 annotators, %d samples):\n" % \
                             (k, dataframe_4annots.shape[0]))
            pprint.pprint(k_annot)

            r_file.write('# %s, 2 annotators\n' % k)
            r_file.write(list_r_format(dataframe_2annots, 2))
        

            kappa, k_max, k_min, k_nor = fleiss_kappa(dataframe_2annots, 2)

            sys.stdout.write("Kappa for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], kappa))
            sys.stdout.write("Kappa Max. for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], k_max))
            sys.stdout.write("Kappa Min. for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], k_min))
            sys.stdout.write("Kappa Normal for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], k_nor))
            
            prop_annot, k_annot = fleiss_kappa_ex(dataframe_2annots, 2)

            sys.stdout.write("Proportion per category for %s (2 annotators, %d samples):\n" % \
                             (k, dataframe_2annots.shape[0]))
            pprint.pprint(prop_annot)
            
            sys.stdout.write("Kappa per category for %s (2 annotators, %d samples):\n" % \
                             (k, dataframe.shape[0]))
            pprint.pprint(k_annot)

        r_file.close()
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:        
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'main.tweets_crowd_csv_to_dataseries_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())