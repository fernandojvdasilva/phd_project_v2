#!/usr/local/bin/python2.7
# encoding: utf-8
'''
tools.tweets_extract_features -- extract features from tweets for machine learning experiments

tools.tweets_extract_features reads tweets and converts into a .csv with their features.


@author:     Fernando J. V. da Silva

@copyright:  2015 Fernando J. V. da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
@deffield    updated: Updated
'''

import sys
import os
import string

import pandas as pd

sys.path.append(os.path.join(os.getcwd(), '../'))

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from tweets.tweet_corpus import TweetCorpus
from tweets_annotator_manual.tweet_annotation_manual import TweetAnnotationManual
from tweets_annotator_manual.tweets_annotator_config import AnnotatorConfig
from tweets.tweets_filter_repeated import TweetsFilterRepeated

__all__ = []
__version__ = 0.1
__date__ = '2015-10-15'
__updated__ = '2015-10-15'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

header = ''

def get_class_headers(votes):
    global header

    if header != '':
        return header

    for annotation in votes.keys():
        for tag in votes[annotation].keys():
            if tag == 'neutral':
                continue
            header += tag + ';'

    return header

HASHTAG_STR = u"#"


JOY_HASHTAGS = [HASHTAG_STR + u"feliz", # JOY (Alegria) \
					 HASHTAG_STR + u"contente", \
					 HASHTAG_STR + u"alegre", \
					HASHTAG_STR + u"alegria", \
					HASHTAG_STR + u"felicidade",\
                     #HASHTAG_STR + u"encantado",\ # "encanto" and "encantado" usually refer to places, not emotions.
                     #HASHTAG_STR + u"encanto",\
                     #HASHTAG_STR + u"prazeroso",\ # "prazeroso" didn't find reasonable matches
                     #HASHTAG_STR + u"delicia",\ # "delicia" and "delicioso" brings too many pornographic tweets
                     #HASHTAG_STR + u"delicioso"
                ]

SAD_HASHTAGS = [	 HASHTAG_STR + u"triste", # SADNESS (Tristeza) \
					 HASHTAG_STR + u"chateado", \
                     HASHTAG_STR + u"chateada", \
					 HASHTAG_STR + u"magoado", \
                     HASHTAG_STR + u"magoada", \
					 HASHTAG_STR + u"desanimo", \
                     HASHTAG_STR + u"desanimado",\
                     HASHTAG_STR + u"desanimada",\
                     HASHTAG_STR + u"melancolico",\
                     HASHTAG_STR + u"melancólico",\
                     HASHTAG_STR + u"melancolia",\
                     HASHTAG_STR + u"deprimido",\
                     HASHTAG_STR + u"deprimida",\
                     #HASHTAG_STR + u"depressao",\ # "Depressão" is often meant as something related to comedy due to
                     #HASHTAG_STR + u"depressão",\ # some internet "memes" and not always as emotional tweets
                     #HASHTAG_STR + u"deprimente",\
                     #HASHTAG_STR + u"dor",\ # "Dor" (pain) doesn't seem to mean sadness, as there is a lot of people posting on pain for tatoos.
                     #HASHTAG_STR + u"dolorido",\
                     #HASHTAG_STR + u"dolorida",\
                     #HASHTAG_STR + u"doendo",\
                     #HASHTAG_STR + u"sofrimento",\ # the same for "sofrimento"
                     #HASHTAG_STR + u"sofrendo",\
                     #HASHTAG_STR + u"aflito",\ # "Aflito" is more related to expectation and sometimes fear, but doesn't show sadness.
                     #HASHTAG_STR + u"aflita",\
                     #HASHTAG_STR + u"aflicao",\
                     #HASHTAG_STR + u"aflição",\
                     HASHTAG_STR + u"pesar",\
                     HASHTAG_STR + u"pesames",\
                     HASHTAG_STR + u"pêsames", \
                     HASHTAG_STR + u"luto"
                     ]

FEAR_HASHTAGS = [HASHTAG_STR + u"medo", # Fear (Medo) \
                     HASHTAG_STR + u"commedo", \
                     HASHTAG_STR + u"empanico",\
                     HASHTAG_STR + u"aterrorizado",\
                     HASHTAG_STR + u"aterrorizada",\
                     #HASHTAG_STR + u"terrível",\ # "Terrível" sometimes refer to something bad or with bad behaviour, but no usually to fear
                     #HASHTAG_STR + u"terrivel",\
                     HASHTAG_STR + u"apavorado",\
                     HASHTAG_STR + u"apavorada",\
                     #HASHTAG_STR + u"espanto",\ # "Espanto" most of times means surprise but has some occurrencies with disgust
                     HASHTAG_STR + u"susto",\
                     HASHTAG_STR + u"assustado",\
                     HASHTAG_STR + u"assustada",\
                     HASHTAG_STR + u"receio",\
                     HASHTAG_STR + u"receoso",\
                     HASHTAG_STR + u"receosa"
                     ]

ANGER_HASHTAGS = [HASHTAG_STR + u"raiva", # Anger (Raiva) \
                     HASHTAG_STR + u"comraiva", \
                     #HASHTAG_STR + u"furia", \  # "Furia" doesn't show any angry tweets
                     #HASHTAG_STR + u"fúria", \
                     #HASHTAG_STR + u"furioso",\ # There are many tweets in which "furioso" is used as a slang for something "awesome"
                     #HASHTAG_STR + u"furiosa",\
                     HASHTAG_STR + u"irritado",\
                     HASHTAG_STR + u"irritada",\
                     HASHTAG_STR + u"irritação",\
                     HASHTAG_STR + u"aborrecido",\
                     HASHTAG_STR + u"aborrecida",\
                     #HASHTAG_STR + u"incomodo",\ # "Incômodo" doesn't seem to be related to anger.
                     #HASHTAG_STR + u"incômodo",\
                     #HASHTAG_STR + u"incomodado",\
                     #HASHTAG_STR + u"incomodada"
                     ]

TRUST_HASHTAGS = [   # Trust (Confiança) \
                     # HASHTAG_STR + u"confianca", # Removing #confiança (trust) because there is more non-emotional
                                                   # tweets which refer to the Brazilian soccer team Confiança and
                                                   # motivation tweets than real emotional tweets with this hashtag.
                     # HASHTAG_STR + u"confiança", \
                     HASHTAG_STR + u"confio", \
                     HASHTAG_STR + u"confiavel", \
                     HASHTAG_STR + u"confiável", \
                     HASHTAG_STR + u"admiro", \
                     HASHTAG_STR + u"admirável", \
                     HASHTAG_STR + u"admiravel", \
                     HASHTAG_STR + u"admiração", \
                     HASHTAG_STR + u"admiracao", \
                     HASHTAG_STR + u"idolo", \
                     HASHTAG_STR + u"meuidolo"
                  ]

DISG_HASHTAGS = [HASHTAG_STR + u"desgosto", # Disgust (Desgosto) \
                     HASHTAG_STR + u"aversao", \
                     HASHTAG_STR + u"aversão", \
                     HASHTAG_STR + u"antipatia", \
                     HASHTAG_STR + u"antipatico", \
                     HASHTAG_STR + u"antipático", \
                     HASHTAG_STR + u"antipatica", \
                     HASHTAG_STR + u"antipática", \
                     HASHTAG_STR + u"antipatico", \
                     HASHTAG_STR + u"repugnante", \
                     HASHTAG_STR + u"repugnancia", \
                     HASHTAG_STR + u"repugnância", \
                     HASHTAG_STR + u"nojento", \
                     HASHTAG_STR + u"nojenta", \
                     HASHTAG_STR + u"nojo", \
                     #HASHTAG_STR + u"despresivel", \ # typo here: the correct is desprezível
                     #HASHTAG_STR + u"despresível", \
                      ]

SUR_HASHTAGS = [HASHTAG_STR + u"surpreso", # Surprise (Surpreso) \
                     HASHTAG_STR + u"surpresa", \
                     HASHTAG_STR + u"surpreendido", \
                     HASHTAG_STR + u"surpreendida", \
                     HASHTAG_STR + u"espantado",\
                     HASHTAG_STR + u"espantada",\
                     HASHTAG_STR + u"perplexo",\
                     HASHTAG_STR + u"perplexa",\
                     HASHTAG_STR + u"perplexidade",\
                     HASHTAG_STR + u"quemdiria",\
                     HASHTAG_STR + u"susto", \
                     HASHTAG_STR + u"assustado",\
                     HASHTAG_STR + u"assustada"
                ]

# Only "previsão" (prevision) really appear to bring previsions

ANTI_HASHTAGS = [    #HASHTAG_STR + u"expectativa", # Anticipation (Antecipação) \
                     #HASHTAG_STR + u"naexpectativa",\
                     #HASHTAG_STR + u"curioso",\
                     #HASHTAG_STR + u"curiosa",\
                     #HASHTAG_STR + u"atento",\
                     #HASHTAG_STR + u"atenta",\
                     #HASHTAG_STR + u"interessado",\
                     #HASHTAG_STR + u"interessada",\
                     #HASHTAG_STR + u"interessante",\
                     #HASHTAG_STR + u"deolho",\
                     #HASHTAG_STR + u"monitorando",\
                     #HASHTAG_STR + u"observando",\
                     #HASHTAG_STR + u"analisando",\
                     HASHTAG_STR + u"previsao",\
                     HASHTAG_STR + u"previsão"
                 ]

def extract_classes_auto(tweet_text):
    annotation_hashtags = [TRUST_HASHTAGS, DISG_HASHTAGS, JOY_HASHTAGS, SAD_HASHTAGS, ANTI_HASHTAGS,
                           SUR_HASHTAGS, ANGER_HASHTAGS, FEAR_HASHTAGS]

    classes = []
    for emotion_annotation_hashtags in annotation_hashtags:
        annotate = False
        for hashtag in emotion_annotation_hashtags:
            if hashtag.encode('utf-8') in tweet_text.encode('utf-8'):
                annotate = True
                break
        if annotate:
            classes.append(1)
        else:
            classes.append(0)

    classes.append(0) # neutral class

    return classes



def extract_classes(manual_annotations, config):
    # We consider the most annotated tags only
    votes = {}
    for annotation_config in config['annotations']:
        votes[annotation_config['name']] = {}
        for options in annotation_config['options']:
            votes[annotation_config['name']][options['tag']] = 0
        votes[annotation_config['name']]['neutral'] = 0

    for annotation in manual_annotations:
        for tag in annotation.tags.keys():
            if tag == 'sarcasm': # We aren't using sarcasm here...
                continue

            if annotation.tags[tag] == 'remove': # This tweet must be discarded!! =========
                return None

            if annotation.tags[tag] != 'dontknow':
                votes[tag][annotation.tags[tag]] += 1

    header = get_class_headers(votes)

    all_neutral = True
    classes = [] # classes in binary features. Each feature is an emotion. The last one is for neutral
    for annotation in votes.keys():
        choosen_annotation = ''
        max_num_votes = 0
        for tag in votes[annotation].keys():
            if votes[annotation][tag] > max_num_votes:
                max_num_votes = votes[annotation][tag]
                choosen_annotation = tag

        if choosen_annotation == 'neutral':
            classes.extend([0,0])
        else:
            all_neutral = False
            for tag in votes[annotation].keys():
                if tag == 'neutral':
                    continue
                if tag == choosen_annotation:
                    classes.append(1)
                else:
                    classes.append(0)

    if all_neutral:
        classes.append(1)
    else:
        classes.append(0)

    return classes

def save_to_csv(filename, out_dir, tagged_tweets):
    csv_file = open("%s/%s" % (out_dir, filename), 'w')
    header = 'tweet_id;text;TRU;DIS;JOY;SAD;ANT;SUR;ANG;FEA;NEUTRAL;\n'
    csv_file.write(header)

    for k in tagged_tweets.keys():
        tweet_text = tagged_tweets[k]['text'].replace('\n\r', '')
        tweet_text = tweet_text.replace(';', ',')
        tweet_text = tweet_text.replace('"', '\'')

        line = '%ul;"%s";' % (k, tweet_text)
        for e in tagged_tweets[k]['classes']:
            line += '%d;' % e
        line += '\n'
        csv_file.write(line.encode('utf-8'))

    csv_file.close()


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  Copyright 2015 Fernando J. V. da Silva. All rights reserved.

  Licensed under GPL

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

#    try:
    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-p", "--path", dest="path", help="Stock Corpus path" )
    parser.add_argument("-t", "--tagged", dest="tagged", help="Automatic tagged Corpus path" )
    parser.add_argument("-o", "--out", dest="out", help="Output directory" )
    parser.add_argument("-c", "--config", dest="config", help="Config file path" )
    parser.add_argument('-V', '--version', action='version', version=program_version_message)


    # Process arguments
    args = parser.parse_args()

    out_dir = args.out

    path = args.path
    config_path = args.config
    tagged_path = args.tagged


    config = AnnotatorConfig()
    config.load(config_path)


    tweet_corpus = TweetCorpus()
    tweet_corpus.load_corpus(path)

    tagged_tweets = {}

    # Saving CSV from the stock corpus
    while tweet_corpus.next_tweet() != False:
        manual_annotations = TweetAnnotationManual.get_annotations_by_tweet(path, tweet_corpus.curr_tweet)

        if len(manual_annotations) == 0:
            continue

        classes = extract_classes(manual_annotations, config)

        if classes is None:
            continue

        lowers = tweet_corpus.curr_tweet['text'].lower()

        tagged_tweets[tweet_corpus.curr_tweet['id']] = {'text': lowers, 'classes': classes}

    save_to_csv('tweets_stocks.csv', out_dir, tagged_tweets)


    tweet_corpus = TweetCorpus()
    tweet_corpus.load_corpus(tagged_path)

    tweets_filter = TweetsFilterRepeated(tweet_corpus)
    tweets_filter.find_repeated()
    tweets_filter.delete_repeated()

    tagged_tweets = {}

    tweet_corpus.load_corpus(tagged_path)
    tweet_corpus.first_tweet()

    # Saving CSV from the automatic tagged emotion corpus
    while tweet_corpus.next_tweet() != False:

        classes = extract_classes_auto(tweet_corpus.curr_tweet['text'].lower())

        lowers = tweet_corpus.curr_tweet['text'].lower()

        tagged_tweets[tweet_corpus.curr_tweet['id']] = {'text': lowers, 'classes': classes}

    save_to_csv('tweets_auto-tagged.csv', out_dir, tagged_tweets)



    print "Done!"

    return 0
#     except KeyboardInterrupt:
#         ### handle keyboard interrupt ###
#         return 0
#     except Exception, e:
#         if DEBUG or TESTRUN:
#             raise(e)
#         indent = len(program_name) * " "
#         sys.stderr.write(program_name + ": " + repr(e) + "\n")
#         sys.stderr.write(indent + "  for help use --help")
#         return 2

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.tweets_annotation_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
