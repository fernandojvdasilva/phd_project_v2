
add_emolex_to_dataseries <- function(df)
{
  library("syuzhet")
  
  df_emolex <- get_nrc_sentiment(as.character(df$text))  
  
  df$emolex_joy <- df_emolex$joy
  df$emolex_sad <- df_emolex$sad
  df$emolex_trust <- df_emolex$trust
  df$emolex_disgust <- df_emolex$disgust
  df$emolex_anger <- df_emolex$anger
  df$emolex_fear <- df_emolex$fear
  df$emolex_anticipation <- df_emolex$anticipation
  df$emolex_surprise <- df_emolex$surprise
  df$emolex_positive <- df_emolex$positive
  df$emolex_negative <- df_emolex$negative
  
  return(df)
}

#csv_dir <- '/media/fernando/DADOS/UNICAMP/PHD/corpus_dataseries'
csv_dir <- '/media/fernando/DADOS/UNICAMP/PHD/src/phd_project/experiments/full_agreement'

#df_ctx_free <- read.csv(file.path(csv_dir, "tweets_auto-tagged.csv"), sep=";")
#df_ctx_free_full = add_emolex_to_dataseries(df_ctx_free)
#write.csv(df_ctx_free_full, file= file.path(csv_dir,"tweets_auto-tagged-emolex.csv" ))


df_ctx_specific <- read.csv(file.path(csv_dir, "tweets_stocks.csv"), sep=";")
df_ctx_specific = add_emolex_to_dataseries(df_ctx_specific)
write.csv(df_ctx_specific, file=file.path(csv_dir, "tweets_stocks_emolex.csv"))
