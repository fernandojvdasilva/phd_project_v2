#!/usr/local/bin/python2.7
# encoding: utf-8
'''
    Copyright 2014 Fernando J. V. da Silva
    
    This file is part of ibov_tweet_predict.

    ibov_tweet_predict is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ibov_tweet_predict is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ibov_tweet_predict.  If not, see <http://www.gnu.org/licenses/>
'''    

'''
main.tweets_csv_to_json -- converts csv file into tweet corpus

main.tweets_csv_to_json converts csv file in the format extracted from the
google engine database into locally json files representing tweets.

@author:     Fernando J. V. da Silva

@copyright:  2014 Fernando J. V. da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
@deffield    updated: Updated
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

sys.path.append(os.path.join(os.getcwd(), '../'))



from tweets.tweet_corpus_factory import TweetCorpusFactory


__all__ = []
__version__ = 0.1
__date__ = '2014-08-30'
__updated__ = '2014-08-30'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  Copyright 2014 Fernando J. V. da Silva. All rights reserved.

    ibov_tweet_predict is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ibov_tweet_predict is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ibov_tweet_predict.  If not, see <http://www.gnu.org/licenses/>
    
USAGE
''' % (program_shortdesc, str(__date__))

#    try:
    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('-V', '--version', action='version', version=program_version_message)
    parser.add_argument(dest="csv_path",
                        help="CSV file path", metavar="csv_path")
    parser.add_argument(dest="json_dir",
                        help="JSON output directory", metavar="json_dir")

    # Process arguments
    args = parser.parse_args()

    csv_path = args.csv_path
    json_dir = args.json_dir

    if not os.path.exists(csv_path):
        sys.stderr.write("The CSV path %s does not exist\n" % csv_path)
        return -1
    else:
        tweet_corpus = TweetCorpusFactory.create_tweet_corpus(csv_path)

        if tweet_corpus == None:
            sys.stderr.write("The CSV file has an invalid format!\n")
            return -1

        tweet_corpus.save(json_dir)

        sys.stdout.write("The corpus was successfully loaded!\n")
        return 0
'''
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2
'''

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'main.tweets_csv_to_json_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
