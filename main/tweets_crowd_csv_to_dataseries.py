#!/usr/local/bin/python2.7
# encoding: utf-8
'''
main.tweets_crowd_csv_to_dataseries -- Convert CSV downloaded from tweets annotated by crowd-sourcing experiment into dataseries

main.tweets_crowd_csv_to_dataseries is a tool that convert CSV files downloaded from Google App Engine
and annotated by the crowd sourcing annotation experiment into a dataseries used by machine learning algorithms

@author:     fernando

@copyright:  2017 Fernando Vieira da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com

'''

import sys
import os

import pandas as pd
import pprint

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

from nlp_utils.corpus_analysis import *

__all__ = []
__version__ = 0.1
__date__ = '2017-04-11'
__updated__ = '2017-04-11'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg
    
    
def save_to_csv(filename, out_dir, tagged_tweets):
    csv_file = open("%s/%s" % (out_dir, filename), 'w')
    header = 'tweet_id;text;TRU;DIS;JOY;SAD;ANT;SUR;ANG;FEA;NEUTRAL;conf_tru_dis;conf_joy_sad;conf_ant_sur;conf_ang_fea;num_annot\n'
    csv_file.write(header)

    for k in tagged_tweets.keys():
        tweet_text = tagged_tweets[k]['text'].replace('\n\r', '')
        tweet_text = tweet_text.replace(';', ',')
        tweet_text = tweet_text.replace('"', '\'')

        line = '%ul;"%s";' % (k, tweet_text)
        for e in tagged_tweets[k]['classes']:
            line += '%d;' % e
        
        # Saving confidences
        line += '%.3f;%.3f;%.3f;%.3f;' % (tagged_tweets[k]['confidences']['trust_vs_disgust'], \
                                         tagged_tweets[k]['confidences']['joy_vs_sadness'], \
                                         tagged_tweets[k]['confidences']['surprise_vs_antecip'], \
                                         tagged_tweets[k]['confidences']['anger_vs_fear'])

        # Saving number of annotators
        line += '%d' % tagged_tweets[k]['num_annot']
            
        line += '\n'
        #csv_file.write(str(line.encode('utf-8')))
        csv_file.write(line)

    csv_file.close()
    
    

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by fernando on %s.
  Copyright 2017 Fernando Vieira da Silva. All rights reserved.
  

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        
        parser.add_argument("-a", "--annots", dest="annots", help="Full path to the CSV file containing annotations (TweetAnnotationCrowd)" )
        parser.add_argument("-t", "--tweets", dest="tweets", help="Full path to the CSV file containing tweets stored in Google App Engine (TweetCrowd)" )                
        #parser.add_argument("-u", "--users", dest="users", help="Full path to the CSV file containing user profiles stored in Google App Engine (UserAnnotator)" )
        parser.add_argument('-f', '--fullagreement', dest="full_agreement", help='Only considers full aggrement.', action='store_true')        
        parser.add_argument("-o", "--out", dest="out", help="Output directory" )

        # Process arguments
        args = parser.parse_args()

        annots_path = args.annots
        tweets_path = args.tweets
        #users_path = args.users
        out_dir = args.out
        full_agreement = args.full_agreement
        
        
        annots = pd.read_csv(annots_path)
        tweets = pd.read_csv(tweets_path)
        #users = pd.read_csv(users_path)
        
        # Select tweets with 3 or more annotations only
        tweets = tweets.loc[tweets['num_annot'] >= 3]
        
        # Select these tweets' annotations
        annots = annots.loc[annots['tweet_id'].isin(tweets['tweet_id'])]
        
        annotated_tweets = {}
        
        agreement_lists = {'trust_vs_disgust': {'TRU':[], 'DIS': [], 'neutral': [], 'dontknow': []},
                     'joy_vs_sadness': {'JOY':[], 'SAD': [], 'neutral': [], 'dontknow':[]},
                     'surprise_vs_antecip': {'ANT':[], 'SUR': [], 'neutral': [], 'dontknow': []},
                     'anger_vs_fear': {'ANG':[], 'FEA': [], 'neutral': [], 'dontknow': []}}
        
        
        
        sys.stdout.write("Reading annotations...\n")
        for index, tweet in tweets.iterrows():
            tweet_annots = annots.loc[annots['tweet_id'] == tweet['tweet_id']]            
            
            votes = {'trust_vs_disgust': {'TRU':0, 'DIS': 0, 'neutral': 0, 'dontknow': 0},
                     'joy_vs_sadness': {'JOY':0, 'SAD': 0, 'neutral': 0, 'dontknow': 0},
                     'surprise_vs_antecip': {'ANT':0, 'SUR': 0, 'neutral': 0, 'dontknow': 0},
                     'anger_vs_fear': {'ANG':0, 'FEA': 0, 'neutral': 0, 'dontknow': 0}}
            
            classes = []
            confidences = {}
            for index, tweet_annot in tweet_annots.iterrows():
                for key in votes.keys():
                    votes[key][tweet_annot[key]] += 1
                    
            all_neutral = True
            any_dontknow = False
            all_agree = True
            for annotation in votes.keys():
                choosen_annotation = ''
                max_num_votes = 0
                tie = False
                tie_num_votes = 0
                for tag in votes[annotation].keys():
                    if votes[annotation][tag] > max_num_votes:                        
                        max_num_votes = votes[annotation][tag]
                        choosen_annotation = tag
                        if not full_agreement is None and full_agreement and \
                            (votes[annotation][tag] + votes[annotation]['dontknow']) == tweet['num_annot'] and \
                            votes[annotation]['dontknow'] <= 1:
                            all_agree = all_agree and True
                        else:
                            all_agree = False
                    elif votes[annotation][tag] == max_num_votes:
                        tie = True
                        tie_num_votes = max_num_votes
                        
                if tie and tie_num_votes == max_num_votes:
                    choosen_annotation = 'dontknow'
        
                if choosen_annotation == 'neutral':                    
                    classes.extend([0,0])
                elif choosen_annotation == 'dontknow':
                    if tie and tie_num_votes == max_num_votes:
                        classes.extend([-2, -2])
                    else:
                        classes.extend([-1,-1])
                    any_dontknow = True                
                else:
                    all_neutral = False
                    for tag in votes[annotation].keys():
                        if tag in ['neutral', 'dontknow']:
                            continue
                        if tag == choosen_annotation:
                            classes.append(1)
                        else:
                            classes.append(0)
                            
                confidences[annotation] = votes[annotation][choosen_annotation] / tweet['num_annot']
                    
            if any_dontknow:
                classes.append(-1)
            else:
                if all_neutral:
                    classes.append(1)
                else:
                    classes.append(0)

            if full_agreement is None or full_agreement == False or all_agree == True:
                annotated_tweets[tweet['tweet_id']] = {'text': tweet['text'], 'classes': classes, \
                                                       'confidences': confidences, 'num_annot': tweet['num_annot']}
                
                
                # Appending votes for calculating Fleiss' Kappa agreement later
                for k in votes.keys():
                    for k_ in votes[k].keys():
                        agreement_lists[k][k_].append(votes[k][k_])
        
        sys.stdout.write("Saving dataseries CSV...\n")
        save_to_csv('tweets_stocks.csv', out_dir, annotated_tweets)
        sys.stdout.write("Dataseries file saved!\n")
        
        sys.stdout.write("Calculating Fleiss' Kappa agreement...\n")
        
        for k in agreement_lists.keys():
            dataframe = pd.DataFrame(agreement_lists[k])
                        
            
            # Remove everything with less than 3 annotations
            dataframe['sum'] = dataframe.sum(axis=1)                        
            dataframe = dataframe.loc[dataframe['sum'] >= 3]
            
            # Remove 'dontknow' dimension
            dataframe = dataframe.drop('dontknow', axis=1)
            
            # Update the sum without 'dontknow' dimension
            dataframe = dataframe.drop('sum', axis=1)
            dataframe['sum'] = dataframe.sum(axis=1)            
                                    
            # We calculate the agreement for the subset of 4 annotators
            dataframe_4annots = pd.DataFrame(dataframe)            
            dataframe_4annots = dataframe_4annots.loc[dataframe_4annots['sum'] == 4]
            dataframe_4annots = dataframe_4annots.drop('sum', 1)
            
            # We calculate the agreement for the subset of 2 annotators
            dataframe_2annots = pd.DataFrame(dataframe)            
            dataframe_2annots = dataframe_2annots.loc[dataframe_2annots['sum'] == 2]
            dataframe_2annots = dataframe_2annots.drop('sum', 1)
            
            dataframe = dataframe.loc[dataframe['sum'] == 3]
            dataframe = dataframe.drop('sum', 1)
            
            kappa, k_max, k_min, k_nor = fleiss_kappa(dataframe, 3)
            
            sys.stdout.write("Kappa for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], kappa))
            sys.stdout.write("Kappa Max. for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], k_max))
            sys.stdout.write("Kappa Min. for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], k_min))
            sys.stdout.write("Kappa Normal for %s (3 annotators, %d samples): %f\n" % \
                             (k, dataframe.shape[0], k_nor))
            
            
            
            prop_annot, k_annot = fleiss_kappa_ex(dataframe, 3)
            
            sys.stdout.write("Proportion per category for %s (3 annotators, %d samples):\n" % \
                             (k, dataframe.shape[0]))
            pprint.pprint(prop_annot)
            
            sys.stdout.write("kappa per category for %s (3 annotators, %d samples):\n" % \
                             (k, dataframe.shape[0]))
            pprint.pprint(k_annot)
            
            kappa, k_max, k_min, k_nor = fleiss_kappa(dataframe_4annots, 4)
            
            sys.stdout.write("Kappa for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], kappa))
            sys.stdout.write("Kappa Max. for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], k_max))
            sys.stdout.write("Kappa Min. for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], k_min))
            sys.stdout.write("Kappa Normal for %s (4 annotators, %d samples): %f\n" % \
                             (k, dataframe_4annots.shape[0], k_nor))
            
            prop_annot, k_annot = fleiss_kappa_ex(dataframe_4annots, 4)

            sys.stdout.write("Proportion per category for %s (4 annotators, %d samples):\n" % \
                             (k, dataframe_4annots.shape[0]))
            pprint.pprint(prop_annot)
            
            sys.stdout.write("Kappa per category for %s (4 annotators, %d samples):\n" % \
                             (k, dataframe_4annots.shape[0]))
            pprint.pprint(k_annot)


            kappa, k_max, k_min, k_nor = fleiss_kappa(dataframe_2annots, 2)

            sys.stdout.write("Kappa for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], kappa))
            sys.stdout.write("Kappa Max. for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], k_max))
            sys.stdout.write("Kappa Min. for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], k_min))
            sys.stdout.write("Kappa Normal for %s (2 annotators, %d samples): %f\n" % \
                             (k, dataframe_2annots.shape[0], k_nor))
            
            prop_annot, k_annot = fleiss_kappa_ex(dataframe_2annots, 2)

            sys.stdout.write("Proportion per category for %s (2 annotators, %d samples):\n" % \
                             (k, dataframe_2annots.shape[0]))
            pprint.pprint(prop_annot)
            
            sys.stdout.write("Kappa per category for %s (2 annotators, %d samples):\n" % \
                             (k, dataframe.shape[0]))
            pprint.pprint(k_annot)

        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:        
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'main.tweets_crowd_csv_to_dataseries_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())