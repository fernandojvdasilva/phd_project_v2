'''
Created on 2 de jun de 2017

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode, TweetTree
import nltk

from nlp_utils.nlp_semantic import sem_is_stock_code



class ParseLayerHashtag(ParseLayer):
    
    def parse(self, nodes):
        res_nodes = []
        i = 0
        while i < len(nodes):
            node = nodes[i]
            node_content = node.content
            if node_content in TweetTreeNode.TAGS.keys():
                res_nodes.append(node)
            elif node_content in ['#', '#'] and i < len(nodes)-1:
                hash_node = TweetTreeNode('HASH')
                hash_node.addChild(TweetTreeNode('.'))                                    
                res_nodes.append(hash_node)
                if nodes[i+1].content != 'WE':
                    i += 1
            else:
                res_nodes.append(node)
                
            i += 1
                
        return self.nextLayer.parse(res_nodes) 
                
