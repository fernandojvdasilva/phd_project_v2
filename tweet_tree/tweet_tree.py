'''
Created on 2 de jun de 2017

@author: fernando
'''

from nlp_utils.nlp_lexical import lex_tokenize
import preprocessor as tp

class ParseLayer:
    
    def __init__(self, nextLayer):
        self.nextLayer = nextLayer        
    
    def parse(self, nodes):
        pass


class TweetTreeNode:
    
    TAGS = {'WE': 'Emotional word', \
             'EM': 'Emotion tags', \
             'W': 'Single word', \
             'STOP': 'Stop word', \
             'NOT': 'Negation word', \
             'T': 'Target word (starting with @ or $ or any stock code)', \
             'EXC': 'Exclamation', \
             'INT': 'Interrogation', \
             'ET':  'Emoticon', \
             'POS': 'Part of Speech', \
             'RT': 'Retweet tag', \
             'POL': 'Politician name', \
             'CUR': 'Currency value', \
             'PER': 'Percentage value', \
             'NUM': 'Numeric value', \
             'U': 'URL', \
             'HASH': 'Hashtag'
             }
        
    
    def __init__(self, content):
        self.children = []
        self.content = content
        self.parent = None
        
        
    def addChild(self, child):
        child.parent = self
        self.children.append(child)
        
    @property
    def content(self):
        return self.__content
    
    @content.setter
    def content(self, content):
        self.__content = content
        
    def getParseTree(self):
        if len(self.children) == 0:
            res = self.content
        else:
            children_str = ''            
                        
            for c in self.children:
                if len(c.children) > 0:
                    children_str += '(%s)' % c.getParseTree()
                else:
                    children_str += '%s ' % c.getParseTree()
            
            if children_str[-1] == ' ':
                children_str = children_str[:-1]
                               
            res = "%s %s" % (self.content, children_str)
            
            if self.parent is None:
                res = "(%s)" % res
        
        return res
        
class TweetPOSTreeNode(TweetTreeNode):
        
    def getParseTree(self):            
        if len(self.children) == 0:
            res = self.__content
        else:
            children_str = ''
            
            for c in self.children:
                if len(c.children) > 0:
                    children_str += '(%s)' % c.getParseTree()
                else:
                    children_str += '%s ' % c.getParseTree()
                    
            if children_str[-1] == ' ':
                children_str = children_str[:-1]
                
            res = "%s %s" % (self.__content, children_str)
        
        return res
        
    @property
    def content(self):
        if len(self.children) > 0:
            return self.children[0].content
        else:
            return None

    @content.setter
    def content(self, content):
        self.__content = content
        
class TweetTree:

    def __init__(self, first_parse_layer):
        self.first_parse_layer = first_parse_layer

    
    def preprocess(self, text):
        tp.set_options(tp.OPT.URL, tp.OPT.MENTION, tp.OPT.NUMBER, tp.OPT.RESERVED)
        text = tp.tokenize(text)
        text = text.replace('(', '[')
        text = text.replace(')', ']')
        text = text.replace('_', ' ')
        word_list = lex_tokenize(text)
        
        return word_list
    
    def parse_text(self, text):
        word_list = self.preprocess(text)
        return self.parse(word_list)
    
    def parse(self, word_list):
        self.nodes = self.first_parse_layer.parse(word_list)
        return self.nodes
        
    def toString(self):
        pass