'''
Created on 2 de jun de 2017

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode, TweetTree
import nltk

from nlp_utils.nlp_semantic import sem_is_stock_code

TARGET_TAGS = ['@', '$']

class ParseLayerTarget(ParseLayer):
    
    def parse(self, nodes):
        res_nodes = []
        for node in nodes:
            node_content = node.content
            if node_content in TweetTreeNode.TAGS.keys():
                res_nodes.append(node)                
            elif node_content[0] in TARGET_TAGS or sem_is_stock_code(node_content):
                stopword_node = TweetTreeNode('T')                
                stopword_node.addChild(node)
                res_nodes.append(stopword_node)
            else:
                res_nodes.append(node)
                
        return self.nextLayer.parse(res_nodes) 
                
