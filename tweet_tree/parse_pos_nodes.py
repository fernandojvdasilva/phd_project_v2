'''
Created on 16 de Ago de 2018

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode, TweetTree, TweetPOSTreeNode 
import nltk

from nlp_utils.nlp_syntactic import syn_pos_tag

class ParsePOS(ParseLayer):
    
    def parse(self, nodes):
        
        tags = syn_pos_tag(nodes, 'pt')
             
        res_nodes = []
        
        for tags_ in tags:
            for t in tags_:
                pos_node = TweetPOSTreeNode(t[1])                
                content_node = TweetTreeNode(t[0])                
                pos_node.addChild(content_node)
                
                res_nodes.append(pos_node)
                
        return self.nextLayer.parse(res_nodes) 
                
