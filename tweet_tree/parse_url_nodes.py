'''
Created on 2 de jun de 2017

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode, TweetTree
import nltk

from nlp_utils.nlp_semantic import sem_is_stock_code



class ParseLayerUrl(ParseLayer):
    
    def parse(self, nodes):
        res_nodes = []
        i = 0
        while i < len(nodes):
            node = nodes[i]
            node_content = node.content
            
            if i < len(nodes)-2:
                node1 = nodes[i+1]            
                node1_content = node1.content
            
                node2 = nodes[i+2]            
                node2_content = node2.content
            
            if node_content in TweetTreeNode.TAGS.keys():
                res_nodes.append(node)
                i += 1                
            elif node_content == '$' and i < len(nodes)-2 and \
            node1_content == 'URL' and node2_content == '$':
                url_node = TweetTreeNode('U')
                url_node.addChild(TweetTreeNode('.'))
                res_nodes.append(url_node)
                i += 3
            else:
                res_nodes.append(node)
                i += 1
                
        return self.nextLayer.parse(res_nodes) 
                
