'''
Created on 31 de mai de 2018

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode, TweetTree


class ParseLayerStringNodes(ParseLayer):
    
    def parse(self, nodes):
        res_nodes = []
        for node in nodes:
            if type(node) is str:
                res_nodes.append(TweetTreeNode(node))
            else:
                res_nodes.append(node)
                            
                
        return self.nextLayer.parse(res_nodes) 
                
