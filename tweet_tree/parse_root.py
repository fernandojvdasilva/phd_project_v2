'''
Created on 2 de jun de 2017

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode

class ParseLayerRoot(ParseLayer):
    
    def parse(self, nodes):
        root_node = TweetTreeNode('ROOT')
        for node in nodes:                
            root_node.addChild(node)
        
        return [root_node]

    
        