'''
Created on 2 de jun de 2017

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode, TweetTree
import nltk

NEGATION_WORDS = ['não', 'ñ', 'nada', 'nunca', 'ninguém', 'ninguem', 'nenhum', 'nenhuma', 'nao', 'nem', 'jamais']

class ParseLayerNegation(ParseLayer):
    
    def parse(self, nodes):
        res_nodes = []
        for node in nodes:
            node_content = node.content
            if node_content in TweetTreeNode.TAGS.keys():
                res_nodes.append(node) 
            elif node_content in NEGATION_WORDS:
                stopword_node = TweetTreeNode('NOT')                
                stopword_node.addChild(node)
                res_nodes.append(stopword_node)
            else:
                res_nodes.append(node)
                
        return self.nextLayer.parse(res_nodes) 
                
