
Unrecognized option -M!


Tree Kernels in SVM-light V5.01-TK-1.2 : SVM Learning module 15.11.06
by Alessandro Moschitti, moschitti@info.uniroma2.it
University of Rome "Tor Vergata"


Copyright: Thorsten Joachims, thorsten@ls8.cs.uni-dortmund.de

This software is available for non-commercial use only. It must not
be modified and distributed without prior permission of the author.
The author is not responsible for implications from the use of this
software.

   usage: svm_learn [options] example_file model_file

Arguments:
         example_file-> file with training data
         model_file  -> file to store learned decision rule in
General options:
         -?          -> this help
         -v [0..3]   -> verbosity level (default 1)
Learning options:
         -z {c,r,p}  -> select between classification (c), regression (r),
                        and preference ranking (p) (default classification)
         -c float    -> C: trade-off between training error
                        and margin (default [avg. x*x]^-1)
         -w [0..]    -> epsilon width of tube for regression
                        (default 0.1)
         -j float    -> Cost: cost-factor, by which training errors on
                        positive examples outweight errors on negative
                        examples (default 1) (see [4])
         -b [0,1]    -> use biased hyperplane (i.e. x*w+b>0) instead
                        of unbiased hyperplane (i.e. x*w>0) (default 1)
         -i [0,1]    -> remove inconsistent training examples
                        and retrain (default 0)
Performance estimation options:
         -x [0,1]    -> compute leave-one-out estimates (default 0)
                        (see [5])
         -o ]0..2]   -> value of rho for XiAlpha-estimator and for pruning
                        leave-one-out computation (default 1.0) (see [2])
         -k [0..100] -> search depth for extended XiAlpha-estimator 
                        (default 0)
Transduction options (see [3]):
         -p [0..1]   -> fraction of unlabeled examples to be classified
                        into the positive class (default is the ratio of
                        positive and negative examples in the training data)
Kernel options:
         -t int      -> type of kernel function:
                        0: linear (default)
                        1: polynomial (s a*b+c)^d
                        2: radial basis function exp(-gamma ||a-b||^2)
                        3: sigmoid tanh(s a*b + c)
                        4: user defined kernel from kernel.h
                        5: combination of forest and vector sets according to W, V, S, C options
                        11: re-ranking based on trees (each instance must have two trees),
                        12: re-ranking based on vectors (each instance must have two vectors)
                        13: re-ranking based on both tree and vectors (each instance must have
                            two trees and two vectors)  
         -W [S,A]    -> with an 'S', a tree kernel is applied to the sequence of trees of two input
                        forests and the results are summed;  
                     -> with an 'A', a tree kernel is applied to all tree pairs from the two forests
                        (default 'S')
         -V [S,A]    -> same as before but regarding sequences of vectors are used (default 'S' and
                        the type of vector-based kernel is specified by the option -S)
         -S [0,4]    -> kernel to be used with vectors (default polynomial of degree 3,
                        i.e. -S = 1 and -d = 3)
         -C [*,+,T,V]-> combination operator between forests and vectors (default 'T')
                     -> 'T' only the contribution from trees is used (specified by option -W)
                     -> 'V' only the contribution from vectors is used (specified by option -V)
                     -> '+' or '*' sum or multiplication of the contributions from vectors and 
                            trees (default T) 
         -D [0,1]    -> 0, SubTree kernel or 1, SubSet Tr