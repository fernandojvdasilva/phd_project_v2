#Best Parameters for JOY_vs_SAD: prec    97.316667
#rec     98.310000
#f1      97.810802
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#Best Parameters for TRU_vs_DIS: prec    84.540000
#rec     90.546667
#f1      87.436685
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#Best Parameters for ANG_vs_FEA: prec    96.936667
#rec     99.263333
#f1      98.086084
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#Best Parameters for ANT_vs_SUR: prec     96.306667
#rec     100.000000
#f1       98.118456
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#SVM_TREE_ALGS = { "ST-KERNEL": {"type": '0', "decay": "L"}, 
#                  "SST-KERNEL": {"type": '1', "decay": "L"},
#				   "SST-bow-KERNEL": {"type": '2', "decay": "L"},
#				  "PT-KERNEL": {"type": '3', "decay": "M"}}


# The MOS-KERNEL with 0.1 decay has shown the best results for all classifiers
BEST_KERNEL=-1
BEST_DECAY=0.1

# echo "ALL vs JOY"

# MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_JOY_BEST_PARS.txt"
# DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_JOY.txt"


# ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1

# echo "ALL vs SAD"

# MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_SAD_BEST_PARS.txt"
# DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt"


# ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1


# echo "ALL vs TRU"

# MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_TRU_BEST_PARS.txt"
# DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_TRU.txt"


# ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1


# echo "ALL vs DIS"

# MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_DIS_BEST_PARS.txt"
# DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_DIS.txt"


# ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1


# echo "ALL vs ANT"

# MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_ANT_BEST_PARS.txt"
# DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_ANT.txt"


# ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1

# echo "ALL vs SUR"


# MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_SUR_BEST_PARS.txt"
# DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SUR.txt"


# ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1


# echo "ALL vs ANG"

# MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_ANG_BEST_PARS.txt"
# DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_ANG.txt"


# ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1


echo "ALL vs FEA"

MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_FEA_BEST_PARS.txt"
DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_FEA.txt"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $BEST_KERNEL -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1