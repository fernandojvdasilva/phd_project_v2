#!/bin/bash
# Usage: svm_light_cross_validation full_data num_train_items num_test_items k_cycles results_file
# based in http://niketanblog.blogspot.com/2012/02/cross-validation-on-svm-light.html
DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_TRU_vs_DIS.txt


 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  
done < "$input"

echo "DONE!"


DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_JOY_vs_SAD.txt


 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  
done < "$input"

echo "DONE!"

DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ANG_vs_FEA.txt


 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  
done < "$input"

echo "DONE!"

DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ANT_vs_SUR.txt


 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  
done < "$input"

echo "DONE!"