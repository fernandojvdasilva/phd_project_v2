#/bin/sh
echo 'Training ANG vs FEA'
./train_tree_classifier_ANG_vs_FEA.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ANT vs SUR'
./train_tree_classifier_ANT_vs_SUR.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training TRU vs DIS'
./train_tree_classifier_TRU_vs_DIS.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training JOY vs SAD'
./train_tree_classifier_JOY_vs_SAD.sh > ../logs/train_tree_classifiers.log 2>&1

echo 'Training ALL vs JOY'
./train_tree_classifier_ALL_vs_JOY.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ALL vs SAD'
./train_tree_classifier_ALL_vs_SAD.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ALL vs TRU'
./train_tree_classifier_ALL_vs_TRU.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ALL vs DIS'
./train_tree_classifier_ALL_vs_DIS.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ALL vs ANG'
./train_tree_classifier_ALL_vs_ANG.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ALL vs FEA'
./train_tree_classifier_ALL_vs_FEA.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ALL vs ANT'
./train_tree_classifier_ALL_vs_ANT.sh > ../logs/train_tree_classifiers.log 2>&1
echo 'Training ALL vs SUR'
./train_tree_classifier_ALL_vs_SUR.sh > ../logs/train_tree_classifiers.log 2>&1
