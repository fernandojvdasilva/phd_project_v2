MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_JOY_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_JOY.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_JOY.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_JOY.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE

MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_SAD_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_SAD.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_SAD.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_SAD.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE


MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_TRU_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_TRU.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_TRU.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_TRU.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE

MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_DIS_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_DIS.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_DIS.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_DIS.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE

MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_ANG_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_ANG.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_ANG.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_ANG.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE


MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_FEA_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_FEA.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_FEA.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_FEA.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE

MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_ANT_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_ANT.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_ANT.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_ANT.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE

MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_SUR_BEST_PARS.txt"
TEST_FILE="../datasets/tweets_auto-tagged-emolex_test_svmlight_ALL_vs_SUR.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_ALL_vs_SUR.txt"
LOG_FILE="../logs/test-tree-kernel-model_ALL_vs_SUR.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE
