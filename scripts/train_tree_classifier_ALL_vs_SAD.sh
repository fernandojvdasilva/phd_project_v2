#!/bin/bash
# Usage: svm_light_cross_validation full_data num_train_items num_test_items k_cycles results_file
# based in http://niketanblog.blogspot.com/2012/02/cross-validation-on-svm-light.html
RESULT_FILE=../logs/results_train_tree_classifier_ALL_vs_SAD.log

echo "Running SVM-Light via cross validation on" ../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt "by using" $((`wc -l < ../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt`/3)) "training items and" $((`wc -l < ../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt`/3*2)) "test items (Total number of cross-validation cycles:" 3 > $RESULT_FILE

MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_SAD"
TEMP_FILE="svm-tree-kernel-tempFile_ALL_vs_SAD."$RANDOM".txt"
PRED_FILE="../logs/svm-tree-kernel-prediction_ALL_vs_SAD."$RANDOM".txt"
DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt
NUM_TRAIN=$((`wc -l < ../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt`/3))
NUM_TEST=$((`wc -l < ../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt`/3*2))
NUM_CYCLES=3

TEMP_DATA_FILE=$DATA_FILE"."$RANDOM".temp"
TRAIN_FILE=$TEMP_DATA_FILE".train"
TEST_FILE=$TEMP_DATA_FILE".test"

TEMP_RESULT=$RESULT_FILE".temp"
SVM_PATTERN='s/Accuracy on test set: \([0-9]*.[0-9]*\)% ([0-9]* correct, [0-9]* incorrect, [0-9]* total)\.*/'
for k in `seq 1 $NUM_CYCLES`
do
 sort -R $DATA_FILE > $TEMP_DATA_FILE
 head -n $NUM_TRAIN $TEMP_DATA_FILE > $TRAIN_FILE
 tail -n $NUM_TEST $TEMP_DATA_FILE > $TEST_FILE

 echo "------------------------------------------"  >> $RESULT_FILE
 echo "Cross-validation cycle:" $k >> $RESULT_FILE

 echo ""  >> $RESULT_FILE

 
  echo "MOS-KERNEL, decay 0.1, c 46.926809"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 46.926809 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 301.012143"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 301.012143 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 131.674569"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 131.674569 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 91.294255"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 91.294255 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 16.962487"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 16.962487 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 16.959629"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 16.959629 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 5.983877"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 5.983877 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 201.123086"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 201.123086 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 91.908215"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 91.908215 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 123.125006"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 123.125006 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 2.079931"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 2.079931 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 350.355748"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 350.355748 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 178.642954"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 178.642954 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 23.868763"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 23.868763 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 20.067899"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 20.067899 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 20.261142"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 20.261142 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 36.275373"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 36.275373 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 74.392783"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 74.392783 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 56.553707"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 56.553707 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"


  echo "MOS-KERNEL, decay 0.1, c 34.422299"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F -1 -L 0.1 -c 34.422299 $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k MOS-KERNEL $i /g" $TEMP_RESULT >> $RESULT_FILE".better"



done
 echo 'Done.' >> $RESULT_FILE