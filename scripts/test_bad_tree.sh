DATA_FILE=../datasets/tweets_auto-tagged-emolex_test_svmlight_TRU_vs_DIS.txt
MODEL_FILE="../models/classifier-tree-kernel-model_TRU_vs_DIS_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred1.txt
 

var=$(../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 ../logs/test_bad_tree.txt $MODEL_FILE $PRED_FILE)
echo 'var='$var
echo $var

DATA_FILE=../datasets/tweets_auto-tagged-emolex_test_svmlight_JOY_vs_SAD.txt
MODEL_FILE="../models/classifier-tree-kernel-model_JOY_vs_SAD_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred2.txt


 var=$(../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 ../logs/test_bad_tree.txt $MODEL_FILE $PRED_FILE)
 echo $var

DATA_FILE=../datasets/tweets_auto-tagged-emolex_test_svmlight_ANG_vs_FEA.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ANG_vs_FEA_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred3.txt

var=$(../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 ../logs/test_bad_tree.txt $MODEL_FILE $PRED_FILE)
echo $var

DATA_FILE=../datasets/tweets_auto-tagged-emolex_test_svmlight_ANT_vs_SUR.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ANT_vs_SUR_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred4.txt


var=$(../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 ../logs/test_bad_tree.txt $MODEL_FILE $PRED_FILE)
echo $var