
Unrecognized option -M!


Tree Kernels in SVM-light V5.01-TK-1.2 : SVM Learning module 15.11.06
by Alessandro Moschitti, moschitti@info.uniroma2.it
University of Rome "Tor Vergata"


Copyright: Thorsten Joachims, thorsten@ls8.cs.uni-dortmund.de

This software is available for non-commercial use only. It must not
be modified and distributed without prior permission of the author.
The author is not responsible for implications from the use of this
software.

   usage: svm_learn [options] example_file model_file

Arguments:
         example_file-> file with training data
         model_file  -> file to store learned decision rule in
General options:
         -?          -> this help
         -v [0..3]   -> verbosity level (default 1)
Learning options:
         -z {c,r,p}  -> select between classification (c), regression (r),
                        and preference ranking (p) (default classification)
         -c float    -> C: trade-off between training error
                        and margin (default [avg. x*x]^-1)
         -w [0..]    -> epsilon width of tube for regression
                        (default 0.1)
         -j float    -> Cost: cost-factor, by which training errors on
                        positive examples outweight errors on negative
                        examples (default 1) (see [4])
         -b [0,1]    -> use biased hyperplane (i.e. x*w+b>0) instead
                        of unbiased hyperplane (i.e. x*w>0) (default 1)
         -i [0,1]    -> remove inconsistent training examples
                        and retrain (default 0)
Performance estimation options:
         -x [0,1]    -> compute leave-one-out estimates (default 0)
                        (see [5])
         -o ]0..2]   -> value of rho for XiAlpha-estimator and for pruning
                        leave-one-out computation (default 1.0) (see [2])
         -k [0..100] -> search depth for extended XiAlpha-estimator 
                        (default 0)
Transduction options (see [3]):
         -p [0..1]   -> fraction of unlabeled examples to be classified
                        into the positive class (default is the ratio of
                        positive and negative examples in the training data)
Kernel options:
         -t int      -> type of kernel function:
                        0: linear (default)
                        1: polynomial (s a*b+c)^d
                        2: radial basis function exp(-gamma ||a-b||^2)
                        3: sigmoid tanh(s a*b + c)
                        4: user defined kernel from kernel.h
                        5: combination of forest and vector sets according to W, V, S, C options
                        11: re-ranking based on trees (each instance must have two trees),
                        12: re-ranking based on vectors (each instance must have two vectors)
                        13: re-ranking based on both tree and vectors (each instance must have
                            two trees and two vectors)  
         -W [S,A]    -> with an 'S', a tree kernel is applied to the sequence of trees of two input
                        forests and the results are summed;  
                     -> with an 'A', a tree kernel is applied to all tree pairs from the two forests
                        (default 'S')
         -V [S,A]    -> same as before but regarding sequences of vectors are used (default 'S' and
                        the type of vector-based kernel is specified by the option -S)
         -S [0,4]    -> kernel to be used with vectors (default polynomial of degree 3,
                        i.e. -S = 1 and -d = 3)
         -C [*,+,T,V]-> combination operator between forests and vectors (default 'T')
                     -> 'T' only the contribution from trees is used (specified by option -W)
                     -> 'V' only the contribution from vectors is used (specified by option -V)
                     -> '+' or '*' sum or multiplication of the contributions from vectors and 
                            trees (default T) 
         -D [0,1]    -> 0, SubTree kernel or 1, SubSet Tree kernels (default 1)
         -L float    -> decay factor in tree kernel (default 0.4)
         -S [0,4]    -> kernel to be used with vectors (default polynomial of degree 3, 
                        i.e. -S = 1 and -d = 3)
         -T float    -> multiplicative constant for the contribution of tree kernels when -C = '+'
         -N float    -> 0 = no normalization, 1 = tree normalization, 2 = vector normalization and 
                        3 = tree normalization of both trees and vectors. The normalization is applied 
                        to each individual tree or vector (default 3).
         -u string   -> parameter of user defined kernel
         -d int      -> parameter d in polynomial kernel
         -g float    -> parameter gamma in rbf kernel
         -s float    -> parameter s in sigmoid/poly kernel
         -r float    -> parameter c in sigmoid/poly kernel
         -u string   -> parameter of user defined kernel
Optimization options (see [1]):
         -q [2..]    -> maximum size of QP-subproblems (default 10)
         -n [2..q]   -> number of new variables entering the working set
                        in each iteration (default n = q). Set n<q to prevent
                        zig-zagging.
         -m [5..]    -> size of cache for kernel evaluations in MB (default 40)
                        The larger the faster...
         -e float    -> eps: Allow that error for termination criterion
                        [y [w*x+b] - 1] >= eps (default 0.001)
         -h [5..]    -> number of iterations a variable needs to be
                        optimal before considered for shrinking (default 100)
         -f [0,1]    -> do final optimality check for variables removed
                        by shrinking. Although this test is usually 
                        positive, there is no guarantee that the optimum
                        was found if the test is omitted. (default 1)
Output options:
         -l string   -> file to write predicted labels of unlabeled
                        examples into after transductive learning
         -a string   -> write all alphas to this file after learning
                        (in the same order as in the training set)

(more)

More details in:
[1] T. Joachims, Making Large-Scale SVM Learning Practical. Advances in
    Kernel Methods - Support Vector Learning, B. Sch�lkopf and C. Burges and
    A. Smola (ed.), MIT Press, 1999.
[2] T. Joachims, Estimating the Generalization performance of an SVM
    Efficiently. International Conference on Machine Learning (ICML), 2000.
[3] T. Joachims, Transductive Inference for Text Classification using Support
    Vector Machines. International Conference on Machine Learning (ICML),
    1999.
[4] K. Morik, P. Brockhausen, and T. Joachims, Combining statistical learning
    with a knowledge-based approach - A case study in intensive care  
    monitoring. International Conference on Machine Learning (ICML), 1999.
[5] T. Joachims, Learning to Classify Text Using Support Vector
    Machines: Methods, Theory, and Algorithms. Dissertation, Kluwer,
    2002.


For Tree-Kernel details:
[6] A. Moschitti, A study on Convolution Kernels for Shallow Semantic Parsing.
    In proceedings of the 42-th Conference on Association for Computational
    Linguistic, (ACL-2004), Barcelona, Spain, 2004.

[7] A. Moschitti, Making tree kernels practical for natural language learning.
    In Proceedings of the Eleventh International Conference for Computational
    Linguistics, (EACL-2006), Trento, Italy, 2006.

