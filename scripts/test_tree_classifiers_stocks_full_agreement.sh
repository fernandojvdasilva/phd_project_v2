MODEL_FILE="../models/classifier-tree-kernel-model_JOY_vs_SAD_BEST_PARS.txt"
TEST_FILE="../datasets/full_agreement/tweets_stocks_emolex_svmlight_JOY_vs_SAD.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_stocks_full_agreement_JOY_vs_SAD.txt"
LOG_FILE="../logs/test-tree-kernel-model_stocks_full_agreement_JOY_vs_SAD.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE &

MODEL_FILE="../models/classifier-tree-kernel-model_TRU_vs_DIS_BEST_PARS.txt"
TEST_FILE="../datasets/full_agreement/tweets_stocks_emolex_svmlight_TRU_vs_DIS.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_stocks_full_agreement_TRU_vs_DIS.txt"
LOG_FILE="../logs/test-tree-kernel-model_stocks_full_agreement_TRU_vs_DIS.log"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE &

MODEL_FILE="../models/classifier-tree-kernel-model_ANG_vs_FEA_BEST_PARS.txt"
TEST_FILE="../datasets/full_agreement/tweets_stocks_emolex_svmlight_ANG_vs_FEA.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_stocks_full_agreement_ANG_vs_FEA.txt"
LOG_FILE="../logs/test-tree-kernel-model_stocks_full_agreement_ANG_vs_FEA.log"

../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE &

MODEL_FILE="../models/classifier-tree-kernel-model_ANT_vs_SUR_BEST_PARS.txt"
TEST_FILE="../datasets/full_agreement/tweets_stocks_emolex_svmlight_ANT_vs_SUR.txt"
PRED_FILE="../logs/results-test-tree-kernel-model_stocks_full_agreement_ANT_vs_SUR.txt"
LOG_FILE="../logs/test-tree-kernel-model_stocks_full_agreement_ANT_vs_SUR.log"

../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 $TEST_FILE $MODEL_FILE $PRED_FILE > $LOG_FILE &