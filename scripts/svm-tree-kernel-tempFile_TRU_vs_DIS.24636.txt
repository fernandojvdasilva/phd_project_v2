Scanning examples...done
Reading examples into memory...100..200..300..400..500..600..700..800..900..1000..1100..1200..1300..1400..1500..1600..1700..1800..1900..2000..2100..2200..2300..2400..2500..2600..2700..2800..2900..3000..3100..3200..3300..3400..3500..3600..3700..3800..OK. (3815 examples read)

Number of examples: 3815, linear space size: 0

estimating ...
Setting default regularization parameter C=1.0000
Optimizing..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
 Checking optimality of inactive variables...done.
 Number of inactive variables = 2290
done. (3659 iterations)
Optimization finished (174 misclassified, maxdiff=0.00099).
Runtime in cpu-seconds: 14.53
Number of SV: 2785 (including 1352 at upper bound)
L1 loss: loss=684.05450
Norm of weight vector: |w|=36.44895
Norm of longest example vector: |x|=1.00000
Estimated VCdim of classifier: VCdim<=1329.52620
Computing XiAlpha-estimates...done
Runtime for XiAlpha-estimates in cpu-seconds: 0.01
XiAlpha-estimate of the error: error<=35.44% (rho=1.00,depth=0)
XiAlpha-estimate of the recall: recall=>76.04% (rho=1.00,depth=0)
XiAlpha-estimate of the precision: precision=>70.39% (rho=1.00,depth=0)
Number of kernel evaluations: 8398525
Writing model file...done
