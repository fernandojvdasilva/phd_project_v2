#Best Parameters for JOY_vs_SAD: prec    97.316667
#rec     98.310000
#f1      97.810802
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#Best Parameters for TRU_vs_DIS: prec    84.540000
#rec     90.546667
#f1      87.436685
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#Best Parameters for ANG_vs_FEA: prec    96.936667
#rec     99.263333
#f1      98.086084
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#Best Parameters for ANT_vs_SUR: prec     96.306667
#rec     100.000000
#f1       98.118456
#Name: (SST-bow-KERNEL, 0.1), dtype: float64

#SVM_TREE_ALGS = { "ST-KERNEL": {"type": '0', "decay": "L"}, 
#                  "SST-KERNEL": {"type": '1', "decay": "L"},
#				   "SST-bow-KERNEL": {"type": '2', "decay": "L"},
#				  "PT-KERNEL": {"type": '3', "decay": "M"}}


# The SST-bow-KERNEL with 0.1 decay has shown the best results for all classifiers

for k in `seq -1 3`
do

BEST_DECAY=0.1


MODEL_FILE="../models/classifier-tree-kernel-model_JOY_vs_SAD_BEST_PARS_KERNEL_"$k".txt"
DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_JOY_vs_SAD.txt"


../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $k -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1 &

MODEL_FILE="../models/classifier-tree-kernel-model_TRU_vs_DIS_BEST_PARS_KERNEL_"$k".txt"
DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_TRU_vs_DIS.txt"

../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $k -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1 &


MODEL_FILE="../models/classifier-tree-kernel-model_ANG_vs_FEA_BEST_PARS_KERNEL_"$k".txt"
DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ANG_vs_FEA.txt"

../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $k -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1 &


MODEL_FILE="../models/classifier-tree-kernel-model_ANT_vs_SUR_BEST_PARS_KERNEL_"$k".txt"
DATA_FILE="../datasets/tweets_auto-tagged-emolex_train_svmlight_ANT_vs_SUR.txt"

../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F $k -L $BEST_DECAY $DATA_FILE $MODEL_FILE 2>&1 &

done