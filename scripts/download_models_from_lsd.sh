#!/bin/bash


DIR_NAME=$(date '+%Y-%m-%d')
mkdir $DIR_NAME

REMOTE_DIR=phd_project/experiments


scp fernandojvdasilva@lsd00.lsd.ic.unicamp.br:~/$REMOTE_DIR/../*.log $DIR_NAME 
scp fernandojvdasilva@lsd00.lsd.ic.unicamp.br:~/*.log $DIR_NAME 
scp fernandojvdasilva@lsd00.lsd.ic.unicamp.br:~/$REMOTE_DIR/*.pkl $DIR_NAME 
