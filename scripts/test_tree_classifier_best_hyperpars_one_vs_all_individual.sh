#!/bin/bash
# Usage: svm_light_cross_validation full_data num_train_items num_test_items k_cycles results_file
# based in http://niketanblog.blogspot.com/2012/02/cross-validation-on-svm-light.html
DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_FEA.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_FEA_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"



DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_ANG.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_ANG_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"


DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_JOY.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_JOY_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"


DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SAD.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_SAD_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"


DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_TRU.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_TRU_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"

DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_DIS.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_DIS_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"


DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_SUR.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_SUR_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"


DATA_FILE=../datasets/tweets_auto-tagged-emolex_train_svmlight_ALL_vs_ANT.txt
MODEL_FILE="../models/classifier-tree-kernel-model_ALL_vs_ANT_BEST_PARS.txt"
PRED_FILE=../logs/tmp_check_bug_file_pred.txt
 
input=$DATA_FILE
while IFS= read -r var
do
  echo "$var" > tmp_check_bug_file.txt
  echo $var
  #../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_learn -t 5 -F 0 -L 0.10 tmp_check_bug_file.txt tmp_check_bug_model_file.txt  &
  ../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/svm_classify -v 1 tmp_check_bug_file.txt $MODEL_FILE $PRED_FILE
done < "$input"

echo "DONE!"


echo "DONE!"