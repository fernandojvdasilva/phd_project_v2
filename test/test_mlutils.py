'''
Created on 17/07/19

@author: fernando
'''
import unittest

import sys
import os

sys.path.append(os.path.join(os.getcwd(), '../'))

import pandas as pd
import numpy as np

from mlutils.resampling import ml_upsample

import pprint

class Test(unittest.TestCase):

    def testUpsample(self):
        df = pd.DataFrame({"Person":
                    ["John", "Myla", "Lewis", "John", "Myla"],
                    "Age": [24., np.nan, 21., 33, 26],
                    "Single": [0, 1, 1, 1, 1]})

        df, indx = ml_upsample(df, "Single")

        pprint.pprint(df)
        pprint.pprint(indx)

        assert(not df is None)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testTreeUnderscore', 'Test.testTreeEndHashtag']
    unittest.main()
