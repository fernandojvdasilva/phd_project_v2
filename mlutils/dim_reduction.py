'''
Created on Nov 29, 2016

@author: fernando
'''

from sklearn.decomposition import TruncatedSVD


def num_dimensions_by_variance(variance, explained_variance):
    variance_ratios = sorted(explained_variance)[:-1]
    cumulative_variance = 0
    num_dimensions = 0
    for var in variance_ratios:
        cumulative_variance = cumulative_variance + var
        num_dimensions += 1
        if cumulative_variance > (float(variance) / 100):
            break
    
    return num_dimensions

def svd_reduct(X_train, X_test, cum_variance, svd_transformer=None):
    
    if svd_transformer is None:
        # Starts reducing to the half of dimensions. Then reduces to the minimum cumulative variance
        try:
            svd_transformer = TruncatedSVD(n_components=X_train.shape[1]/2)
            svd_transformer.fit(X_train)
        except:
            svd_transformer = TruncatedSVD(n_components=200)
            svd_transformer.fit(X_train)
    
    num_dimensions = num_dimensions_by_variance(cum_variance, svd_transformer.explained_variance_ratio_)

    if num_dimensions > 0 and svd_transformer is None:
        svd_transformer = TruncatedSVD(n_components=num_dimensions)
        svd_transformer.fit(X_train)
        
    svd_X_train = svd_transformer.transform(X_train)
    svd_X_test = svd_transformer.transform(X_test)
        
    return svd_X_train, svd_X_test, num_dimensions, svd_transformer