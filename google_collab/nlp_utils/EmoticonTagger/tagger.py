# encoding: utf-8
'''
Created on 8 de jun de 2017

@author: fernando
'''

EMOTICON_LIST = {'Anger': ['>:[', ':{', ':@', '>:(', \
                           u'😠', u'😡'],
                 
                'Anticipation': [],
                
                'Disgust': ["D‑':", 'D:<', 'D:', 'D8', 'D;', 'D=', 'DX', \
                         u'😨', u'😧', u'😦', u'😱', u'😫', u'😩'],
                 
                'Fear': ["D‑':", 'D:<', 'D:', 'D8', 'D;', 'D=', 'DX', \
                         u'😨', u'😧', u'😦', u'😱', u'😫', u'😩'],
                 
                'Joy': [':‑)', ':)', ':-]', ':]', ':-3', ':3', ':->', ':>', '8-)', '8)', ':-}', \
                        ':}', ':o)', ':c)', ':^)', '=]', '=)', ':‑D', ':D', '8‑D', '8D', 'x‑D', \
                        'xD', 'X‑D', 'XD', '=D', '=3', 'B^D', ':-))',  ":'‑)", ":')", \
                        u'☺', u'😊', u'😀', u'😁', u'😃', u'😄', u'😆', u'😍', u'😂', u'😘', u'😍'],
                 
                'Sadness': [':‑(', ':(', ':‑c', ':c', ':‑<', ':<', ':‑[', ':[', ':-||',  ":'‑(", \
                            ":'(", "D‑':", 'D:<', 'D:', 'D8', 'D;', 'D=', 'DX',\
                            u'☹️', u'😟', u'😞', u'😖', u'😣', u'😢', u'😭', u'😨', u'😔' \
                            u'😧', u'😦', u'😱', u'😫', u'😩'],
                 
                'Surprise': [':‑O', ':O', ':‑o', ':o', ':-0', '8‑0', '>:O', \
                             u'😮', u'😯', u'😲'],
                
                'Trust': [u'😘', u'😍']
                }


class EmoticonTagger():    


    def __init__(self):
        pass
    
    
    def tag(self, word):
        emoticon_found = False
        tagged_emotions = { 'Anger': False,
                            'Anticipation': False,
                            'Disgust': False,
                            'Fear': False,
                            'Joy': False,
                            'Sadness': False,
                            'Surprise': False,
                            'Trust': False}

        for k in tagged_emotions.keys():
            if word in EMOTICON_LIST[k]:
                tagged_emotions[k] = True
                emoticon_found = True
                
        if not emoticon_found:
            return None
            
        return tagged_emotions
    
    def tagWordList(self, wordList):
        tagged_words = []
        for w in wordList:
            tagged_words.append(self.tag(w))
        
        return tagged_words
        