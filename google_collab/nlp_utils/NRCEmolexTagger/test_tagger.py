'''
Created on 2 de jun de 2017

@author: fernando
'''
import unittest

from nlp_utils.NRCEmolexTagger.tagger import NRCEmolexTagger

class Test(unittest.TestCase):


    def testTagWordList(self):
        tagger = NRCEmolexTagger()
        
        tags = tagger.tagWordList(u'no sabado vou ao breakout porto escape game #medo'.split(' '), 
                                  ['pt', 'en'])
        
        assert(len(tags) > 0)
        
        tags = tagger.tagWordList(u'o guri usou maçarico pra dourar a empanada... sério, só eu que tô me sentindo um idiota? #masterchefbr #depressão'.split(' '), 
                                  ['pt', 'en'])
        
        assert(len(tags) > 0)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testTagWordList']
    unittest.main()