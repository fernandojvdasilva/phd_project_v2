'''
Created on 25 de abr de 2017

@author: fernando
'''
import pandas as pd
from math import nan

def fleiss_kappa(dataframe, num_raters, debug=False):
    if dataframe.shape[0] == 0 or num_raters == 0:
        return nan
    p = dataframe.sum(axis=0)
    p = p / (num_raters * dataframe.shape[0])
    
    if debug:
        import pprint
        pprint.pprint(dataframe)
    
    if debug:
        print("pj:")
        pprint.pprint(p)
        
    p = p**2
    
    P_e = p.sum()    
    
    P = dataframe**2    
    
    
    P = (1.0 / (num_raters * (num_raters-1))) * (P.sum(axis=1) - num_raters)
    
    if debug:
        print("Pi:")
        pprint.pprint(P)
    
    P_ = (1.0 / dataframe.shape[0]) * P.sum()
    
    # Avoid division by zero
    if P_e == 1:
        return nan
    
    if debug:
        print("Pe:")
        pprint.pprint(P_e)
    
    k = (P_ - P_e) / (1 - P_e)
    
    k_max = (P_ ** 2) / ((1-P_)**2 + 1)
    
    k_min = (P_ - 1) / (P_ + 1)
    
    k_nor = 2*P_ - 1 
    
    return k, k_max, k_min, k_nor


''' Calculate the fleiss kappa per category '''
def fleiss_kappa_ex(dataframe, num_raters):
    if dataframe.shape[0] == 0 or num_raters == 0:
        return None, None
    
    pj = dataframe.sum(axis=0)
    pj = pj / (num_raters * dataframe.shape[0])
    
    num_annot = dataframe**2
    num_annot = num_annot.sum(axis=0)
    
    Pj_up = num_raters*dataframe.shape[0]*pj
    Pj_down = num_raters*dataframe.shape[0]*(num_raters-1)*pj 
    
    P_j = (num_annot -  Pj_up) / Pj_down
    
   
    k = (P_j - pj) / (1 - pj) 
    
    return pj, k