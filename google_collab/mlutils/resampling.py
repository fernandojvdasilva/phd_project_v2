__author__ = 'Fernando J. V. da Silva'

import numpy as np

def ml_upsample(df, class_label):
    num_lines_class_one = len(df.loc[df[class_label] == 1])
    num_lines_class_all = len(df.loc[df[class_label] == 0])


    if num_lines_class_one < num_lines_class_all:
      class_to_upsample = 1
      num_lines = num_lines_class_all - num_lines_class_one
    elif num_lines_class_all < num_lines_class_one:
      class_to_upsample = 0
      num_lines = num_lines_class_one - num_lines_class_all
    
    df_minor_class = df.loc[df[class_label] == class_to_upsample]

    i = 0
    indexes = []
    while i < num_lines:      
      for index, row in df_minor_class.iterrows():
        if i == num_lines:
          break        

        if row[class_label] == class_to_upsample:
          i += 1
          df = df.append(row)
          indexes.append(index)

    return df, indexes


def ml_downsample(df, class_label):
	num_lines_class_one = len(df.loc[df[class_label] == 1])

	# TODO: Ramdomly select a sample of 0 class
	pass
            
