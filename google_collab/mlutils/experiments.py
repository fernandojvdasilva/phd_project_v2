# -*- encoding: utf-8 -*-
from threading import Thread
from datetime import *
from sklearn.metrics import *

__author__ = 'Fernando J. V. da Silva'

#from sklearn import cross_validation
from sklearn import model_selection
import numpy as np
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import classification_report
from sklearn.model_selection import StratifiedKFold

def kfoldIndexes(data, targets, k):
    skf = StratifiedKFold(n_splits=k)
    
    kfolds = []
    for train_indexes, test_indexes in skf.split(data, targets):
        kfolds.append({"train": train_indexes, "test": test_indexes})
    
    return kfolds

    

def kfold(data, targets, k):
    TR_SET = []
    TE_SET = []
    kfolds = StratifiedKFold(y=targets, n_folds=k)

    for tr_indexes, te_indexes in kfolds:
        tr_fold = np.ndarray(shape=(0,data.shape[1]))
        te_fold = np.ndarray(shape=(0,data.shape[1]))
        tr_targets = np.ndarray(shape=(0, 1))
        te_targets = np.ndarray(shape=(0, 1))

        for i in tr_indexes:
            tr_fold = np.append(tr_fold, [data[i]], axis=0)
            tr_targets = np.append(tr_targets, targets[i])

        for i in te_indexes:
            te_fold = np.append(te_fold, [data[i]], axis=0)
            te_targets = np.append(te_targets, targets[i])

        TR_SET.append(dict(data=tr_fold, targets=tr_targets))
        TE_SET.append(dict(data=te_fold, targets=te_targets))

    return TR_SET, TE_SET

def folding(data, targets, k):
    TR_EXT, TE_EXT = kfold(data, targets, k)
    TR_INT = []
    TE_INT = []
    for TR, TE in zip(TR_EXT, TE_EXT):
        CURR_TR_INT, CURR_TE_INT = kfold(TR['data'], TR['targets'], k)
        TR_INT.append(CURR_TR_INT)
        TE_INT.append(CURR_TE_INT)

    return TR_EXT, TE_EXT, TR_INT, TE_INT

def train(data, targets, H):
    H.fit(data, targets)
    return H

def test(classific, test_data, test_targets):
    error_count = 0

    false_positive = 0
    true_positive = 0
    false_negative = 0
    true_negative = 0

    for i in range(len(test_data)):
        result = classific.predict(test_data[i])
        if test_targets[i] == 1.0:
            if result[0] == 1.0:
                true_positive += 1
            else:
                false_negative += 1
        else:
            if result[0] == 1.0:
                false_positive += 1
            else:
                true_negative += 1

        if test_targets[i] != result[0]:
                error_count += 1

    acc = (float(len(test_data)-error_count) / len(test_data))

    return true_positive, false_positive, true_negative, false_negative, acc

def calc_precision(true_positive, false_positive):
    ''' precision é o número de acertos em relação aos que o algoritmo diz estarem corretos '''
    if (true_positive + false_positive) == 0 :
        precision = 0.0
    else:
        precision = float(true_positive) / float(true_positive + false_positive)

    return precision

def calc_recall(true_positive, false_negative):
    ''' recall é o número de acertos em relação aos acertos reais existentes '''
    if (true_positive + false_negative) == 0:
        recall = 0.0
    else:
        recall = float(true_positive) / float(true_positive + false_negative)

    return recall

def calc_f_measure(true_positive, false_positive, true_negative, false_negative):
    precision = calc_precision(true_positive, false_positive)
    recall = calc_recall(true_positive, false_negative)

    if precision + recall == 0:
        f_measure = 0.0
    else:
        f_measure = 2*(precision*recall)/(precision+recall)

    return f_measure

def alg_evaluate(alg_name, exp_name, hipergrid, TR_SET, TE_SET, TR2_SET, TE2_SET):

    k = len(TR_SET)

    accur = 0
    true_positive = 0
    false_positive = 0
    true_negative = 0
    false_negative = 0

    for i in range(len(TR_SET)):
        TR = TR_SET[i]
        TE = TE_SET[i]

        max_f_measure=0.0
        max_recall=0.0
        max_precision=0.0
        max_acc = 0.0
        max_H = None

        for H in hipergrid:

            f_measure = 0
            accur2 = 0
            true_positive2 = 0
            false_positive2 = 0
            true_negative2 = 0
            false_negative2 = 0
            for TR2, TE2 in zip(TR2_SET[i], TE2_SET[i]):
                class2 = train(TR2['data'], TR2['targets'], H)
                tp, fp, tn, fn, acc = test(class2, \
                                           TE2['data'], \
                                           TE2['targets'])
                true_positive2 += tp
                false_positive2 += fp
                true_negative2 += tn
                false_negative2 += fn
                accur2 += acc

            f_measure = calc_f_measure(true_positive2, false_positive2, true_negative2, false_negative2)

            if f_measure > max_f_measure:
                max_f_measure = f_measure
                max_recall = calc_recall(true_positive2, false_negative2)
                max_precision = calc_precision(true_positive2, false_positive2)
                max_H = H
                max_acc = accur2/k

        classific = train(TR['data'], TR['targets'], max_H)
        tp, fp, tn, fn, acc = test(classific, TE['data'], TE['targets'])

        true_positive += tp
        false_positive += fp
        true_negative += tn
        false_negative += fn
        accur += acc

    f_measure = calc_f_measure(true_positive, false_positive, true_negative, false_negative)
    recall = calc_recall(true_positive, false_negative)
    precision = calc_precision(true_positive, false_positive)

    return f_measure, recall, precision, max_H, accur/k


class AlgEvalThread(Thread):
    def __init__(self, results, exp_name, alg_name, H, TR_SET, TE_SET, TR2_SET, TE2_SET):
        self.exp_name = exp_name
        self.alg_name = alg_name
        self.H = H
        self.TR_SET = TR_SET
        self.TE_SET = TE_SET
        self.TR2_SET = TR2_SET
        self.TE2_SET = TE2_SET
        self.results = results

        self.results[self.exp_name][self.alg_name] = dict(f_measure=0.0, recall=0.0, precision=0.0, train=0.0, acc=0.0)

        Thread.__init__(self)

    def run(self):
        f_measure, recall, precision, max_H, acc = alg_evaluate(self.alg_name, self.exp_name, self.H, \
                                                                     self.TR_SET, self.TE_SET, self.TR2_SET, self.TE2_SET)

        print('---> F-Measure %s para %s: %.3f ' % (self.alg_name, self.exp_name, f_measure))

        self.results[self.exp_name][self.alg_name]['f_measure'] = f_measure
        self.results[self.exp_name][self.alg_name]['recall'] = recall
        self.results[self.exp_name][self.alg_name]['precision'] = precision
        self.results[self.exp_name][self.alg_name]['acc'] = acc
        
        

def ml_test_classifier(X_test, Y_test, target_names, clf, corpus, score='f1', start_datetime=None, label=None):
    if start_datetime is None:
        start_datetime = datetime.now()
    Y_true, Y_pred = Y_test, clf.predict(X_test)
    end_datetime = datetime.now()
    report_file = open('report.log', 'a')
    report_file.write('=============================\n')
    report_file.write("CORPUS: %s\n" % corpus)
    if not label is None:
        report_file.write("Additional Info: %s\n" % label)
    report_file.write('Started at %s and finished at %s \n' % (start_datetime.strftime("%d/%m/%y 0:%I:%M%p"), end_datetime.strftime("%d/%m/%y 0:%I:%M%p")))    
    report_file.write(classification_report(Y_true, Y_pred, target_names=target_names))
    report_file.write('\n')
    report_file.close()
    
    
    if score == 'f1':
        score_val = f1_score(Y_true, Y_pred)
    elif score == 'precision':
        score_val = precision_score(Y_true, Y_pred)
    elif score == 'recall':
        score_val = recall_score(Y_true, Y_pred)
    else:
        score_val = accuracy_score(Y_true, Y_pred)        
        
    return score_val
    
NUM_JOBS=7
#NUM_JOBS=2

N_ITER=20
#N_ITER=1


def ml_hyperpar_selection_random(classifier, tuned_parameters, score, X_train, X_test, Y_train, Y_test, target_names, corpus, label=None):
    start_datetime = datetime.now()
    clf = RandomizedSearchCV(classifier, tuned_parameters, cv=3, 
        scoring=score, n_jobs=NUM_JOBS, n_iter=N_ITER, verbose=99)
    clf.fit(X_train, Y_train)
    print("Best parameters set found on development set:")    
    print(clf.best_params_)
    
    #print "Grid scores on development set:"
    #for params, mean_score, scores in clf.grid_scores_:
    #    print "%0.3f (+/-%0.03f) for %r" % (mean_score, scores.std() * 2, params)
    
    
    print("Detailed classification report:")    
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    
    score_val = ml_test_classifier(X_test, Y_test, target_names, clf, corpus, score, start_datetime, label)        
    
    return clf, score_val      
        

def dl_test_classifier(X_test, Y_test, target_names, clf, corpus, score='f1', start_datetime=None, label=None):
    if start_datetime is None:
        start_datetime = datetime.now()
    Y_true, Y_pred = Y_test, [int(pred > 0.5) for pred in clf.predict(X_test)]
    end_datetime = datetime.now()
    report_file = open('report.log', 'a')
    report_file.write('=============================\n')
    report_file.write("CORPUS: %s\n" % corpus)
    if not label is None:
        report_file.write("Additional Info: %s\n" % label)
    report_file.write('Started at %s and finished at %s \n' % (start_datetime.strftime("%d/%m/%y 0:%I:%M%p"), end_datetime.strftime("%d/%m/%y 0:%I:%M%p")))    
    report_file.write(classification_report(Y_true, Y_pred, target_names=target_names))
    report_file.write('\n')
    report_file.close()
    
    
    if score == 'f1':
        score_val = f1_score(Y_true, Y_pred)
    elif score == 'precision':
        score_val = precision_score(Y_true, Y_pred)
    elif score == 'recall':
        score_val = recall_score(Y_true, Y_pred)
    else:
        score_val = accuracy_score(Y_true, Y_pred)        
        
    return score_val

        
DL_BATCH_SIZE=64
DL_EPOCHS=10       
def dl_train(classifier, score, X_train, X_test, Y_train, Y_test, target_names, corpus, label=None):
    start_datetime = datetime.now()
    
    classifier.fit(X_train, Y_train, verbose=2, batch_size=DL_BATCH_SIZE, 
            epochs=DL_EPOCHS, validation_data=(X_test, Y_test))
        
    print("Model Summary:")    
    print(classifier.summary())    
    
    print("Detailed classification report:")    
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    
    score_val = dl_test_classifier(X_test, Y_test, target_names, classifier, corpus, score, start_datetime, label)        
    
    return classifier, score_val      
