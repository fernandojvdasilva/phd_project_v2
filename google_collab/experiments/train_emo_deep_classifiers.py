
'''
Created on 19 de out de 2017

@author: fernando
'''
import sys
import os

sys.path.append(os.getcwd())

import pickle
import logging
import numpy as np
from datetime import datetime, date, time
import pandas as pd
from nlp_utils.nlp_lexical import lex_tokenize
from nlp_utils.nlp_semantic import *
from nlp_utils.nlp_bag_of_words import *
from mlutils.experiments import *
from mlutils.dim_reduction import *
import traceback
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten, Input, MaxPooling1D, Convolution1D, Embedding
from keras.preprocessing.sequence import pad_sequences
from nlp_utils.w2v import train_word2vec, np_to_indexes
from copy import copy
from keras.layers.merge import Concatenate
from experiments.experiments_utils import extract_emos_n_class

CLASS_INDEX_TRU = 0
CLASS_INDEX_DIS = 1

CLASS_INDEX_JOY = 2
CLASS_INDEX_SAD = 3

CLASS_INDEX_ANT = 4
CLASS_INDEX_SUR = 5

CLASS_INDEX_ANG = 6
CLASS_INDEX_FEAR = 7

CLASS_INDEX_NEUTRAL = 8



def experiment_one_vs_one_deep(classifiers, dataframe_train, dataframe_test,  \
                          emo1, emo2, corpus, score, label, vocabulary_inv, max_sentence_len):
    
    emos_class_train, indexes_train = extract_emos_n_class(dataframe_train, emo1, emo2)
    emos_class_test, indexes_test = extract_emos_n_class(dataframe_test,  emo1, emo2)     
    
    #X_train, Y_train, X_test, Y_test = df_tfidf_train_test_split(emos_class, emos_attrs, 0.8)
    dataframe_train = dataframe_train.iloc[indexes_train]
    dataframe_test = dataframe_test.iloc[indexes_test]         
    
    X_train = np_to_indexes(dataframe_train['wordlist'], vocabulary_inv, max_sentence_len)
    Y_train = emos_class_train
    X_test = np_to_indexes(dataframe_test['wordlist'], vocabulary_inv, max_sentence_len)
    Y_test = emos_class_test
    
    
    best_clf = None
    best_accuracy = -1.0
    best_clf_name = ""
    for clf_config in classifiers:
        exp_label = 'Evaluating classifier %s: \n' % clf_config['clf-name']
        print(exp_label) 
        try:
            clf, accuracy = dl_train(clf_config['classifier'], score, \
                                    X_train, X_test, Y_train, Y_test, [emo2, emo1], corpus, exp_label)        
        
            # Saving current classifier
            if label is None:
                filename = 'models/classifier_dl_%s_vs_%s_%s.h5' % (emo1, emo2, clf_config['clf-name'])
            else:
                filename = 'models/classifier_dl_%s_vs_%s_%s_%s.h5' % (emo1, emo2, label, clf_config['clf-name'])
            
            clf.save(filename)
            
            if accuracy > best_accuracy:            
                best_accuracy = accuracy
                best_clf = clf
                best_clf_name = clf_config['clf-name']
        
        
        except Exception as ex:                    
            print(repr(ex) + "\n")
            traceback.print_exc()
        
        
            
    print("Best Classifier: %s\n" % best_clf_name)
    print("Best Accuracy: %f\n" % best_accuracy)
    
    
    # Saving best classifier
   
    if label is None:
        filename = 'models/classifier_dl_%s_vs_%s.h5' % (emo1, emo2)
    else:
        filename = 'models/classifier_dl_%s_vs_%s_%s.h5' % (emo1, emo2, label)
    
    best_clf.save(filename)
    

def experiment_one_vs_all_deep(classifiers, dataframe_train, dataframe_test,  \
                          emo, corpus, score, label, vocabulary_inv, max_sentence_len):
       
    X_train = np_to_indexes(dataframe_train['wordlist'], vocabulary_inv, max_sentence_len)
    Y_train = dataframe_train[emo]
    X_test = np_to_indexes(dataframe_test['wordlist'], vocabulary_inv, max_sentence_len)
    Y_test = dataframe_test[emo]
    
    
    best_clf = None
    best_accuracy = -1.0
    best_clf_name = ""
    for clf_config in classifiers:
        exp_label = 'Evaluating classifier %s: \n' % clf_config['clf-name']
        print(exp_label) 
        try:
            clf, accuracy = dl_train(clf_config['classifier'], score, \
                                    X_train, X_test, Y_train, Y_test, ['all', emo], corpus, exp_label)        
        
            # Saving current classifier
            if label is None:
                filename = 'models/classifier_dl_all_vs_%s_%s.h5' % (emo, clf_config['clf-name'])
            else:
                filename = 'models/classifier_dl_all_vs_%s_%s_%s.h5' % (emo, label, clf_config['clf-name'])
            
            clf.save(filename)
            
            if accuracy > best_accuracy:            
                best_accuracy = accuracy
                best_clf = clf
                best_clf_name = clf_config['clf-name']
        
        
        except Exception as ex:                    
            print(repr(ex) + "\n")
            traceback.print_exc()
        
        
            
    print("Best Classifier: %s\n" % best_clf_name)
    print("Best Accuracy: %f\n" % best_accuracy)
    
    
    # Saving best classifier
   
    if label is None:
        filename = 'models/classifier_dl_%s_vs_%s.h5' % (emo1, emo2)
    else:
        filename = 'models/classifier_dl_%s_vs_%s_%s.h5' % (emo1, emo2, label)
    
    best_clf.save(filename)



def benchmark_deep_train(dataframe_path, clf_names, score, emotions=None):
    try:
        dataframe_train = pd.read_csv(dataframe_path.replace('.csv', '_train.csv'), engine='c')
        dataframe_test = pd.read_csv(dataframe_path.replace('.csv', '_test.csv'), engine='c')               
                        
    except Exception as ex:
        dataframe_full = pd.read_csv(dataframe_path, engine='c')        
        
    
        dataframe_train, dataframe_test, indexes = \
        df_train_test_split(dataframe_full, 0.8)
        
        # Saving split data
        dataframe_train.to_csv(dataframe_path.replace('.csv', '_train.csv'))
        dataframe_test.to_csv(dataframe_path.replace('.csv', '_test.csv'))
    
    # DEBUGGING ONLY
    #dataframe_train = dataframe_train.iloc[:1000]
    #dataframe_test = dataframe_test.iloc[:1000]
    
    dataframe_train['wordlist'] = np.array([sem_tokenize_stock_corpus_no_stem(tweet_text) 
                                            for tweet_text in dataframe_train['text']])
    dataframe_test['wordlist'] = np.array([sem_tokenize_stock_corpus_no_stem(tweet_text) 
                                           for tweet_text in dataframe_test['text']])
    
    
    # Building list of words
    dataframe_train_words = [w for wordlist in dataframe_train['wordlist'] for w in wordlist]        
    dataframe_test_words = [w for wordlist in dataframe_test['wordlist'] for w in wordlist]
    
    dataframe_all_words = list(set(dataframe_train_words+dataframe_test_words))
    
    # This experiment was based in the CNN developed in 
    # https://github.com/alexander-rakhlin/CNN-for-Sentence-Classification-in-Keras/blob/master/sentiment_cnn.py
    
    vocabulary_inv = {word: index for index, word in enumerate(dataframe_all_words)}
    
    with open('datasets/vocabulary_inv.pkl', 'wb') as handle:
            pickle.dump(vocabulary_inv, handle)
    
    maxlen = max([len(w) for w in dataframe_all_words])  
    
    #x_train = [[vocabulary_inv[c] for c in lex_tokenize(x)] for x in dataframe_train['text']]
    #x_test = [[vocabulary_inv[c] for c in lex_tokenize(x)] for x in dataframe_test['text']]
    
    x_train = [[c for c in lex_tokenize(x)] for x in dataframe_train['text']]
    x_test = [[c for c in lex_tokenize(x)] for x in dataframe_test['text']]
    
    #x_train = pad_sequences(x_train, maxlen=maxlen)
    #x_test = pad_sequences(x_test, maxlen=maxlen)
    
    max_features = len(dataframe_all_words)
        
    # Model Hyperparameters
    embedding_dim = 50
    filter_sizes = (3, 8)
    num_filters = 10
    dropout_prob = (0.5, 0.8)
    hidden_dims = 50
    
    # Training parameters
    batch_size = 64
    num_epochs = 10

    # Word2Vec parameters (see train_word2vec)
    min_word_count = 1
    context = 10
    
    # Preprocessing parameters
    sequence_length = 400
    max_words = 5000

    
    embedding_weights = train_word2vec(x_train + x_test, vocabulary_inv, 
                                       num_features=embedding_dim,
                                        min_word_count=min_word_count, context=context,
                                        model_dir='models')
    
     

    if emotions is None:
        emotions = [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT', 'SUR')]


    models = []
    for _ in range(len(emotions)):
          
        input_shape = (sequence_length,)
    
        model_input = Input(shape=input_shape)
        
        z = Embedding(len(vocabulary_inv), embedding_dim, input_length=sequence_length, name="embedding")(model_input)
      
        z = Dropout(dropout_prob[0])(z)
    
        conv_blocks = []
        for sz in filter_sizes:
            conv = Convolution1D(filters=num_filters,
                                 kernel_size=sz,
                                 padding="valid",
                                 activation="relu",
                                 strides=1)(z)
            conv = MaxPooling1D(pool_size=2)(conv)
            conv = Flatten()(conv)
            conv_blocks.append(conv)
        z = Concatenate()(conv_blocks) if len(conv_blocks) > 1 else conv_blocks[0]
    
        z = Dropout(dropout_prob[1])(z)
        
        z = Dense(hidden_dims, activation="relu")(z)
        model_output = Dense(1, activation="sigmoid")(z)
        
        model = Model(model_input, model_output)
        model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])
                
        weights = np.array([v for v in embedding_weights.values()])
        print("Initializing embedding layer with word2vec weights, shape", weights.shape)
        embedding_layer = model.get_layer("embedding")
        embedding_layer.set_weights([weights])

        print(model.summary())

        models.append(model)
            
    print('Running one vs one experiments...')     
         
    for emo in emotions:
        #if not os.path.exists('experiments/classifier_dl__%s_vs_%s_emolex.h5' % (emo[0], emo[1])):
        print('--> Training %s vs %s ...' % (emo[0], emo[1]))
        experiment_one_vs_one_deep([{'clf-name': 'cnn', 'classifier': models.pop()}], 
                                   dataframe_train, dataframe_test, \
                                   emo[0], emo[1], dataframe_path, score, 'emolex', 
                                   vocabulary_inv, sequence_length)

    
    


__author__ = 'Fernando J. V. da Silva'



''' Features in TFIDF stem word frequencies, disconsidering stop words '''
#tfidf_features = pickle.load(open("tweets_tfidf_features.pkl", "rb" ) )

'''
Classes: TRU,DIS,JOY,SAD,ANT,SUR,ANG,FEA, NEUTRAL
'''
benchmark_deep_train('datasets/tweets_auto-tagged-emolex.csv',\
                ['CNN'], \
                'f1', \
                [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')])
