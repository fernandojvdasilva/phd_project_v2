'''
Created on 13/02/2015

@author: fernando
'''
import json

class AnnotatorConfig(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.props = []
    
   
    def load(self, json_path):
        json_file = open(json_path)
        result = True
        
        if json_file:
            json_str = json_file.read()        
            self.props = json.loads(json_str)                
        else:
            result = False
            
        return result
    
    def persist(self, json_path, overwrite=False):
        pass
    
    def __getitem__(self, key):
        return self.props[key]
        