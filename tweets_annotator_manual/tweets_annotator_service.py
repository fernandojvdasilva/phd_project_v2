'''
Created on 29/09/2014

@author: fernando
'''
from tweets_annotator_config import AnnotatorConfig
from tweets.tweet_corpus import TweetCorpus
from tweets_annotator_manual.tweet_annotation_manual import TweetAnnotationManual
from tweets_remove_repeated import TweetsRemoveRepeated
from tweets_upload_annotations import TweetsUploadAnnotations
import os

class TweetsAnnotatorService:
    '''
    classdocs
    '''

    def __init__(self, corpus_dir, lang, config_file):
        '''
        Constructor
        '''
        self.corpus_dir = corpus_dir
        self.lang = lang
        self.env_setup = False
        self.config = None
        self.annotator_id = None
        self.config_file = config_file
        self.current_annotation_index = 0       
        self.curr_annotation = None
    
    def setup_annotation_env(self, annotator_id, reset_previous=False):
        if reset_previous or not self.env_setup:
            self.load_configs(self.config_file)
            self.corpus = TweetCorpus()
            self.corpus.load_corpus(self.corpus_dir)
            self.annotator_id = annotator_id
        self.env_setup = True
    
    def load_configs(self, config_file):
        self.config = AnnotatorConfig()
        self.config.load(config_file)   

    def get_num_remaining_tweets_to_annotate(self):
        return len(self.corpus.tweet_paths) - self.corpus.curr_tweet_index
        
    def find_first_tweet_to_annotate(self, annotator_id):
        if not self.env_setup:
            raise Exception("You haven't setup the environment!")
        
        tweet = self.corpus.first_tweet()
        while tweet != False:
            annotation_filepath = TweetAnnotationManual.filepath_by_tweet(self.corpus_dir, \
                                                                          self.corpus.curr_tweet, \
                                                                                    annotator_id)
            if not os.path.isfile(annotation_filepath):
                break
            
            tweet = self.corpus.next_tweet()
        
        return tweet

    def upload_annotations(self):
        uploader = TweetsUploadAnnotations(self)
        uploader.upload_annotations()


    def begin_annotation(self):
        if not self.env_setup:
            raise Exception("You haven't setup the environment!")

        are_there_tweets = self.find_first_tweet_to_annotate(self.annotator_id)
        self.first_annotation()
        return are_there_tweets

    def remove_repeated_tweets(self):
        repeated_remover = TweetsRemoveRepeated(self)
        repeated_remover.find_repeated()
        repeated_remover.mark_repeated_to_remove()
        repeated_remover.delete_repeated()

        # reload corpus
        self.corpus.load_corpus(self.corpus_dir)

    def end_annotation(self, annotator_id):
        pass
    
    def next_tweet(self):
        if not self.env_setup:
            raise Exception("You haven't setup the environment!")
        
        self.first_annotation()
        return self.corpus.next_tweet()
    
    def prev_tweet(self):
        if not self.env_setup:
            raise Exception("You haven't setup the environment!")
        
        self.first_annotation()
        return self.corpus.prev_tweet()

    def first_annotation(self):
        self.curr_annotation = self.config['annotations'][0]
        self.current_annotation_index = 0
        return self.curr_annotation

    def next_annotation(self):
        self.current_annotation_index += 1
        if self.current_annotation_index == len(self.config['annotations']):
            return False
        self.curr_annotation = self.config['annotations'][self.current_annotation_index]        
        return self.curr_annotation
    
    def prev_annotation(self):
        self.current_annotation_index -= 1
        
        if self.current_annotation_index < 0:            
            return False
        
        self.curr_annotation = self.config['annotations'][self.current_annotation_index]        
                
        return self.curr_annotation
    
    def annotate_tweet(self, tag_name, tag_val):
        if self.corpus.curr_tweet.manual_annotation == None:
            self.corpus.curr_tweet.manual_annotation = TweetAnnotationManual(self.corpus_dir, \
                                                                      self.corpus.curr_tweet, \
                                                                      self.annotator_id)
        
        self.corpus.curr_tweet.manual_annotation.tags[tag_name] = tag_val
        self.corpus.curr_tweet.manual_annotation.persist()        
    
    