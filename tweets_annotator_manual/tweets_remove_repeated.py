from tweets.tweets_filter_repeated import TweetsFilterRepeated

class TweetsRemoveRepeated(TweetsFilterRepeated):

    def __init__(self, tweets_annotator_service):
        self.annotator_service = tweets_annotator_service
        self.corpus = self.annotator_service.corpus
        self.repeated_paths = []
        self.repeated_ids = []

    def mark_repeated_to_remove(self):
        if len(self.repeated_ids) == 0:
            return False

        tweet = self.corpus.first_tweet()
        while tweet != False:
            if tweet['id'] in self.repeated_ids:
                self.annotator_service.annotate_tweet('remove', 'remove')
            tweet = self.corpus.next_tweet()

        return True

