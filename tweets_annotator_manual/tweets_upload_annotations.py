import re
import hashlib
import os

from apiclient.discovery import build

class TweetsUploadAnnotations:

    def __init__(self, tweets_annotator_service):
        self.annotator_service = tweets_annotator_service
        self.corpus = self.annotator_service.corpus
        self.already_uploaded_files = []

    def md5_int(self, string):
        md5_hasher = hashlib.md5()
        md5_hasher.update(string)
        return int(md5_hasher.hexdigest(), 16)

    def mark_uploaded_file(self, curr_uploaded_filename):
        uploaded_files_log = open(os.path.join(self.corpus.corpus_dir, "metadata/uploaded_files.txt"), 'a+')

        md5_hasher = hashlib.md5()
        md5_hasher.update(curr_uploaded_filename)
        uploaded_files_log.write(md5_hasher.hexdigest() + "\n")

        uploaded_files_log.close()

    def load_uploaded_files(self):
        if os.path.exists(os.path.join(self.corpus.corpus_dir, "metadata/uploaded_files.txt")):
            uploaded_files_log = open(os.path.join(self.corpus.corpus_dir, "metadata/uploaded_files.txt"), 'r')

            file_lines = uploaded_files_log.read().split('\n')

            self.already_uploaded_files = [int(hashstr, 16) for hashstr in file_lines if hashstr != '']

    def send_annotation_file(self, annot_filepath, annot_filename):

        api_root = 'https://ibov-twitter.appspot.com/_ah/api'
        api = 'tweetsAnnot'
        version = '1'
        discovery_url = '%s/discovery/v1/apis/%s/%s/rest' % (api_root, api, version)

        annot_file = open(annot_filepath, 'r')
        annotation = annot_file.read()
        annot_file.close()

        body_data = {
                     "filename": annot_filename,
                     "annotator_id": str(self.annotator_service.annotator_id),
                     "annotation": annotation,
                     "tweet_id": annot_filename[9:-10]
                     }


        service = build(api, version, discoveryServiceUrl=discovery_url)


        # Fetch all greetings and print them out.
        response = service.upload().annotation(body=body_data).execute()

        self.mark_uploaded_file(annot_filename)
        
        
        


    def upload_annotations(self):

        self.load_uploaded_files()

        for annot_dir in os.listdir(os.path.join(self.corpus.corpus_dir, "annotation", "manual")):
            for annot_file in os.listdir(os.path.join(self.corpus.corpus_dir, "annotation", "manual", annot_dir)):

                annotation_hash = self.md5_int(annot_file)

                if not annotation_hash in self.already_uploaded_files:
                    #try:
                        self.send_annotation_file(os.path.join(self.corpus.corpus_dir, "annotation", "manual",
                                                               annot_dir, annot_file),
                                                  annot_file)
                    #except:
                    #    continue

