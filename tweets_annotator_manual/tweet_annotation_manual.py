'''
Created on 13/02/2015

@author: fernando
'''
import json
from json.encoder import JSONEncoder
import os
from os import listdir
from os.path import isfile, join

class TweetAnnotationManual(object):
    '''
    classdocs
    '''


    def __init__(self, corpus_dir, tweet, annotator_id):
        '''
        Constructor
        '''
        self.tweet = tweet
        self.annotator_id = annotator_id
        self.tags = {}
        self.corpus_dir = corpus_dir

    @staticmethod
    def get_annotations_by_tweet(corpus_dir, tweet):
        tweet_dir = "%s/annotation/manual/%d-%.2d/" % (corpus_dir, \
                                                       tweet.props['date'].year, \
                                                       tweet.props['date'].month)

        if not os.path.exists(tweet_dir):
            return []

        annotation_files = [f for f in listdir(tweet_dir) if str(tweet.props['id']) in f and isfile(join(tweet_dir, f)) and f[-4:] == 'json']

        manual_annotations = [TweetAnnotationManual(corpus_dir, tweet, f[-8:-5].replace('.','')) for f in annotation_files]

        for annot in manual_annotations:
            annot.load()

        return manual_annotations


    @staticmethod
    def filepath_by_tweet(corpus_dir, tweet, annotator_id):
        filepath = "%s/annotation/manual/%d-%.2d/%d%.2d%.2d-%ul-%.3d.json" % (corpus_dir, \
                                                                         tweet.props['date'].year, \
                                                                         tweet.props['date'].month, \
                                                                         tweet.props['date'].year, \
                                                                         tweet.props['date'].month, \
                                                                         tweet.props['date'].day, \
                                                                         tweet.props['id'],
                                                                         int(annotator_id))
        return filepath
    
    
    def persist(self):      
        json_path = self.filepath_by_tweet(self.corpus_dir, self.tweet, self.annotator_id)
        
        dir = os.path.dirname(json_path)
        if not os.path.exists(dir):
            os.makedirs(dir)
        
        json_str = JSONEncoder().encode(self.tags)
                    
        json_file = open(json_path, 'w+')
        json_file.write(json_str)


    def load(self):
        json_path = self.filepath_by_tweet(self.corpus_dir, self.tweet, self.annotator_id)

        json_file = open(json_path)
        result = True

        if json_file:
            json_str = json_file.read()
            self.tags = json.loads(json_str)
        else:
            result = False

        return result
