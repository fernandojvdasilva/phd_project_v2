import sys
import os
from sklearn.preprocessing.data import Normalizer
#sys.path.append("/media/DADOS/UNICAMP/PHD/src/phd_project")

sys.path.append(os.path.join(os.getcwd(), '..'))

import pickle
import logging
import numpy as np
#from sklearn.decomposition import RandomizedPCA
#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
#from sklearn.grid_search import GridSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.linear_model import Perceptron
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import normalize
from datetime import datetime, date, time
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from nlp_utils.nlp_semantic import *
from nlp_utils.nlp_bag_of_words import *
from mlutils.experiments import *
from mlutils.resampling import *
from mlutils.dim_reduction import *
import scipy
from scipy.stats import randint as sp_randint
import os
import sys
import traceback

#import pudb; pudb.set_trace()

CLASS_INDEX_TRU = 0
CLASS_INDEX_DIS = 1

CLASS_INDEX_JOY = 2
CLASS_INDEX_SAD = 3

CLASS_INDEX_ANT = 4
CLASS_INDEX_SUR = 5

CLASS_INDEX_ANG = 6
CLASS_INDEX_FEAR = 7

CLASS_INDEX_NEUTRAL = 8




def upsample_emos_attrs(dataframe, attrs, emo):
    new_dataframe, indexes = ml_upsample(dataframe, emo)
    expanded_attrs = attrs[indexes]
    new_attrs = np.append(attrs, expanded_attrs)
    return new_dataframe, new_attrs


def extract_emos_attrs_n_class(dataframe, attrs, emo1, emo2):
    try:
        #train_emos_indexes = [i for i in range(dataframe.shape[0]) if dataframe[emo1][i] == 1 or dataframe[emo2][i] == 1]
        train_emos_indexes = []
        emos_class_list = []
        i_ = 0
        for i, row in dataframe.iterrows():
            if row[emo1] == 1:
              train_emos_indexes.append(i_)
              emos_class_list.append(1)
            elif row[emo2] == 1:
              train_emos_indexes.append(i_)
              emos_class_list.append(0)
            i_ += 1
    except:
        print("dataframe.shape")
        print(dataframe.shape)
        print("Exception trace")
        traceback.print_exc()
    #emos_class_list = []
    #for i in train_emos_indexes:
    #    if dataframe[emo1][i] == 1:
    #        emos_class_list.append(1)
    #    else:
    #        emos_class_list.append(0)
    
    emos_class = np.array(emos_class_list)
    emos_attrs = attrs[train_emos_indexes]
    return emos_class, emos_attrs

def experiment_one_vs_one(classifiers, dataframe_train, attrs_train, dataframe_test, attrs_test, \
                          emo1, emo2, corpus, score, label=None):
    
    emos_class_train, emos_attrs_train = extract_emos_attrs_n_class(dataframe_train, attrs_train, emo1, emo2)
    emos_class_test, emos_attrs_test = extract_emos_attrs_n_class(dataframe_test, attrs_test, emo1, emo2)     
    
    #X_train, Y_train, X_test, Y_test = df_tfidf_train_test_split(emos_class, emos_attrs, 0.8)
    X_train = emos_attrs_train
    Y_train = emos_class_train
    X_test = emos_attrs_test
    Y_test = emos_class_test
    
    
    best_clf = None
    best_accuracy = -1.0
    best_clf_name = ""
    for clf_config in classifiers:
        exp_label = 'Evaluating classifier %s: \n' % clf_config['clf-name']
        print(exp_label) 
        try:
            clf, accuracy = ml_hyperpar_selection_random(clf_config['classifier'], clf_config['parameters'], score, \
                                                     X_train, X_test, Y_train, Y_test, [emo2, emo1], corpus, exp_label)        
        
            # Saving current classifier
            if label is None:
                filename = '../models/classifier_%s_vs_%s_%s.pkl' % (emo1, emo2, clf_config['clf-name'])
            else:
                filename = '../models/classifier_%s_vs_%s_%s_%s.pkl' % (emo1, emo2, label, clf_config['clf-name'])
            
            with open(filename, 'wb') as handle:
                pickle.dump(clf.best_estimator_, handle)
            handle.close()
            
            if accuracy > best_accuracy:            
                best_accuracy = accuracy
                best_clf = clf
                best_clf_name = clf_config['clf-name']
        
        
        except Exception as ex:                    
            print(repr(ex) + "\n")
            traceback.print_exc()
        
        
            
    print("Best Classifier: %s\n" % best_clf_name)
    print("Best Accuracy: %f\n" % best_accuracy)
    
    
    # Saving best classifier
    if label is None:
        filename = '../models/classifier_%s_vs_%s.pkl' % (emo1, emo2)
    else:
        filename = '../models/classifier_%s_vs_%s_%s.pkl' % (emo1, emo2, label)
    
    with open(filename, 'wb') as handle:
        pickle.dump(best_clf.best_estimator_, handle)
    handle.close()
    
#    (classifiers, dataframe_train, tfs_train, dataframe_test, tfs_test, emo, dataframe_path)
    
def experiment_one_vs_all(classifiers, dataframe, attrs, dataframe_test, attrs_test, emo, corpus, score, label=None, resample=None):       

    if resample == 'up':
        dataframe, attrs = upsample_emos_attrs(dataframe, attrs, emo)
    elif resample == 'down':
        pass

    emos_class = dataframe[emo]
    emos_class_test = dataframe_test[emo]
    
    #X_train, Y_train, X_test, Y_test = df_tfidf_train_test_split(emos_class, attrs, 0.8)
    X_train = attrs
    Y_train = emos_class
    
    X_test = attrs_test
    Y_test = emos_class_test
    
    best_clf = None
    best_f1_score = -1.0
    best_clf_name = ""
    for clf_config in classifiers:
        exp_label = 'Evaluating classifier %s: \n' % clf_config['clf-name']
        print(exp_label)
        try:
            clf, f1_score = ml_hyperpar_selection_random(clf_config['classifier'], clf_config['parameters'], score, \
                                                     X_train, X_test, Y_train, Y_test, ['no-%s' % emo, emo], corpus, exp_label)
        
        
            # Saving current classifier
            if label is None:
                filename = '../models/classifier_%s_vs_all_%s.pkl' % (emo, clf_config['clf-name'])
            else:
                filename = '../models/classifier_%s_vs_all_%s_%s.pkl' % (emo, label, clf_config['clf-name'])
            
            with open(filename, 'wb') as handle:
                pickle.dump(clf.best_estimator_, handle)
            handle.close()        
        
            if f1_score > best_f1_score:            
                best_f1_score = f1_score
                best_clf = clf
                best_clf_name = clf_config['clf-name']
            
        except Exception as ex:                    
            print(repr(ex) + "\n")
            traceback.print_exc()
            
    print("Best Classifier: %s\n" % best_clf_name)
    print("Best F1 Score: %f\n" % best_f1_score)
    
    # Saving best classifier
    if label is None:
        filename = '../models/classifier_%s_vs_all.pkl' % emo
    else:
        filename = '../models/classifier_%s_vs_all_%s.pkl' % (emo, label)
    
    if best_clf != None:
        with open(filename, 'wb') as handle:
            pickle.dump(best_clf.best_estimator_, handle)
        handle.close()    
    
    

def benchmark_train(dataframe_path, clf_names, score, emotions=None, 
                    one_vs_one=False, one_vs_all=False, pars=None, force_gen_data=False, 
                    calc_prob=False, label=None, resample=None):
    try:
        if not force_gen_data:
            dataframe_train = pd.read_csv(dataframe_path.replace('.csv', '_train.csv'), engine='c')
            dataframe_test = pd.read_csv(dataframe_path.replace('.csv', '_test.csv'), engine='c')

            tfs_train = pickle.load(open(dataframe_path.replace('.csv', '_train.pkl'), 'rb'))
            tfs_test = pickle.load(open(dataframe_path.replace('.csv', '_test.pkl'), 'rb'))

            svd_train = pickle.load(open(dataframe_path.replace('.csv', '_svd_train.pkl'), 'rb'))
            svd_test = pickle.load(open(dataframe_path.replace('.csv', '_svd_test.pkl'), 'rb'))

 
 
#         tfs_train = pickle.load(open(dataframe_path.replace('.csv', '_train.pkl'), 'rb'), encoding='latin1')
#         tfs_test = pickle.load(open(dataframe_path.replace('.csv', '_test.pkl'), 'rb'), encoding='latin1')
#         
#         svd_train = pickle.load(open(dataframe_path.replace('.csv', '_svd_train.pkl'), 'rb'), encoding='latin1')
#         svd_test = pickle.load(open(dataframe_path.replace('.csv', '_svd_test.pkl'), 'rb'), encoding='latin1')

    except Exception as ex:
        print(ex)
        force_gen_data = True


    if force_gen_data:
        dataframe_full = pd.read_csv(dataframe_path, engine='c')

        # DEBUGGING ONLY
        #dataframe_full = dataframe_full.iloc[:100]
        
        try:
            tfs = pickle.load(open(dataframe_path.replace('csv', 'pkl'), "rb" ) )
        except:
            tfidf, tfs = bow_vectorize(dataframe_full, 'text', sem_tokenize_stock_corpus)
    
        tfs_train, dataframe_train, tfs_test, dataframe_test = \
        df_tfidf_train_test_split(dataframe_full, tfs, 0.8)
        
        svd_train, svd_test, num_dim, svd_transformer = svd_reduct(tfs_train, tfs_test, 0.99)
    
        # Saving split data
        dataframe_train.to_csv(dataframe_path.replace('.csv', '_train.csv'))
        dataframe_test.to_csv(dataframe_path.replace('.csv', '_test.csv'))
    
        # Saving vectorized data
        with open(dataframe_path.replace('.csv', '_train.pkl'), 'wb') as handle:
            pickle.dump(tfs_train, handle)
        handle.close()
                
        with open(dataframe_path.replace('.csv', '_test.pkl'), 'wb') as handle:
            pickle.dump(tfs_test, handle)
        handle.close()
        
        # Saving SVD reduced data
        with open(dataframe_path.replace('.csv', '_svd_train.pkl'), 'wb') as handle:
            pickle.dump(svd_train, handle)
        handle.close()
        
        with open(dataframe_path.replace('.csv', '_svd_test.pkl'), 'wb') as handle:
            pickle.dump(svd_test, handle)
        handle.close()
        
        # Saving vectorizer
        with open('../models/tfidf_vectorizer.pkl', 'wb') as handle:
            pickle.dump(tfidf, handle)
        handle.close()
        
        # Saving SVD transformer
        with open('../models/svd_transformer.pkl', 'wb') as handle:
            pickle.dump(svd_transformer, handle)
        handle.close()

        
    classifiers = []    


    if 'SVM-Linear' in clf_names:
        if pars is None:
            pars = {'kernel':['linear'], 'gamma':scipy.stats.expon(scale=.1), \
                                   'C':scipy.stats.expon(scale=100)}
        else:
            pars['kernel'] = ['linear']
        classifiers.append({'clf-name': 'SVM-Linear', \
                    'classifier': SVC(C=1, probability=calc_prob, verbose=99, class_weight='auto', max_iter=10000, cache_size=500), \
                    'parameters': pars\
                    })
    
    if 'SVM-RBF' in clf_names:
        if pars is None:
            pars = {'kernel':['rbf'], 'gamma':scipy.stats.expon(scale=.1), \
                                   'C':scipy.stats.expon(scale=100)}
        else:
            pars['kernel'] = ['rbf']

        classifiers.append({'clf-name': 'SVM-RBF', \
                    'classifier': SVC(C=1, probability=calc_prob, verbose=99, class_weight='auto', max_iter=10000, cache_size=500), \
                    'parameters': pars\
                    })
        
    if 'SVM-POLYNOMIAL' in clf_names:
        if pars is None:
            pars = {'kernel':['poly'], 'gamma':scipy.stats.expon(scale=.1), \
                                   'C':scipy.stats.expon(scale=100)}
        else:
            pars['kernel'] = ['poly']

        classifiers.append({'clf-name': 'SVM-POLYNOMIAL', \
                    'classifier': SVC(C=1, probability=calc_prob, verbose=99, class_weight='auto', max_iter=10000, cache_size=500), \
                    'parameters': pars\
                    })
        
        
        
    '''classifiers = [{'kernel': ['rbf'], 'gamma': [i for i in np.arange(1e-3, 1e4, 10)],
                     'C': [i for i in np.arange(1e-3, 1e4, 10)]}]'''
    
    # We're using reduced dimensions
    svd_and_emolex_train = svd_train
    svd_and_emolex_test = svd_test
    for dim in ["emolex_joy","emolex_sad","emolex_trust",\
                "emolex_disgust","emolex_anger","emolex_fear",\
                "emolex_anticipation","emolex_surprise",\
                "emolex_positive","emolex_negative"]:
        svd_and_emolex_train = np.c_[svd_and_emolex_train, dataframe_train[dim]]
        svd_and_emolex_test = np.c_[svd_and_emolex_test, dataframe_test[dim]]
        
    normalizer = Normalizer().fit(svd_and_emolex_train)
    svd_and_emolex_train = normalizer.transform(svd_and_emolex_train)
    svd_and_emolex_test = normalizer.transform(svd_and_emolex_test)
    
    with open('../models/normalizer.pkl', 'wb') as handle:
            pickle.dump(normalizer, handle)
    handle.close()    
    
    # We're only running one vs one experiment with emolex attributes to speed up the process
#     print('Running one vs all experiments...')
#     for emo in ['JOY', 'SAD', 'TRU', 'DIS', 'ANG', 'FEA', 'ANT', 'SUR']:
#         if not os.path.exists('experiments/classifier_%s_vs_all.pkl' % emo):
#             print('--> Training %s vs all...' % emo)
#             experiment_one_vs_all(classifiers, dataframe_train, svd_train, dataframe_test, svd_test, emo, dataframe_path)
#                         
    
    # DEBUGGING ONLY
    # ldataframe_full = dataframe_train.iloc[:100]
    
    if one_vs_all:
        print('Running one vs all experiments with Emolex attributes...')
        for emo in emotions:
            #if not os.path.exists('models/classifier_%s_vs_all_emolex.pkl' % emo):
            print('--> Training %s vs all...' % emo)
            if not label is None:
                label = 'emolex_%s' % label
            else:
                label = 'emolex'

            experiment_one_vs_all(classifiers, dataframe_train, 
                                       svd_and_emolex_train, dataframe_test, 
                                       svd_and_emolex_test, emo, dataframe_path, 
                                       score, label, resample)
#     
#     print('Running one vs one experiments...')
#     for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT', 'SUR')]:
#         if not os.path.exists('experiments/classifier_%s_vs_%s.pkl' % (emo[0], emo[1])):
#             print('--> Training %s vs %s...' % (emo[0], emo[1]))
#             experiment_one_vs_one(classifiers, dataframe_train, svd_train, dataframe_test, svd_test, emo[0], emo[1], dataframe_path)
            
    if one_vs_one:
        print('Running one vs one experiments with Emolex attributes...')
        if emotions is None:
            emotions = [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT', 'SUR')] 
        for emo in emotions:
            #if not os.path.exists('experiments/classifier_%s_vs_%s_emolex.pkl' % (emo[0], emo[1])):
            print('--> Training %s vs %s with Emolex...' % (emo[0], emo[1]))
            if not label is None:
                label = 'emolex_%s' % label
            else:
                label = 'emolex'
            experiment_one_vs_one(classifiers, dataframe_train, svd_and_emolex_train, dataframe_test, \
                                      svd_and_emolex_test, emo[0], emo[1], dataframe_path, score, label)
    
    
    

__author__ = 'Fernando J. V. da Silva'



''' Features in TFIDF stem word frequencies, disconsidering stop words '''
#tfidf_features = pickle.load(open("tweets_tfidf_features.pkl", "rb" ) )

'''
Classes: TRU,DIS,JOY,SAD,ANT,SUR,ANG,FEA, NEUTRAL
'''
#tfidf_classes = pickle.load(open("tweets_tfidf_classes.pkl", "rb" ) )


# =========== Experiment 1: Use the whole dataset (trust vs non-trust, disgust vs non-disgust, etc...)==================

# Observing the PCA
#experiment_preliminar(tfidf_features, tfidf_classes)



# Experiment 2: Use (Suttles and Ide) approach and split the dataset according to the classes
#benchmark_train('/media/fernando/DADOS/UNICAMP/PHD/models/2017-06-18/tweets_auto-tagged-emolex.csv',\

# ONLY ANG vs FEA
# benchmark_train('experiments/tweets_auto-tagged-emolex.csv',\
#                 ['SVM-Linear', 'SVM-POLYNOMIAL', 'SVM-RBF'], \
#                 'f1', \
#                 [('ANG', 'FEA')])


benchmark_train('../datasets/tweets_auto-tagged-emolex.csv',\
                ['SVM-Linear', 'SVM-POLYNOMIAL', 'SVM-RBF'], \
                'f1_macro', \
                [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')], \
                one_vs_one=True,
                force_gen_data=True, 
                calc_prob=True)


benchmark_train('../datasets/tweets_auto-tagged-emolex.csv',\
                ['SVM-RBF'], \
                'f1_macro', \
                ['JOY', 'SAD', 'TRU', 'DIS', 'ANT', 'SUR', 'ANG', 'FEA'], \
                one_vs_all=True,
                calc_prob=True)

'''
benchmark_train('../datasets/tweets_auto-tagged-emolex.csv',\
                ['SVM-RBF'], \
                'f1_macro', \
                ['JOY', 'SAD', 'TRU', 'DIS', 'ANT', 'SUR', 'ANG', 'FEA'], \
                one_vs_all=True,
                calc_prob=True,
                resample='up')

benchmark_train('../datasets/tweets_auto-tagged-emolex.csv',\
                ['SVM-RBF'], \
                'f1_macro', \
                ['JOY', 'SAD', 'TRU', 'DIS', 'ANT', 'SUR', 'ANG', 'FEA'], \
                one_vs_all=True,
                calc_prob=True,
                resample='down')
'''
