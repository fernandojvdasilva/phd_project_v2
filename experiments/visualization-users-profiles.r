library(xtable)

visualize_users_profiles <- function(series, labels, output_chart, title, output_table ) {

  td_new <- data.frame(cbind("count" = table(series)))
  td_new <- cbind("labels" = labels, td_new)
  
  
  piepercent<- round(100*td_new$count/sum(td_new$count), 1)
  
  png(file = output_chart)
  
  # Plot the chart.
  pie(td_new$count, labels = piepercent, main = title,col = rainbow(length(td_new$count)))
  legend("topright", labels, cex = 0.8,
         fill = rainbow(length(td_new$count)))
  
  # Save the file.
  dev.off()

  print.xtable(xtable(td_new, caption=title, label=basename(output_table)), file=output_table)
  
}



base_dir <- '/media/fernando/DADOS/UNICAMP/PHD/src/phd_project/experiments/visualization'

csv_dir <- '/media/fernando/DADOS/UNICAMP/PHD/src/phd_project/datasets'

# Visualizing emotions in the whole corpora

df <- read.csv(file.path(csv_dir, "tweets_db_UserAnnotator.csv"), sep=",")

df <- subset(df,  df$num_tickets > 0 | 
                  df$next_ticket_count < 20)

visualize_users_profiles(df$age, 
                     c("18 to 25", "26 to 30", "31 to 40", "41 to 50", "51 to 60"),
                     file.path(base_dir, "user_profiles_age.png"), 
                     "Ages (percentage)",
                     file.path(base_dir, "user_profiles_age.tex"))

visualize_users_profiles(df$gender, 
                         c("female", "male"),
                         file.path(base_dir, "user_profiles_gender.png"), 
                         "Gender (percentage)",
                         file.path(base_dir, "user_profiles_gender.tex"))

visualize_users_profiles(df$student_degree, 
                         c("high school", "undergrad. student", "graduate", "master degree", "doctor degree"),
                         file.path(base_dir, "user_profiles_degree.png"), 
                         "Academic Degrees (percentage)",
                         file.path(base_dir, "user_profiles_degree.tex"))

# Using humanities == social sciences
df$student_area <- as.character(df$student_area)
df$student_area[df$student_area == 's'] <- 'h'

visualize_users_profiles(df$student_area, 
                         c("biological sciences", "math or engineering", "humanities", "none"),
                         file.path(base_dir, "user_profiles_area.png"), 
                         "Annotators Subject Areas of Study (percentage)",
                         file.path(base_dir, "user_profiles_area.tex"))

visualize_users_profiles(df$stock_technical, 
                         c("Doesn't know technical analysis", "Knows technical analysis"),
                         file.path(base_dir, "user_profiles_tech.png"), 
                         "Knowledge of Technical Analysis (percentage)",
                         file.path(base_dir, "user_profiles_tech.tex"))

visualize_users_profiles(df$stock_exp, 
                         c("Doesn't have experience in the stock market", "Has experience in the stock market"),
                         file.path(base_dir, "user_profiles_stock.png"), 
                         "Experience in the Stock Market (percentage)",
                         file.path(base_dir, "user_profiles_stock.tex"))
