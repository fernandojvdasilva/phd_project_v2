'''
Created on 5 de set de 2017

@author: fernando
'''

import pandas as pd
import numpy as np
from nlp_utils.nlp_semantic import *
from nlp_utils.nlp_bag_of_words import *
from mlutils.experiments import *
from mlutils.dim_reduction import *
import pickle
import os
import pprint
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import f1_score
from multiprocessing import Pool
import math

# The dataframe_path must be the full path to the target domain dataframe (stock market or
# any other containing neutral tweets).

from experiments_utils import *



# def extract_kfolds(clf, emo_pair, emo_pair_key, neutral_Y, neutral_X, emo_Y, emo_X, pred):
#     emo_kfolds = kfoldIndexes(emo_X, emo_Y, 5)
#     neutral_kfolds = kfoldIndexes(neutral_X, neutral_Y, 5)
#     clf[emo_pair_key][emo_pair[0]]['emo_folds_ind'] = []
#     clf[emo_pair_key][emo_pair[1]]['emo_folds_ind'] = []
#     curr_indexes = [0, 0]
#     for fold in emo_kfolds:
#         emo_fold_indexes = [], []
#         for i in fold['test']:
#             if pred[i] == 0:
#                 emo_fold_indexes[0].append(curr_indexes[0])
#                 curr_indexes[0] += 1
#             else:
#                 emo_fold_indexes[1].append(curr_indexes[1])
#                 curr_indexes[1] += 1
#         
#         clf[emo_pair_key][emo_pair[0]]['emo_folds_ind'].append(emo_fold_indexes[0])
#         clf[emo_pair_key][emo_pair[1]]['emo_folds_ind'].append(emo_fold_indexes[1])
#     
#     clf[emo_pair_key]['neutral_folds_ind'] = [fold['test'] for fold in neutral_kfolds]

def extract_kfolds(clf, emo_pair, emo_pair_key, neutral_Y, neutral_X, emo0_X, emo1_X):
    emo0_kfolds = kfoldIndexes(emo0_X, [0] * len(emo0_X), 5)
    emo1_kfolds = kfoldIndexes(emo1_X, [1] * len(emo1_X), 5)
    
    neutral_kfolds = kfoldIndexes(neutral_X, neutral_Y, 5)
    
    clf[emo_pair_key][emo_pair[0]]['emo_folds_ind'] = [fold['test'] for fold in emo0_kfolds]
    clf[emo_pair_key][emo_pair[1]]['emo_folds_ind'] = [fold['test'] for fold in emo1_kfolds]    
    clf[emo_pair_key]['neutral_folds_ind'] = [fold['test'] for fold in neutral_kfolds]


def predict_probabilities(models_dir, emotions, emo_dataframe_train, emo_svd_and_emolex_train, clf):
    pool = Pool()
    need_save = False
    for emo_pair in emotions:
        try:
            emo_pair_key = '%s-vs-%s' % (emo_pair[1], emo_pair[0])
            #if emo_pair[0] in clf[emo_pair_key].keys():
            #    print("%s already predicted" % emo_pair_key)
            #    continue
            need_save = True
            print("Predicting %s" % emo_pair_key)
                        
            
            neutral_Y, neutral_X, neutral_indexes = extract_neutral_attrs_n_class(emo_dataframe_train,
                                                                                  emo_svd_and_emolex_train, 
                                                                                  emo_pair[1], emo_pair[0])
            
            emo_Y, emo_X, emo_indexes = extract_emos_attrs_n_class(emo_dataframe_train, 
                emo_svd_and_emolex_train, 
                emo_pair[1], emo_pair[0])

            
            neutral_Y, neutral_X = df_tfidf_cut_random(neutral_Y, neutral_X, math.ceil(len(emo_X)/2))
            
            
            pred_prob = clf[emo_pair_key]['model'].predict_proba(neutral_X)
            pred = clf[emo_pair_key]['model'].predict(neutral_X)
            clf[emo_pair_key][emo_pair[0]] = {}
            clf[emo_pair_key][emo_pair[1]] = {}
            clf[emo_pair_key][emo_pair[0]]['neutral-prob'] = [p[0] for p in pred_prob]
            clf[emo_pair_key][emo_pair[1]]['neutral-prob'] = [p[1] for p in pred_prob]
            clf[emo_pair_key]['neutral-pred'] = pred
                        
            
                        
            pool1 = pool.apply_async(clf[emo_pair_key]['model'].predict, [emo_X]) 
            pool2 = pool.apply_async(clf[emo_pair_key]['model'].predict_proba, [emo_X])
            pred = pool1.get()
            pred_prob = pool2.get()
            clf[emo_pair_key][emo_pair[0]]['%s-prob' % emo_pair[0]] = [prob for prob, p in 
                zip(pred_prob, pred) if p == 0]
            clf[emo_pair_key][emo_pair[1]]['%s-prob' % emo_pair[1]] = [prob for prob, p in 
                zip(pred_prob, pred) if p == 1]
            
            clf[emo_pair_key][emo_pair[0]]['%s-pred' % emo_pair[0]] = [p for p in pred if p == 0]
            clf[emo_pair_key][emo_pair[1]]['%s-pred' % emo_pair[1]] = [p for p in pred if p == 1]
            
            # Extracting K-Fold (k=5) indexes:
            extract_kfolds(clf, emo_pair, emo_pair_key, neutral_Y, neutral_X, 
                           [p for p in pred if p == 0], 
                           [p for p in pred if p == 1]) 
            
            
        except Exception as ex:
            print("exception\n")
            print("emo0 = %s, emo1 = %s" % (emo_pair[0], emo_pair[1]))
            print(ex)
    
    if need_save:
        # Must remove deep learning models before dumping because pickle can't serialize keras' models.
        if 'model-dl' in clf['JOY-vs-SAD'].keys(): 
            clf['JOY-vs-SAD']['model-dl'] = None    
            clf['TRU-vs-DIS']['model-dl'] = None    
            clf['ANG-vs-FEA']['model-dl'] = None    
            clf['ANT-vs-SUR']['model-dl'] = None
        
        with open(os.path.join(models_dir, 'probs_train_neutral.pkl'), 'wb') as handle:
            pickle.dump(clf, handle)
        handle.close()
        
        
    return clf


def calculate_roc(emotions, clf):
    for emo_pair in emotions:
        emo_pair_key = '%s-vs-%s' % (emo_pair[1], emo_pair[0])
        fpr0, tpr0, thresholds0 = roc_curve([0] * len(clf[emo_pair_key][emo_pair[0]]['neutral-prob']) + \
                                            [1] * len(clf[emo_pair_key][emo_pair[0]]['%s-prob' % emo_pair[0]]), \
                                            clf[emo_pair_key][emo_pair[0]]['neutral-prob'] + \
                                            [p[0] for p in clf[emo_pair_key][emo_pair[0]]['%s-prob' % emo_pair[0]]])
        print("Emotion %s" % emo_pair[0])
        clf[emo_pair_key][emo_pair[0]]['roc-fpr'] = fpr0
        clf[emo_pair_key][emo_pair[0]]['roc-tpr'] = tpr0
        clf[emo_pair_key][emo_pair[0]]['roc-thresholds'] = thresholds0
        fpr1, tpr1, thresholds1 = roc_curve([0] * len(clf[emo_pair_key][emo_pair[1]]['neutral-prob']) + \
                                            [1] * len(clf[emo_pair_key][emo_pair[1]]['%s-prob' % emo_pair[1]]), \
                                            clf[emo_pair_key][emo_pair[1]]['neutral-prob'] + \
                                            [p[1] for p in clf[emo_pair_key][emo_pair[1]]['%s-prob' % emo_pair[1]]])
        print("Emotion %s" % emo_pair[1])
        clf[emo_pair_key][emo_pair[1]]['roc-fpr'] = fpr1
        clf[emo_pair_key][emo_pair[1]]['roc-tpr'] = tpr1
        clf[emo_pair_key][emo_pair[1]]['roc-thresholds'] = thresholds1
        
    return clf


def benchmark_train_neutral(dataframe_dir, models_dir, emotions=None):
    
    normalizer = pickle.load(open(os.path.join(models_dir, 'normalizer.pkl'), "rb" ), encoding='latin1')
                
    emo_svd_train, emo_svd_test, emo_dataframe_train, emo_dataframe_test = load_emo_dataframe(dataframe_dir)

    
    emo_svd_and_emolex_train, emo_svd_and_emolex_test = add_emolex_features(emo_svd_train, 
                                                                    emo_svd_test, emo_dataframe_train, 
                                                                    emo_dataframe_test, normalizer)
    
    
        
    clf = load_classifiers(models_dir)
    
    if emotions is None:
        emotions = [('SAD', 'JOY'), ('DIS', 'TRU'), ('FEA', 'ANG'), ('SUR', 'ANT')]
        
    clf = predict_probabilities(models_dir, emotions, emo_dataframe_train, 
                                emo_svd_and_emolex_train, clf)
    
    
    clf = calculate_roc(emotions, clf)
                
    for emo_pair in emotions:
        emo_pair_key = '%s-vs-%s' % (emo_pair[1], emo_pair[0])
        
        for i in range(1, 100):            
            threshold0 = i / 100.0
            
            for j in range(1, 100):
                threshold1 = j / 100.0
            
                avg_f1 = 0
                k_count = 0
                for emo_ind0, emo_ind1, neutral_ind in zip(clf[emo_pair_key][emo_pair[0]]["emo_folds_ind"], \
                                                           clf[emo_pair_key][emo_pair[1]]["emo_folds_ind"], \
                                                           clf[emo_pair_key]["neutral_folds_ind"]):
                    
                    f1 = f1_score([2] * len(neutral_ind) + 
                                  [0] * len(emo_ind0) + 
                                  [1] * len(emo_ind1), 
                                  # Neutral tweets
                                  [2 if clf[emo_pair_key][emo_pair[0]]['neutral-prob'][l] < threshold0 and \
                                   clf[emo_pair_key][emo_pair[1]]['neutral-prob'][l] < threshold1 else
                                   clf[emo_pair_key]['neutral-pred'][l] for l in neutral_ind] +
                                  
                                  # Tweets with emotion 0
                                  [2 if clf[emo_pair_key][emo_pair[0]]['%s-prob' % emo_pair[0]][l][0] < threshold0 and \
                                   clf[emo_pair_key][emo_pair[0]]['%s-prob' % emo_pair[0]][l][1] < threshold1 else
                                   clf[emo_pair_key][emo_pair[0]]['%s-pred' % emo_pair[0]][l] for l in emo_ind0] +
                                  
                                  # Tweets with emotion 1
                                  [2 if clf[emo_pair_key][emo_pair[1]]['%s-prob' % emo_pair[1]][l][0] < threshold0 and \
                                   clf[emo_pair_key][emo_pair[1]]['%s-prob' % emo_pair[1]][l][1] < threshold1 else
                                   clf[emo_pair_key][emo_pair[1]]['%s-pred' % emo_pair[1]][l] for l in emo_ind1]
                                   
                                  , 
                                  average='macro')                        
            
                    k_count += 1
                    avg_f1 += f1
                
                avg_f1 /= k_count
                                                
                if not 'max-f1' in clf[emo_pair_key] or \
                    (avg_f1 > clf[emo_pair_key]['max-f1']):
                    clf[emo_pair_key]['max-f1'] = avg_f1
                    clf[emo_pair_key][emo_pair[0]]['threshold'] = threshold0
                    clf[emo_pair_key][emo_pair[1]]['threshold'] = threshold1
                
        
    for emo_pair in emotions:
        emo_pair_key = '%s-vs-%s' % (emo_pair[1], emo_pair[0])
        print("Emotion pair %s" % emo_pair_key)
        for j in range(0, 2):            
            print("Max F1 Score for %s = %.3f" % (emo_pair[j], clf[emo_pair_key]['max-f1']))
            print("Chosen threshold for %s = %.3f\n" % (emo_pair[j], clf[emo_pair_key][emo_pair[j]]['threshold']))           
            
#             Y = [0] * len(clf[emo_pair_key][emo_pair[j]]['neutral-prob']) + \
#                 [1] * len(clf[emo_pair_key][emo_pair[j]]['%s-prob' % emo_pair[j]])
#             
#             Y_pred = [prob >= threshold for prob in clf[emo_pair_key][emo_pair[j]]['neutral-prob']] + \
#             [prob >= threshold for prob in clf[emo_pair_key][emo_pair[j]]['%s-prob' % emo_pair[j]]]            
#                         
#             print(classification_report(Y, Y_pred, target_names=[key_1, key_0, 'neutral']))
#         
#             print("\t\tConfusion Matrix: ")
#             conf_matrix = confusion_matrix(Y, Y_pred)
    
    # Must remove deep learning models before dumping because pickle can't serialize keras' models.
    if 'model-dl' in clf['JOY-vs-SAD'].keys(): 
        clf['JOY-vs-SAD']['model-dl'] = None    
        clf['TRU-vs-DIS']['model-dl'] = None    
        clf['ANG-vs-FEA']['model-dl'] = None    
        clf['ANT-vs-SUR']['model-dl'] = None
    
    with open(os.path.join(models_dir, 'probs_train_neutral.pkl'), 'wb') as handle:
        pickle.dump(clf, handle)
    handle.close()
    
    
    
#benchmark_train_neutral('datasets/tweets_stocks_emolex.csv',\
benchmark_train_neutral('datasets',\
                        'models')    
    