'''
Created on 5 de set de 2017

@author: fernando
'''

import pandas as pd
import numpy as np
from nlp_utils.nlp_semantic import *
from nlp_utils.nlp_bag_of_words import *
from nlp_utils.w2v import np_to_indexes
from mlutils.experiments import *
from mlutils.dim_reduction import *
import pickle
import os
import pprint
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import f1_score
from multiprocessing import Pool

# The dataframe_path must be the full path to the target domain dataframe (stock market or
# any other containing neutral tweets).

from experiments_utils import *

def extract_kfolds(clf, emo_pair, emo_pair_key, neutral_Y, neutral_X, emo0_X, emo1_X):
    emo0_kfolds = kfoldIndexes(emo0_X, [0] * len(emo0_X), 5)
    emo1_kfolds = kfoldIndexes(emo1_X, [1] * len(emo1_X), 5)
    
    neutral_kfolds = kfoldIndexes(neutral_X, neutral_Y, 5)
    
    clf[emo_pair_key][emo_pair[0]]['emo_folds_ind'] = [fold['test'] for fold in emo0_kfolds]
    clf[emo_pair_key][emo_pair[1]]['emo_folds_ind'] = [fold['test'] for fold in emo1_kfolds]    
    clf[emo_pair_key]['neutral_folds_ind'] = [fold['test'] for fold in neutral_kfolds]



def predict_probabilities_dl(models_dir, emotions, emo_dataframe_train, emo_x_train, clf):    
    for emo_pair in emotions:
        try:
            emo_pair_key = '%s-vs-%s' % emo_pair
            #if emo_pair[0] in clf[emo_pair_key].keys() and \
            #'neutral-prob-dl' in clf[emo_pair_key][emo_pair[0]].keys():
            #    print("%s already predicted" % emo_pair_key)
            #    continue                        
            
            print("Predicting %s" % emo_pair_key)
            
            neutral_Y, neutral_X, neutral_indexes = extract_neutral_attrs_n_class(emo_dataframe_train,
                                                                                  emo_x_train, 
                                                                                  emo_pair[0], emo_pair[1])
            
            emo_Y, emo_X, emo_indexes = extract_emos_attrs_n_class(emo_dataframe_train, 
                emo_x_train, 
                emo_pair[0], emo_pair[1])
            
            
            neutral_Y, neutral_X = df_tfidf_cut_random(neutral_Y, neutral_X, len(emo_X)/2)
            
            pred_prob = clf[emo_pair_key]['model-dl'].predict(neutral_X)
            clf[emo_pair_key][emo_pair[0]] = {}
            clf[emo_pair_key][emo_pair[1]] = {}
            clf[emo_pair_key][emo_pair[0]]['neutral-prob-dl'] = [1.0-p[0] for p in pred_prob]
            clf[emo_pair_key][emo_pair[1]]['neutral-prob-dl'] = [p[0] for p in pred_prob]
            clf[emo_pair_key]['neutral-pred-dl'] = [0 if p < 0.5 else 1 for p in pred_prob]
            
                                    
             
            #pool2 = pool.apply_async(clf[emo_pair_key]['model_dl'].predict_proba, [emo_X])
            pred_prob = clf[emo_pair_key]['model-dl'].predict(emo_X)
            pred_ = [int(prob > 0.5) for prob in pred_prob]
            pred_prob = [(1.0-prob[0], prob[0]) for prob in pred_prob]
            
            clf[emo_pair_key][emo_pair[0]]['%s-prob-dl' % emo_pair[0]] = [prob for prob, pred in 
                zip(pred_prob, pred_) if pred == 0]
            clf[emo_pair_key][emo_pair[1]]['%s-prob-dl' % emo_pair[1]] = [prob for prob, pred in 
                zip(pred_prob, pred_) if pred == 1]
            
            clf[emo_pair_key][emo_pair[0]]['%s-pred-dl' % emo_pair[0]] = [pred for pred in pred_ if pred == 0]
            clf[emo_pair_key][emo_pair[1]]['%s-pred-dl' % emo_pair[1]] = [pred for pred in pred_ if pred == 1]
            
            # Extracting K-Fold (k=5) indexes:
            extract_kfolds(clf, emo_pair, emo_pair_key, [0] * len(neutral_X), neutral_X, 
                           [p for p in pred_ if p == 0], 
                           [p for p in pred_ if p == 1])
            
        except Exception as ex:
            print("exception\n")
            print("emo0 = %s, emo1 = %s" % (emo_pair[0], emo_pair[1]))
            print(ex)
        
        
    return clf


# def calculate_roc(emotions, clf):
#     for emo_pair in emotions:
#         emo_pair_key = '%s-vs-%s' % emo_pair
#         fpr0, tpr0, thresholds0 = roc_curve([0] * len(clf[emo_pair_key][emo_pair[0]]['neutral-prob-dl']) + \
#                                             [1] * len(clf[emo_pair_key][emo_pair[0]]['%s-prob-dl' % emo_pair[0]]), \
#                                             clf[emo_pair_key][emo_pair[0]]['neutral-prob-dl'] + \
#                                             clf[emo_pair_key][emo_pair[0]]['%s-prob-dl' % emo_pair[0]])
#         print("Emotion %s" % emo_pair[0])
#         clf[emo_pair_key][emo_pair[0]]['roc-fpr-dl'] = fpr0
#         clf[emo_pair_key][emo_pair[0]]['roc-tpr-dl'] = tpr0
#         clf[emo_pair_key][emo_pair[0]]['roc-thresholds-dl'] = thresholds0
#         fpr1, tpr1, thresholds1 = roc_curve(
#             [0] * len(clf[emo_pair_key][emo_pair[1]]['neutral-prob-dl']) + [1] * len(clf[emo_pair_key][emo_pair[1]]['%s-prob-dl' % emo_pair[1]]), 
#             clf[emo_pair_key][emo_pair[1]]['neutral-prob-dl'] + clf[emo_pair_key][emo_pair[1]]['%s-prob-dl' % emo_pair[1]])
#         print("Emotion %s" % emo_pair[1])
#         clf[emo_pair_key][emo_pair[1]]['roc-fpr-dl'] = fpr1
#         clf[emo_pair_key][emo_pair[1]]['roc-tpr-dl'] = tpr1
#         clf[emo_pair_key][emo_pair[1]]['roc-thresholds-dl'] = thresholds1
#         
#     return clf

def calculate_roc(emotions, clf):
    for emo_pair in emotions:
        emo_pair_key = '%s-vs-%s' % emo_pair
        fpr0, tpr0, thresholds0 = roc_curve([0] * len(clf[emo_pair_key][emo_pair[0]]['neutral-prob-dl']) + \
                                            [1] * len(clf[emo_pair_key][emo_pair[0]]['%s-prob-dl' % emo_pair[0]]), \
                                            clf[emo_pair_key][emo_pair[0]]['neutral-prob-dl'] + \
                                            [p[0] for p in clf[emo_pair_key][emo_pair[0]]['%s-prob-dl' % emo_pair[0]]])
        print("Emotion %s" % emo_pair[0])
        clf[emo_pair_key][emo_pair[0]]['roc-fpr-dl'] = fpr0
        clf[emo_pair_key][emo_pair[0]]['roc-tpr-dl'] = tpr0
        clf[emo_pair_key][emo_pair[0]]['roc-thresholds-dl'] = thresholds0
        fpr1, tpr1, thresholds1 = roc_curve([0] * len(clf[emo_pair_key][emo_pair[1]]['neutral-prob-dl']) + \
                                            [1] * len(clf[emo_pair_key][emo_pair[1]]['%s-prob-dl' % emo_pair[1]]), \
                                            clf[emo_pair_key][emo_pair[1]]['neutral-prob-dl'] + \
                                            [p[1] for p in clf[emo_pair_key][emo_pair[1]]['%s-prob-dl' % emo_pair[1]]])
        print("Emotion %s" % emo_pair[1])
        clf[emo_pair_key][emo_pair[1]]['roc-fpr-dl'] = fpr1
        clf[emo_pair_key][emo_pair[1]]['roc-tpr-dl'] = tpr1
        clf[emo_pair_key][emo_pair[1]]['roc-thresholds-dl'] = thresholds1
        
    return clf



def benchmark_train_neutral_dl(dataframe_path, dataframe_dir, models_dir, emotions=None, debug=False):
        
    
    #svd_train, svd_test, dataframe_train, dataframe_test = load_dataframe(dataframe_path, models_dir)
                       
    emo_svd_train, emo_svd_test, emo_dataframe_train, emo_dataframe_test = load_emo_dataframe(dataframe_dir)

    # DEBUG ONLY:
    if debug:        
        emo_dataframe_train = df_cut_random(emo_dataframe_train, 300)
        emo_dataframe_test = df_cut_random(emo_dataframe_test, 300)                


    vocabulary_inv = pickle.load(open(os.path.join(models_dir, 'vocabulary_inv.pkl'), 'rb'))

    max_sentence_len = 400
    
    #x_train = [[c for c in lex_tokenize(x)] for x in dataframe_train['text']]
    emo_x_train = [[c for c in lex_tokenize(x)] for x in emo_dataframe_train['text']]
    
    #x_train = np_to_indexes(x_train, vocabulary_inv, max_sentence_len)
    emo_x_train = np_to_indexes(emo_x_train, vocabulary_inv, max_sentence_len)
    
    #x_test = [[c for c in lex_tokenize(x)] for x in dataframe_test['text']]
    emo_x_test = [[c for c in lex_tokenize(x)] for x in emo_dataframe_test['text']]
    
    #x_test = np_to_indexes(x_test, vocabulary_inv, max_sentence_len)
    emo_x_test = np_to_indexes(emo_x_test, vocabulary_inv, max_sentence_len)
    
        
    clf = load_classifiers(models_dir, True)
    
    if emotions is None:
        emotions = [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT', 'SUR')]
        
    clf = predict_probabilities_dl(models_dir, emotions,  
                          emo_dataframe_train, emo_x_train, clf)
    
    
    clf = calculate_roc(emotions, clf)
                
#     for emo_pair in emotions:
#         emo_pair_key = '%s-vs-%s' % emo_pair
#         
#         for i in range(1, 10000):            
#             threshold = i / 100.0
#             
#             for j in range(0, 2):
#                 f1 = f1_score([0] * len(clf[emo_pair_key][emo_pair[j]]['neutral-prob-dl']) + 
#                               [1] * len(clf[emo_pair_key][emo_pair[j]]['%s-prob-dl' % emo_pair[j]]), 
#                               [p >= threshold for p in clf[emo_pair_key][emo_pair[j]]['neutral-prob-dl']] + 
#                               [p >= threshold for p in clf[emo_pair_key][emo_pair[j]]['%s-prob-dl' % emo_pair[j]]], 
#                               average='macro')
#                 
#                 if not 'max-f1-dl' in clf[emo_pair_key][emo_pair[j]] or \
#                  f1 > clf[emo_pair_key][emo_pair[j]]['max-f1-dl']:
#                     clf[emo_pair_key][emo_pair[j]]['max-f1-dl'] = f1
#                     clf[emo_pair_key][emo_pair[j]]['threshold-dl'] = threshold

    for emo_pair in emotions:
        emo_pair_key = '%s-vs-%s' % emo_pair
        
        for i in range(1, 100):            
            threshold0 = i / 100.0
            
            for j in range(1, 100):
                threshold1 = j / 100.0
            
                avg_f1 = 0
                k_count = 0
                for emo_ind0, emo_ind1, neutral_ind in zip(clf[emo_pair_key][emo_pair[0]]["emo_folds_ind"], \
                                                           clf[emo_pair_key][emo_pair[1]]["emo_folds_ind"], \
                                                           clf[emo_pair_key]["neutral_folds_ind"]):
                    
                    f1 = f1_score([2] * len(neutral_ind) + 
                                  [0] * len(emo_ind0) + 
                                  [1] * len(emo_ind1), 
                                  # Neutral tweets
                                  [2 if clf[emo_pair_key][emo_pair[0]]['neutral-prob-dl'][l] < threshold0 and \
                                   clf[emo_pair_key][emo_pair[1]]['neutral-prob-dl'][l] < threshold1 else
                                   clf[emo_pair_key]['neutral-pred-dl'][l] for l in neutral_ind] +
                                  
                                  # Tweets with emotion 0
                                  [2 if clf[emo_pair_key][emo_pair[0]]['%s-prob-dl' % emo_pair[0]][l][0] < threshold0 and \
                                   clf[emo_pair_key][emo_pair[0]]['%s-prob-dl' % emo_pair[0]][l][1] < threshold1 else
                                   clf[emo_pair_key][emo_pair[0]]['%s-pred-dl' % emo_pair[0]][l] for l in emo_ind0] +
                                  
                                  # Tweets with emotion 1
                                  [2 if clf[emo_pair_key][emo_pair[1]]['%s-prob-dl' % emo_pair[1]][l][0] < threshold0 and \
                                   clf[emo_pair_key][emo_pair[1]]['%s-prob-dl' % emo_pair[1]][l][1] < threshold1 else
                                   clf[emo_pair_key][emo_pair[1]]['%s-pred-dl' % emo_pair[1]][l] for l in emo_ind1]
                                   
                                  , 
                                  average='macro')                        
            
                    k_count += 1
                    avg_f1 += f1
                
                avg_f1 /= k_count
                                                
                if not 'max-f1-dl' in clf[emo_pair_key] or \
                    (avg_f1 > clf[emo_pair_key]['max-f1-dl']):
                    clf[emo_pair_key]['max-f1-dl'] = avg_f1
                    clf[emo_pair_key][emo_pair[0]]['threshold-dl'] = threshold0
                    clf[emo_pair_key][emo_pair[1]]['threshold-dl'] = threshold1
    
    
                
        
    for emo_pair in emotions:
        emo_pair_key = '%s-vs-%s' % emo_pair
        print("Emotion pair %s" % emo_pair_key)
        for j in range(0, 2):            
            print("Max F1 Score for %s = %.3f" % (emo_pair[j], clf[emo_pair_key]['max-f1-dl']))
            print("Chosen threshold for %s = %.3f\n" % (emo_pair[j], clf[emo_pair_key][emo_pair[j]]['threshold-dl']))
    
    # Must remove deep learning models before dumping because pickle can't serialize keras' models.
    clf['JOY-vs-SAD']['model-dl'] = None    
    clf['TRU-vs-DIS']['model-dl'] = None    
    clf['ANG-vs-FEA']['model-dl'] = None    
    clf['ANT-vs-SUR']['model-dl'] = None
    
    
    with open(os.path.join(models_dir, 'probs_train_deep_neutral.pkl'), 'wb') as handle:
        pickle.dump(clf, handle)
    handle.close()
    
    
    
benchmark_train_neutral_dl('datasets/tweets_stocks_emolex.csv',\
                           'datasets',
                           'models')    
    