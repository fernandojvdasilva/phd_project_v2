emo_joy_hashtags <- c('feliz', 'contente', 'alegre', 'alegria', 'felicidade')

emo_sad_hashtags <- c('triste', 'chateado', 'chateada', 'magoado', 'magoada', 'desanimo',
                      'desanimado', 'desanimada', 'melancolico', 'melancólico', 'melancolia',
                      'deprimido', 'deprimida', 'pesar', 'pesames', 'pêsames', 'luto')

emo_fear_hashtags <- c('medo', 'commedo', 'empanico', 'aterrorizado', 'aterrorizada',
                       'apavorado', 'apavorada', 'susto', 'assustado', 'assustada',
                       'receio', 'receoso', 'receosa')

emo_ang_hashtags <- c('raiva', 'comraiva', 'irritado', 'irritada', 'irritação',
                      'aborrecido', 'aborrecida')

emo_tru_hashtags <- c('confio', 'confiavel', 'confiável',
                      'admiro', 'admirável', 'admiravel', 'admiração', 'admiracao',
                      'idolo', 'meuidolo')

emo_dis_hashtags <- c('desgosto', 'aversao', 'aversão', 'antipatia', 'antipático',
                      'antipatico', 'antipatica', 'antipática', 'repugnante', 'repugnancia',
                      'repugnância', 'nojento', 'nojenta', 'nojo', 'despresível', 'despresível')

emo_sur_hashtags <- c('surpreso', 'surpresa', 'supreendido', 'supreendida', 'espantado',
                      'espantada', 'perplexo', 'perplexa', 'perplexidade', 'quemdiria',
                      'susto', 'assustado', 'assustada')

emo_ant_hashtags <- c('previsao', 'previsão')


emotions_hashtags <- c(emo_joy_hashtags, emo_sad_hashtags, 
                       emo_dis_hashtags, emo_tru_hashtags,
                       emo_ang_hashtags, emo_fear_hashtags,
                       emo_sur_hashtags, emo_ant_hashtags)

stocks_hashtags <- c("abev3",
                     "aedu3",
                     "alll3", 
                     "bbas3", 
                     "bbdc3", 
                     "bbdc4", 
                     "bbse3", 
                     "bisa3", 
                     "brap4", 
                     "brfs3", 
                     "brkm5", 
                     "brml3", 
                     "brpr3", 
                     "bvmf3", 
                     "ccro3", 
                     "cesp6", 
                     "ciel3", 
                     "cmig4", 
                     "cpfe3", 
                     "cple6", 
                     "cruz3", 
                     "csan3", 
                     "csna3", 
                     "ctip3", 
                     "cyre3", 
                     "dasa3", 
                     "dtex3", 
                     "ecor3", 
                     "elet3", 
                     "elet6", 
                     "elpl4", 
                     "embr3", 
                     "enbr3", 
                     "estc3", 
                     "even3", 
                     "fibr3", 
                     "gfsa3", 
                     "ggbr4", 
                     "goau4", 
                     "goll4", 
                     "hgtx3", 
                     "hype3", 
                     "itsa4", 
                     "itub4", 
                     "jbss3", 
                     "klbn11",
                     "klbn4", 
                     "krot3", 
                     "lame4", 
                     "ligt3", 
                     "llxl3", 
                     "lren3", 
                     "mrfg3", 
                     "mrve3", 
                     "natu3", 
                     "oibr4", 
                     "pcar4", 
                     "pdgr3", 
                     "petr3", 
                     "petr4", 
                     "qual3", 
                     "rent3", 
                     "rsid3", 
                     "sanb11",
                     "sbsp3", 
                     "suzb5", 
                     "tble3", 
                     "timp3", 
                     "ugpa3", 
                     "usim5", 
                     "vale3", 
                     "vale5", 
                     "vivt4")


additional_remove_words <- c('...', '....', 'pra', 'desse')

library(xtable)

visualize_word_distribution <- function(df, output_chart, output_table, title) {
  library(wordcloud)
  library(tm)
  
  wordcloud_corpus <- Corpus(DataframeSource(data.frame(doc_id=row.names(df), text=df$text)))
  # Remove stopwords and hashtags used as classes on distant supervision
  wordcloud_corpus <- tm_map(wordcloud_corpus, removeWords, c(stopwords('portuguese'), additional_remove_words, stocks_hashtags))
  
  tm <- TermDocumentMatrix(wordcloud_corpus)
  tm <- removeSparseTerms(tm, 0.999)
  rowTotals <- apply(tm, 1, sum)
  
  v <- sort(rowTotals, decreasing = TRUE)
  wordcloud_df <- data.frame(word = names(v), freq = v)
  
  rownames(wordcloud_df) <- NULL
  
  print.xtable(xtable(wordcloud_df[1:20,], caption=title, label=basename(output_table)), file=output_table)
  
  dev.off()
  png(output_chart, width=800,height=600)
  wordcloud(wordcloud_df$word, wordcloud_df$freq, min_freq=2, scale = c(4, 1), rot.per=0.35, max.words=100, colors=brewer.pal(6,"Dark2"),random.order=FALSE)
  #dev.off()
  
  # library(wordcloud)
  # library(tm)
  # 
  # wordcloud_corpus <- Corpus(DataframeSource(data.frame(df$text)))
  # # Remove stopwords and hashtags used as classes on distant supervision
  # wordcloud_corpus <- tm_map(wordcloud_corpus, removeWords, c(stopwords('portuguese'), emotions_hashtags, additional_remove_words, stocks_hashtags))
  # 
  # tm <- TermDocumentMatrix(wordcloud_corpus)
  # tm <- removeSparseTerms(tm, 0.999)
  # rowTotals <- apply(tm, 1, sum)
  # 
  # v <- sort(rowTotals, decreasing = TRUE)
  # wordcloud_df <- data.frame(word = names(v), freq = v)
  # 
  # rownames(wordcloud_df) <- NULL
  #   
  # print.xtable(xtable(wordcloud_df[1:20,], caption=title, label=basename(output_table)), file=output_table)
  # 
  # png(output_chart, width=300,height=200)
  # wordcloud(wordcloud_df$word, wordcloud_df$freq, min_freq=2, max.words=100, colors=brewer.pal(6,"Dark2"),random.order=FALSE)
  # dev.off()
}


visualize_emo_lexicon <- function(df, output_chart, title, output_lex_table, output_pos_neg_table ) {
  library("syuzhet")
  
  d <- get_nrc_sentiment(as.character(df$text))
  
  td <- data.frame(t(d))
  
  td_new <- data.frame(rowSums(td))
  
  names(td_new)[1] <- "count"
  #td_new <- cbind("emotion" = rownames(td_new), td_new)
  td_new <- cbind("emotion" = c('ANG', 'ANT', 'DIS', 'FEA', 'JOY', 'SAD', 'SUR', 'TRU', 'NEG', 'POS'), 
                  td_new)
  rownames(td_new) <- NULL
  td_new2<-td_new[1:8,]
  
  td_new3<-td_new[9:10,]
  
  library("ggplot2")
  #print(qplot(emotion, data=td_new2, weight=count, geom="histogram",fill=emotion)+ggtitle(title))
  print(qplot(emotion, data=td_new2, weight=count, geom="bar",fill=emotion)+ggtitle(title)+
          geom_text(data=td_new2,aes(y=count,label=count),vjust=0)
  )
  ggsave(output_chart)
  
  #print(qplot(emotion, data=td_new3, weight=count, geom="histogram",fill=emotion)+ggtitle(title))
  print(qplot(emotion, data=td_new3, weight=count, geom="bar",fill=emotion)+ggtitle(title)+
          geom_text(data=td_new3,aes(y=count,label=count),vjust=0)
  )
  
  #Total Lexicon Words
  print.xtable(xtable(td_new2, caption=title, label=basename(output_lex_table)), file=output_lex_table)

  #Positive Words vs Negative Words
  print.xtable(xtable(td_new3, caption=paste(title, '(Positive vs Negative)'), label=basename(output_pos_neg_table)), file=output_pos_neg_table)
  
}


visualize_emo_counts <- function(df, output_chart, title, output_table ) {

  td_new <- data.frame(colSums(Filter(is.numeric, df)))
  
  names(td_new)[1] <- "count"
  td_new <- cbind("emotion" = rownames(td_new), td_new)
  rownames(td_new) <- NULL
  
  td_new2 <- subset(td_new, td_new$count > 0)
  
  td_new2<-td_new[1:8,]

  library("ggplot2")
  
  
  #print(qplot(emotion, data=td_new2, weight=count, geom="histogram",fill=emotion)+ggtitle(title))
  print(qplot(emotion, data=td_new2, weight=count, geom="bar",fill=emotion)+ggtitle(title)+
          geom_text(data=td_new2,aes(y=count,label=count),vjust=0)
          )
  ggsave(output_chart)
  
  print.xtable(xtable(td_new2, caption=title, label=basename(output_table)), file=output_table)
  
}



base_dir <- '/media/fernando/DADOS/UNICAMP/PHD/src/phd_project/experiments/visualization'

csv_dir <- '/media/fernando/DADOS/UNICAMP/PHD/src/phd_project/datasets'

#base_dir <- '/media/fernando/DADOS/UNICAMP/PHD/src/phd_project/experiments/visualization_full_agreement'

#csv_dir <- '/media/fernando/DADOS/UNICAMP/PHD/src/phd_project/experiments/full_agreement'


# Visualizing emotions in the whole corpora

df_ctx_free <- read.csv(file.path(csv_dir, "tweets_auto-tagged.csv"), sep=";")

df_ctx_free <- subset(df_ctx_free,  df_ctx_free$TRU == 1 |
                                  df_ctx_free$DIS == 1 |
                                    df_ctx_free$JOY == 1 |
                                    df_ctx_free$SAD == 1 |
                                    df_ctx_free$ANG == 1 |
                                    df_ctx_free$FEA == 1 |
                                    df_ctx_free$ANT == 1 |
                                    df_ctx_free$SUR == 1
                    )

# visualize_emo_counts(df_ctx_free,
#                     file.path(base_dir, "emotions_count_ctx_free.png"),
#                     "Annotated Emotions Count",
#                     file.path(base_dir, "emotions_count_ctx_free.tex"))
# 
# visualize_emo_lexicon(df_ctx_free, file.path(base_dir, "emotions_lexicon_ctx_free.png"),
#                      "Lexicon Words Frequency",
#                      file.path(base_dir, "emotions_lexicon_ctx_free.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free.tex"))

df_ctx_specific <- read.csv(file.path(csv_dir, "tweets_stocks.csv"), sep=";")

df_ctx_specific <- subset(df_ctx_specific,  df_ctx_specific$TRU == 1 |
                                             df_ctx_specific$DIS == 1 |
                                             df_ctx_specific$JOY == 1 | 
                                             df_ctx_specific$SAD == 1 | 
                                             df_ctx_specific$ANG == 1 |
                                             df_ctx_specific$FEA == 1 |
                                             df_ctx_specific$ANT == 1 |
                                             df_ctx_specific$SUR == 1
)

df_ctx_specific$TRU[df_ctx_specific$TRU == -1] <- 0
df_ctx_specific$DIS[df_ctx_specific$DIS == -1] <- 0
df_ctx_specific$JOY[df_ctx_specific$JOY == -1] <- 0
df_ctx_specific$SAD[df_ctx_specific$SAD == -1] <- 0
df_ctx_specific$ANG[df_ctx_specific$ANG == -1] <- 0
df_ctx_specific$FEA[df_ctx_specific$FEA == -1] <- 0
df_ctx_specific$ANT[df_ctx_specific$ANT == -1] <- 0
df_ctx_specific$SUR[df_ctx_specific$SUR == -1] <- 0



# visualize_emo_counts(df_ctx_specific, 
#                      file.path(base_dir, "emotions_count_ctx_specific.png"), 
#                      # "Annotated Emotions Count - Specific-Context corpus",
#                      "Annotated Emotions Count",
#                      file.path(base_dir, "emotions_count_ctx_specific.tex"))
# 
# visualize_emo_lexicon(df_ctx_specific, file.path(base_dir, "emotions_lexicon_ctx_specific.png"), 
#                       #"Lexicon Words - Specific-Context",
#                       "Lexicon Words",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific.tex"))
# 
# # Visualizing lexicon DB distribution for each set of emotional tweets from context free corpus...
# 
# df_ctx_free_tru <- subset(df_ctx_free, df_ctx_free$TRU == 1)
# visualize_emo_lexicon(df_ctx_free_tru,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_tru.png"),
#                      "Lexicon Words (Trust)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_tru.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_tru.tex"))
# 
# df_ctx_free_dis <- subset(df_ctx_free, df_ctx_free$DIS == 1)
# visualize_emo_lexicon(df_ctx_free_dis,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_dis.png"),
#                      "Lexicon Words (Disgust)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_dis.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_dis.tex"))
# 
# df_ctx_free_joy <- subset(df_ctx_free, df_ctx_free$JOY == 1)
# visualize_emo_lexicon(df_ctx_free_joy,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_joy.png"),
#                      "Lexicon Words (Joy)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_joy.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_joy.tex"))
# 
# df_ctx_free_sad <- subset(df_ctx_free, df_ctx_free$SAD == 1)
# visualize_emo_lexicon(df_ctx_free_sad,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_sad.png"),
#                      "Lexicon Words (Sad)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_sad.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_sad.tex"))
# 
# df_ctx_free_fea <- subset(df_ctx_free, df_ctx_free$FEA == 1)
# visualize_emo_lexicon(df_ctx_free_fea,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_fea.png"),
#                      "Lexicon Words (Fear)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_fea.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_fea.tex"))
# 
# df_ctx_free_ang <- subset(df_ctx_free, df_ctx_free$ANG == 1)
# visualize_emo_lexicon(df_ctx_free_ang,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_ang.png"),
#                      "Lexicon Words (Anger)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_ang.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_ang.tex"))
# 
# df_ctx_free_ant <- subset(df_ctx_free, df_ctx_free$ANT == 1)
# visualize_emo_lexicon(df_ctx_free_ant,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_ant.png"),
#                      "Lexicon Words (Anticip.)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_ant.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_ant.tex"))
# 
# df_ctx_free_sur <- subset(df_ctx_free, df_ctx_free$SUR == 1)
# visualize_emo_lexicon(df_ctx_free_sur,
#                      file.path(base_dir, "emotions_lexicon_ctx_free_sur.png"),
#                      "Lexicon Words (Surp.)",
#                      file.path(base_dir, "emotions_lexicon_ctx_free_sur.tex"),
#                      file.path(base_dir, "emotions_pos_vs_neg_ctx_free_sur.tex"))
# 
# # Visualizing lexicon DB distribution for each set of emotional tweets from context specific (stock) corpus...
# 
# df_ctx_specific_tru <- subset(df_ctx_specific, df_ctx_specific$TRU == 1)
# visualize_emo_lexicon(df_ctx_specific_tru, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_tru.png"), 
#                       #"Lexicon Words - Specific-Context (Trust)",
#                       "Lexicon Words (Trust)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_tru.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_tru.tex"))
# 
# df_ctx_specific_dis <- subset(df_ctx_specific, df_ctx_specific$DIS == 1)
# visualize_emo_lexicon(df_ctx_specific_dis, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_dis.png"), 
#                       #"Lexicon Words - Specific-Context (Disgust)",
#                       "Lexicon Words (Disgust)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_dis.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_dis.tex"))
# 
# df_ctx_specific_joy <- subset(df_ctx_specific, df_ctx_specific$JOY == 1)
# visualize_emo_lexicon(df_ctx_specific_joy, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_joy.png"), 
#                       #"Lexicon Words - Specific-Context (Joy)",
#                       "Lexicon Words (Joy)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_joy.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_joy.tex"))
# 
# df_ctx_specific_sad <- subset(df_ctx_specific, df_ctx_specific$SAD == 1)
# visualize_emo_lexicon(df_ctx_specific_sad, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_sad.png"), 
#                       #"Lexicon Words - Specific-Context (Sad)",
#                       "Lexicon Words (Sad)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_sad.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_sad.tex"))
# 
# df_ctx_specific_fea <- subset(df_ctx_specific, df_ctx_specific$FEA == 1)
# visualize_emo_lexicon(df_ctx_specific_fea, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_fea.png"), 
#                       #"Lexicon Words - Specific-Context (Fear)",
#                       "Lexicon Words (Fear)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_fea.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_fea.tex"))
# 
# df_ctx_specific_ang <- subset(df_ctx_specific, df_ctx_specific$ANG == 1)
# visualize_emo_lexicon(df_ctx_specific_ang, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_ang.png"), 
#                       #"Lexicon Words - Specific-Context (Anger)",
#                       "Lexicon Words (Anger)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_ang.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_ang.tex"))
# 
# df_ctx_specific_ant <- subset(df_ctx_specific, df_ctx_specific$ANT == 1)
# visualize_emo_lexicon(df_ctx_specific_ant, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_ant.png"), 
#                       #"Lexicon Words - Specific-Context (Anticip.)",
#                       "Lexicon Words (Anticipation)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_ant.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_ant.tex"))
# 
# df_ctx_specific_sur <- subset(df_ctx_specific, df_ctx_specific$SUR == 1)
# visualize_emo_lexicon(df_ctx_specific_sur, 
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_sur.png"), 
#                       #"Lexicon Words - Specific-Context (Surpr.)",
#                       "Lexicon Words (Surprise)",
#                       file.path(base_dir, "emotions_lexicon_ctx_specific_sur.tex"),
#                       file.path(base_dir, "emotions_pos_vs_neg_ctx_specific_sur.tex"))

# Visualizing word distribution
visualize_word_distribution(df_ctx_free, file.path(base_dir, "wordcloud_ctx_free.png"),
                            file.path(base_dir, "wordcloud_ctx_free.tex"),
                            "Most frequent words")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$JOY == 1),
                               file.path(base_dir, "wordcloud_ctx_free_joy.png"),
                            file.path(base_dir, "wordcloud_ctx_free_joy.tex"),
                            "Most frequent words in Joy tweets")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$SAD == 1),
                            file.path(base_dir, "wordcloud_ctx_free_sad.png"),
                            file.path(base_dir, "wordcloud_ctx_free_sad.tex"),
                            "Most frequent words in Sad tweets")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$FEA == 1),
                            file.path(base_dir, "wordcloud_ctx_free_fea.png"),
                            file.path(base_dir, "wordcloud_ctx_free_fea.tex"),
                            "Most frequent words in Fear tweets")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$ANG == 1),
                            file.path(base_dir, "wordcloud_ctx_free_ang.png"),
                            file.path(base_dir, "wordcloud_ctx_free_ang.tex"),
                            "Most frequent words in Anger tweets")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$TRU == 1),
                            file.path(base_dir, "wordcloud_ctx_free_tru.png"),
                            file.path(base_dir, "wordcloud_ctx_free_tru.tex"),
                            "Most frequent words in Trust tweets")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$DIS == 1),
                            file.path(base_dir, "wordcloud_ctx_free_dis.png"),
                            file.path(base_dir, "wordcloud_ctx_free_dis.tex"),
                            "Most frequent words in Disgust tweets")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$ANT == 1),
                            file.path(base_dir, "wordcloud_ctx_free_ant.png"),
                            file.path(base_dir, "wordcloud_ctx_free_ant.tex"),
                            "Most frequent words in Anticipation tweets")

visualize_word_distribution(subset(df_ctx_free, df_ctx_free$SUR == 1),
                            file.path(base_dir, "wordcloud_ctx_free_sur.png"),
                            file.path(base_dir, "wordcloud_ctx_free_sur.tex"),
                            "Most frequent words in Surprise tweets")

# Visualizing word distribution for context specific corpus
# visualize_word_distribution(df_ctx_specific, file.path(base_dir, "wordcloud_ctx_specific.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific.tex"),
#                             "Specific-Context most frequent words")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$JOY == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_joy.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_joy.tex"),
#                             "Specific-Context most frequent words in Joy tweets")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$SAD == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_sad.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_sad.tex"),
#                             "Specific-Context most frequent words in Sad tweets")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$FEA == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_fea.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_fea.tex"),
#                             "Specific-Context most frequent words in Fear tweets")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$ANG == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_ang.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_ang.tex"),
#                             "Specific-Context most frequent words in Anger tweets")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$TRU == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_tru.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_tru.tex"),
#                             "Specific-Context most frequent words in Trust tweets")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$DIS == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_dis.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_dis.tex"),
#                             "Specific-Context most frequent words in Disgust tweets")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$ANT == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_ant.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_ant.tex"),
#                             "Specific-Context most frequent words in Anticipation tweets")
# 
# visualize_word_distribution(subset(df_ctx_specific, df_ctx_specific$SUR == 1),
#                             file.path(base_dir, "wordcloud_ctx_specific_sur.png"),
#                             file.path(base_dir, "wordcloud_ctx_specific_sur.tex"),
#                             "Specific-Context most frequent words in Surprise tweets")