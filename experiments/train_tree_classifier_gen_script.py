import sys
import os

from sklearn.model_selection import ParameterSampler
import numpy as np
import scipy
from scipy import stats

sys.path.append(os.getcwd())

#import pudb; pudb.set_trace();


BASE_SCRIPT_CONTENT = '''\
#!/bin/bash
# Usage: svm_light_cross_validation full_data num_train_items num_test_items k_cycles results_file
# based in http://niketanblog.blogspot.com/2012/02/cross-validation-on-svm-light.html
RESULT_FILE=$5

echo "Running SVM-Light via cross validation on" $1 "by using" $2 "training items and" $3 "test items (Total number of cross-validation cycles:" $4 > $RESULT_FILE

MODEL_FILE="../models/classifier-tree-kernel-model_$EMO1_vs_$EMO2"
TEMP_FILE="svm-tree-kernel-tempFile_$EMO1_vs_$EMO2."$RANDOM".txt"
PRED_FILE="../logs/svm-tree-kernel-prediction_$EMO1_vs_$EMO2."$RANDOM".txt"
DATA_FILE=$1
NUM_TRAIN=$2
NUM_TEST=$3
NUM_CYCLES=$4

TEMP_DATA_FILE=$DATA_FILE"."$RANDOM".temp"
TRAIN_FILE=$TEMP_DATA_FILE".train"
TEST_FILE=$TEMP_DATA_FILE".test"

TEMP_RESULT=$RESULT_FILE".temp"
SVM_PATTERN='s/Accuracy on test set: \([0-9]*.[0-9]*\)% ([0-9]* correct, [0-9]* incorrect, [0-9]* total)\.*/'
for k in `seq 1 $NUM_CYCLES`
do
 sort -R $DATA_FILE > $TEMP_DATA_FILE
 head -n $NUM_TRAIN $TEMP_DATA_FILE > $TRAIN_FILE
 tail -n $NUM_TEST $TEMP_DATA_FILE > $TEST_FILE

 echo "------------------------------------------"  >> $RESULT_FILE
 echo "Cross-validation cycle:" $k >> $RESULT_FILE

 echo ""  >> $RESULT_FILE

 $SCRIPT_ALG_BLOCKS

done
 '''

SCRIPT_ALG_BASE_BLOCK = '''\

  echo "$ALG_NAME, decay $d, c $c"  >> $RESULT_FILE
  CURR_MODEL_FILE=$MODEL_FILE"."$RANDOM".txt"
  $SVM_TREE_KERNEL_INSTALL_DIR/svm_learn -t 5 -F $TYPE -$DECAY $d -c $c $TRAIN_FILE $CURR_MODEL_FILE > $TEMP_FILE
  $SVM_TREE_KERNEL_INSTALL_DIR/svm_classify -v 1 $TEST_FILE $CURR_MODEL_FILE $PRED_FILE > $TEMP_RESULT
  cat $TEMP_RESULT >> $RESULT_FILE
  sed '/^Reading model/d' $TEMP_RESULT > $TEMP_RESULT"1"
  sed '/^Precision/d' $TEMP_RESULT"1" > $TEMP_RESULT
  sed "$SVM_PATTERN$k $ALG_NAME $i \1/g" $TEMP_RESULT >> $RESULT_FILE".better"

'''

SVM_TREE_ALGS = { "MOS-KERNEL": {"type": '-1', "decay": "L"}, 
                  "ST-KERNEL": {"type": '0', "decay": "L"}, 
                  "SST-KERNEL": {"type": '1', "decay": "L"},
				  "SST-bow-KERNEL": {"type": '2', "decay": "L"},
				  "PT-KERNEL": {"type": '3', "decay": "M"}}

SVM_TREE_KERNEL_INSTALL_DIR = "../libs/svm-light-TK-1.2/svm-light-TK-1.2.1"


def create_one_vs_all_data_file(dataset_path, dataset_emo_path_neutral, emo, invert, resample=None):
    
    if invert:
        emo_pair_new = ('ALL', emo[0])
    else:        
        emo_pair_new = ('ALL', emo[1])
    
    dataset_one_vs_all_path = dataset_path.replace(".csv", "_svmlight_%s_vs_%s.txt" % emo_pair_new)

    dataset_emo_path = dataset_path.replace(".csv", "_svmlight_%s_vs_%s.txt" % (emo[0], emo[1]))
    
    dataset_one_vs_all_file = open(dataset_one_vs_all_path, 'w')
    
    orig_dataset_file = open(dataset_emo_path, 'r')
    orig_line = orig_dataset_file.readline()
    num_lines_class_one = 0
    num_lines_class_all = 0
    while orig_line:
        orig_line_split = orig_line.split('\t')
        if invert:                  
            cat = int(orig_line_split[0])*-1
            new_line = "%d\t%s" % (int(orig_line_split[0])*-1, orig_line_split[1])
        else:
            cat = int(orig_line_split[0])
            new_line = orig_line
        
        if cat == 1:
          num_lines_class_one += 1
        else:
          num_lines_class_all += 1
        dataset_one_vs_all_file.write(new_line)
        
        orig_line = orig_dataset_file.readline()    
        
    orig_dataset_file.close()
        
    neutral_dataset_file = open(dataset_emo_path_neutral, 'r')
    neutral_line = neutral_dataset_file.readline()
    while neutral_line:
        neutral_line_split = neutral_line.split('\t')                
        dataset_one_vs_all_file.write('-1\t%s' % neutral_line_split[1])    
        neutral_line = neutral_dataset_file.readline()
        num_lines_class_all += 1
    
    neutral_dataset_file.close()    
    

    if not resample is None:
      print('Resampling dataset...')
      if resample == 'up':
        print('Upsampling dataset...')
        print("num_lines_class_one = %d, num_lines_class_all = %d" % (num_lines_class_one, num_lines_class_all))
        
        if num_lines_class_one < num_lines_class_all:
          src_file_path = dataset_emo_path
          num_lines = num_lines_class_all - num_lines_class_one
        elif num_lines_class_all < num_lines_class_one:
          src_file_path = dataset_emo_path_neutral
          num_lines = num_lines_class_one - num_lines_class_all

        print('Duplicating %d lines from file %s' % (num_lines, src_file_path))
        
        i = 0
        while i < num_lines:
          src_file = open(src_file_path, 'r')
          src_line = src_file.readline()                    

          while src_line and i < num_lines:
            src_line_split = src_line.split('\t')
            if invert:
              cat = int(src_line_split[0]) * -1
            else:
              cat = int(src_line_split[0])

            if cat == 1:
              i += 1
              dataset_one_vs_all_file.write(src_line)
            src_line = src_file.readline()
          print("Copied %d lines... Returning to the begin of the file." % (i))

          src_file.close()

          
          
        src_file.close()


      elif resample == 'down':
          pass # We aren't doing downsampling for now...

    dataset_one_vs_all_file.close()
        



def gen_train_svm_tree_emo(dataset_path, emo, algs, cv):
    dataset_emo_path = dataset_path.replace(".csv", "_svmlight_%s_vs_%s.txt" % (emo[0], emo[1]))
    results_log_path = "../logs/results_train_tree_classifier_%s_vs_%s.log" % (emo[0], emo[1])
       
    script_content = BASE_SCRIPT_CONTENT
    script_alg_blocks = ""

    param_grid = {'C': scipy.stats.expon(scale=100)}
    rng = np.random.RandomState(42)
    param_list = list(ParameterSampler(param_grid, n_iter=20, random_state=rng))

    for alg in algs:
        for par in param_list:
          script_alg_block = SCRIPT_ALG_BASE_BLOCK
          script_alg_block = script_alg_block.replace("$ALG_NAME", alg)
          script_alg_block = script_alg_block.replace("$TYPE", SVM_TREE_ALGS[alg]["type"])
          script_alg_block = script_alg_block.replace("$DECAY", SVM_TREE_ALGS[alg]["decay"])
          #script_alg_block = script_alg_block.replace("$d", "%.2f" % (i/10))
          script_alg_block = script_alg_block.replace("$d", "0.1")
          script_alg_block = script_alg_block.replace("$c", "%.6f" % par['C'])

          script_alg_blocks += script_alg_block


    script_content = script_content.replace("$1", dataset_emo_path)
    script_content = script_content.replace("$2", "$((`wc -l < %s`/%d))" % (dataset_emo_path, cv))
    script_content = script_content.replace("$3", "$((`wc -l < %s`/%d*%d))" % (dataset_emo_path, cv, cv-1))
    script_content = script_content.replace("$4", "%d" % cv)
    script_content = script_content.replace("$5", results_log_path)
    script_content = script_content.replace("$EMO1", emo[0])
    script_content = script_content.replace("$EMO2", emo[1])
    script_content = script_content.replace("$SCRIPT_ALG_BLOCKS", script_alg_blocks)
    script_content = script_content.replace("$SVM_TREE_KERNEL_INSTALL_DIR", SVM_TREE_KERNEL_INSTALL_DIR)

    script_content += "echo 'Done.' >> $RESULT_FILE"

    script_file = open("../scripts/train_tree_classifier_%s_vs_%s.sh" % (emo[0], emo[1]), "w")
    script_file.write(script_content)
    script_file.close()


def gen_train_svm_tree(dataset_path, emotions, algs, include_neutral=False, one_vs_one=True, resample=None):
    cv = 3

    for emo in emotions:        

        if include_neutral or not one_vs_one:
           dataset_emo_path_neutral = dataset_path.replace(".csv", "_svmlight_%s_vs_%s_neutral.txt" % (emo[0], emo[1]))
           
        if one_vs_one:
            gen_train_svm_tree_emo(dataset_path, emo, algs, cv)
        else:
            create_one_vs_all_data_file(dataset_path, 
                                       dataset_emo_path_neutral,
                                       emo,
                                       False,
                                       resample)
            
            gen_train_svm_tree_emo(dataset_path, ('ALL', emo[1]), algs, cv)
            
            create_one_vs_all_data_file(dataset_path, 
                                       dataset_emo_path_neutral,
                                       emo,
                                       True,
                                       resample)
            
            gen_train_svm_tree_emo(dataset_path, ('ALL', emo[0]), algs, cv)
            
def create_test_set(dataset_path, resample=None):    
    for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:
          dataset_emo_path_neutral = dataset_path.replace(".csv", "_svmlight_%s_vs_%s_neutral.txt" % (emo[0], emo[1]))

          create_one_vs_all_data_file(dataset_path, 
                                dataset_emo_path_neutral,
                                emo,
                                False,
                                resample)

          create_one_vs_all_data_file(dataset_path, 
                                dataset_emo_path_neutral,
                                emo,
                                True,
                                resample)
        

gen_train_svm_tree("../datasets/tweets_auto-tagged-emolex_train.csv", 
                    [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')],
					["MOS-KERNEL"])

gen_train_svm_tree("../datasets/tweets_auto-tagged-emolex_train.csv", 
                    [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')],
          ["MOS-KERNEL"],
                    one_vs_one=False)



'''
gen_train_svm_tree("../datasets/tweets_auto-tagged-emolex_train.csv", 
                    [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')],
					["MOS-KERNEL", "ST-KERNEL", "SST-KERNEL", "SST-bow-KERNEL", "PT-KERNEL"],
                    one_vs_one=False,
                    resample='up')
'''

# Generating ONE vs ALL test datasets


#create_test_set("../datasets/tweets_auto-tagged-emolex_test.csv")
#create_test_set("../datasets/tweets_stocks_emolex.csv")
#create_test_set("../datasets/full_agreement/tweets_stocks_emolex.csv")