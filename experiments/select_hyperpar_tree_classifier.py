import sys
import os

sys.path.append(os.getcwd())

import pandas as pd

#import pudb; pudb.set_trace()

DECAY_PATTERN = ', decay '
C_PATTERN = ', c '
#DECAY_VAL_LEN = len('0.00')
DECAY_VAL_LEN = len('0.0')
PREC_RECALL_PATTERN = 'Precision/recall'

def find_best_hyperpars(df_results, emos, logs_path):    
    #df_results_avg_cycles = df_results.groupby(['classifier','kernel','decay']).mean()
    
    best_hyperpar_file = open(os.path.join(logs_path, \
                                        'best_hyperpars_tree_classifier.log'), 'w')    
    
    for emo in emos:
        
        df_results_emo = df_results.loc[df_results['classifier'] == '%s_vs_%s' % emo] 
        
        df_results_emo = df_results_emo.groupby(['kernel','decay', 'c']).mean()
        
        df_results_emo = df_results_emo.loc[df_results_emo['f1'].idxmax()]
        
        best_hyperpar_file.write("Best Parameters for %s_vs_%s: " % emo)
        
        best_hyperpar_file.write(str(df_results_emo))
            
        best_hyperpar_file.write('\n\n')
        
    best_hyperpar_file.close()




def gen_results_df(emos, logs_path):       

    df_results = pd.DataFrame(columns=['classifier', 'kernel', 'decay', 'cv_cycle', \
                                       'prec', 'rec'])    

    for emo in emos:
        f = open('../logs/results_train_tree_classifier_%s_vs_%s.log' % emo, 'r')
        
        log_content = f.read()
        f.close()
        
        cv_cycles = log_content.split('Cross-validation cycle')
        
        for i in range(1,len(cv_cycles)):
            trainnings = cv_cycles[i].split('\n')
            trainnings = ['\n'.join(trainnings[i:i+4]) for i in range(2, len(trainnings), 4)]
            
            for train in trainnings:
                decay_pos = train.find(DECAY_PATTERN)
                if decay_pos == -1:
                    continue
                c_pos = train.find(C_PATTERN)
                end_c_pos = train[c_pos+len(C_PATTERN):].find('\n')
                kernel = train[:decay_pos]
                decay = float(train[decay_pos+len(DECAY_PATTERN):decay_pos+len(DECAY_PATTERN)+DECAY_VAL_LEN])
                c = float(train[c_pos+len(C_PATTERN):c_pos+len(C_PATTERN)+end_c_pos])

                prec_recall_pos = train.find(PREC_RECALL_PATTERN)
                
                if prec_recall_pos == -1:
                    continue
                                
                prec_recall_str = train[prec_recall_pos:]
                
                lines = prec_recall_str.split('\n')
                
                
                tokens = lines[0].split(' ')
                prec = float(tokens[len(tokens)-1].split('/')[0].replace('%',''))
                recall = float(tokens[len(tokens)-1].split('/')[1].replace('%',''))
                
                
                df_train_res = pd.DataFrame({'classifier': ['%s_vs_%s' % emo],
                                             'kernel': [kernel],
                                             'decay': [decay],
                                             'c':[c],
                                             'cv_cycle':[i],
                                             'prec':[prec],
                                             'rec':[recall]})
    
                df_results = df_results.append(df_train_res)
    
    df_results['f1'] = 2*((df_results['prec'] * df_results['rec'])/(df_results['prec'] + df_results['rec']))
    
    return df_results
                        

#emos = [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT','SUR')]
#emos = [('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT','SUR')]
emos = [('JOY', 'SAD')]
df_results = gen_results_df(emos, '../logs')

df_results.to_csv('../logs/results_tree_classifier_c_hyper_JOY_VS_SAD.csv')

find_best_hyperpars(df_results, emos, '../logs')