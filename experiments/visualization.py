import pandas as pd
import matplotlib.pyplot as plt

df_ctx_specific = pd.read_csv('tweets_stocks.csv', sep=';', index_col=0)

df_ctx_free = pd.read_csv('tweets_auto-tagged.csv', sep=';', index_col=0)

emotion_labels = ['TRU', 'DIS', 'JOY', 'SAD', 'ANT', 'SUR', 'ANG', 'FEA']


# Counting emotions in the corpus

print 'Context-Free corpus'

emotion_counts = []
for emo in emotion_labels:
    value_counts =  df_ctx_free[emo].value_counts()
    print 'Emotion %s = %d' % (emo, value_counts[1])
    emotion_counts.append(value_counts[1])


fig = plt.figure()
fig.suptitle('Context-Free corpus Emotions')

count_series = pd.Series(emotion_counts, index=emotion_labels)
count_plot = count_series.plot(kind='bar')

plt.show()
fig.savefig('emotions_count_ctx_free.jpg')

print 'Context Specific corpus'
emotion_counts = []
for emo in emotion_labels:
    value_counts =  df_ctx_specific[emo].value_counts()
    print 'Emotion %s = %d' % (emo, value_counts[1])
    emotion_counts.append(value_counts[1])

fig = plt.figure()
fig.suptitle('Context-Specific corpus Emotions')

count_series = pd.Series(emotion_counts, index=emotion_labels)
count_plot = count_series.plot(kind='bar')

plt.show()
fig.savefig('emotions_count_ctx_specific.jpg')
