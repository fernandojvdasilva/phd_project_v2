'''
Created on 15 de set de 2017

@author: fernando
'''
import pandas as pd
import numpy as np
import pickle
import os

from nlp_utils.nlp_bag_of_words import df_tfidf_train_test_split
from mlutils.dim_reduction import svd_reduct

def load_dataframe(dataframe_path, models_dir):
    try:
        dataframe_train = pd.read_csv(dataframe_path.replace('.csv', '_neutral_train.csv'), engine='c')
        dataframe_test = pd.read_csv(dataframe_path.replace('.csv', '_neutral_test.csv'), engine='c')
        tfs_train = pickle.load(open(dataframe_path.replace('.csv', '_neutral_train.pkl'), 'rb'))
        tfs_test = pickle.load(open(dataframe_path.replace('.csv', '_neutral_test.pkl'), 'rb'))
        svd_train = pickle.load(open(dataframe_path.replace('.csv', '_neutral_svd_train.pkl'), 'rb'))
        svd_test = pickle.load(open(dataframe_path.replace('.csv', '_neutral_svd_test.pkl'), 'rb'))
    except Exception as ex:
        dataframe_full = pd.read_csv(dataframe_path, engine='c')
        dataframe_full = dataframe_full.loc[dataframe_full['NEUTRAL'] == 1]
        # Using only 10% of all neutral tweets
        indexes = np.random.rand(len(dataframe_full)) < 0.1
        dataframe_full = dataframe_full[indexes]
        try:
            tfs = pickle.load(open(dataframe_path.replace('csv', 'pkl'), "rb"))
        except:
            tfidf = pickle.load(open(os.path.join(models_dir, 'tfidf_vectorizer.pkl'), "rb"), encoding='latin1')
            tfs = tfidf.transform(dataframe_full['text'])
        tfs_train, dataframe_train, tfs_test, dataframe_test = df_tfidf_train_test_split(dataframe_full, tfs, 0.8)
        # We have to use the previous saved SVD transformer
        svd_transformer = pickle.load(open(os.path.join(models_dir, 'svd_transformer.pkl'), "rb"), encoding='latin1')
        svd_train, svd_test, num_dim, svd_transformer = svd_reduct(tfs_train, tfs_test, 0.5, svd_transformer)
        # Saving split data
        dataframe_train.to_csv(dataframe_path.replace('.csv', '_neutral_train.csv'))
        dataframe_test.to_csv(dataframe_path.replace('.csv', '_neutral_test.csv'))
        # Saving vectorized data
        with open(dataframe_path.replace('.csv', '_neutral_train.pkl'), 'wb') as handle:
            pickle.dump(tfs_train, handle)
        handle.close()
        with open(dataframe_path.replace('.csv', '_neutral_test.pkl'), 'wb') as handle:
            pickle.dump(tfs_test, handle)
        with open(dataframe_path.replace('.csv', '_neutral_test.pkl'), 'wb') as handle:
            pickle.dump(tfs_test, handle)
        handle.close()
        # Saving SVD reduced data
        with open(dataframe_path.replace('.csv', '_neutral_svd_train.pkl'), 'wb') as handle:
            pickle.dump(svd_train, handle)
        handle.close()
        with open(dataframe_path.replace('.csv', '_neutral_svd_test.pkl'), 'wb') as handle:
            pickle.dump(svd_test, handle)
        handle.close()
    return svd_train, svd_test, dataframe_train, dataframe_test


def add_emolex_features(svd_train, svd_test, dataframe_train, dataframe_test, normalizer):
    svd_and_emolex_train = svd_train
    svd_and_emolex_test = svd_test
    for dim in ["emolex_joy","emolex_sad","emolex_trust",\
                "emolex_disgust","emolex_anger","emolex_fear",\
                "emolex_anticipation","emolex_surprise",\
                "emolex_positive","emolex_negative"]:
        svd_and_emolex_train = np.c_[svd_and_emolex_train, dataframe_train[dim]]
        svd_and_emolex_test = np.c_[svd_and_emolex_test, dataframe_test[dim]]
                
    svd_and_emolex_train = normalizer.transform(svd_and_emolex_train)
    svd_and_emolex_test = normalizer.transform(svd_and_emolex_test)

    return svd_and_emolex_train, svd_and_emolex_test



def load_classifiers(models_dir, load_dl=False):        
    try:
        clf = pickle.load(open(os.path.join(models_dir, 'probs_train_neutral.pkl'), 'rb'))
    except:
        clf = {}
        
        clf['JOY-vs-SAD'] = {}
        clf['TRU-vs-DIS'] = {}
        clf['ANG-vs-FEA'] = {}
        clf['ANT-vs-SUR'] = {}
        
        
    clf['JOY-vs-SAD']['model'] = pickle.load(open(os.path.join(models_dir, 
                'classifier_JOY_vs_SAD_emolex.pkl'), 'rb'), 
        encoding='latin1')
    clf['TRU-vs-DIS']['model'] = pickle.load(open(os.path.join(models_dir, 
                'classifier_TRU_vs_DIS_emolex.pkl'), 'rb'), 
        encoding='latin1')
    clf['ANG-vs-FEA']['model'] = pickle.load(open(os.path.join(models_dir, 
                'classifier_ANG_vs_FEA_emolex.pkl'), 'rb'), 
        encoding='latin1')
    clf['ANT-vs-SUR']['model'] = pickle.load(open(os.path.join(models_dir, 
                'classifier_ANT_vs_SUR_emolex.pkl'), 'rb'), 
        encoding='latin1')
    
    if load_dl:
        from keras.models import load_model
    
        clf['JOY-vs-SAD']['model-dl'] = load_model(os.path.join(models_dir, 
                                                                             'classifier_dl_JOY_vs_SAD_emolex.h5'))
        
        clf['TRU-vs-DIS']['model-dl'] = load_model(os.path.join(models_dir, 
                                                                             'classifier_dl_TRU_vs_DIS_emolex.h5'))
        
        clf['ANG-vs-FEA']['model-dl'] = load_model(os.path.join(models_dir, 
                                                                             'classifier_dl_ANG_vs_FEA_emolex.h5'))
        
        clf['ANT-vs-SUR']['model-dl'] = load_model(os.path.join(models_dir, 
                                                                             'classifier_dl_ANT_vs_SUR_emolex.h5'))
        
    return clf

def extract_emos_attrs_n_class(dataframe, attrs, emo1, emo2):
    train_emos_indexes = [i for i in range(dataframe.shape[0]) \
    if (dataframe[emo1][i] == 1 and dataframe[emo2][i] == 0) or (dataframe[emo2][i] == 1 and dataframe[emo1][i] == 0)]
    emos_class_list = []
    for i in train_emos_indexes:
        if dataframe[emo1][i] == 1:
            emos_class_list.append(1)
        else:
            emos_class_list.append(0)
    
    emos_class = np.array(emos_class_list)
    emos_attrs = attrs[train_emos_indexes]    
    return emos_class, emos_attrs, train_emos_indexes

def extract_emos_n_class(dataframe, emo1, emo2):
    train_emos_indexes = [i for i in range(dataframe.shape[0]) \
    if (dataframe[emo1][i] == 1 and dataframe[emo2][i] == 0) or (dataframe[emo2][i] == 1 and dataframe[emo1][i] == 0)]
    emos_class_list = []
    for i in train_emos_indexes:
        if dataframe[emo1][i] == 1:
            emos_class_list.append(1)
        else:
            emos_class_list.append(0)
    
    emos_class = np.array(emos_class_list)    
    return emos_class, train_emos_indexes


def extract_neutral_n_class_and_with_all_neutral(dataframe, emo1, emo2):
    train_emos_indexes = [i for i in dataframe[emo1].index if dataframe[emo1][i] == 0 and dataframe[emo2][i] == 0]
    emos_class_list = []
    for i in train_emos_indexes:
        if dataframe[emo1][i] == 1:
            emos_class_list.append(1)
        else:
            emos_class_list.append(0)
    
    emos_class = np.array(emos_class_list)
        
    return emos_class, train_emos_indexes


def extract_neutral_attrs_n_class_and_with_all_neutral(dataframe, attrs, emo1, emo2):
    train_emos_indexes = [i for i in dataframe[emo1].index if dataframe[emo1][i] == 0 and dataframe[emo2][i] == 0]
    emos_class_list = []
    for i in train_emos_indexes:
        if dataframe[emo1][i] == 1:
            emos_class_list.append(1)
        else:
            emos_class_list.append(0)
    
    emos_class = np.array(emos_class_list)
    emos_attrs = attrs[train_emos_indexes]    
    return emos_class, emos_attrs, train_emos_indexes


def extract_neutral_attrs_n_class(dataframe, attrs, emo1, emo2):
    train_emos_indexes = [i for i in range(dataframe.shape[0]) \
    if dataframe[emo1][i] == 0 and dataframe[emo2][i] == 0 and \
    not (dataframe['TRU'][i] == dataframe['DIS'][i] == dataframe['JOY'][i] == dataframe['SAD'][i] and \
    dataframe['ANG'][i] == dataframe['FEA'][i] == dataframe['ANT'][i] == dataframe['SUR'][i] == 0)]
    #train_emos_indexes = [i for i in dataframe[emo1].index if dataframe[emo1][i] == 0 and dataframe[emo2][i] == 0]
    emos_class_list = []
    for i in train_emos_indexes:
        if dataframe[emo1][i] == 1:
            emos_class_list.append(1)
        else:
            emos_class_list.append(0)
    
    emos_class = np.array(emos_class_list)
    emos_attrs = attrs[train_emos_indexes]    
    return emos_class, emos_attrs, train_emos_indexes

def extract_neutral_n_class(dataframe, emo1, emo2):
    train_emos_indexes = [i for i in range(dataframe.shape[0]) if dataframe[emo1][i] == 0 and dataframe[emo2][i] == 0 and \
    not (dataframe['TRU'][i] == dataframe['DIS'][i] == dataframe['JOY'][i] == dataframe['SAD'][i] and \
    dataframe['ANG'][i] == dataframe['FEA'][i] == dataframe['ANT'][i] == dataframe['SUR'][i] == 0)]
    #train_emos_indexes = [i for i in dataframe[emo1].index if dataframe[emo1][i] == 0 and dataframe[emo2][i] == 0]
    emos_class_list = []
    for i in train_emos_indexes:
        if dataframe[emo1][i] == 1:
            emos_class_list.append(1)
        else:
            emos_class_list.append(0)
    
    emos_class = np.array(emos_class_list)        
    return emos_class, train_emos_indexes



def load_emo_dataframe(models_dir):
    emo_svd_train = pickle.load(open(os.path.join(models_dir, 'tweets_auto-tagged-emolex_svd_train.pkl'), "rb"), 
        encoding='latin1')
    emo_svd_test = pickle.load(open(os.path.join(models_dir, 
                'tweets_auto-tagged-emolex_svd_test.pkl'), "rb"), 
        encoding='latin1')
    emo_dataframe_train = pd.read_csv(os.path.join(models_dir, 
            'tweets_auto-tagged-emolex_train.csv'), 
        engine='c')
    emo_dataframe_test = pd.read_csv(os.path.join(models_dir, 
            'tweets_auto-tagged-emolex_test.csv'), 
        engine='c')
    
    return emo_svd_train, emo_svd_test, emo_dataframe_train, emo_dataframe_test