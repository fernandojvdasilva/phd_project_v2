'''
Created on 04/04/2016

@author: fernando
'''

import sys
sys.path.append("/media/DADOS/UNICAMP/PHD/src/phd_project")

import pickle
from mlutils.experiments import *
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
from sklearn.preprocessing.data import Normalizer

def test_one_vs_one(classifier, dataframe, tfs, emo1, emo2, corpus, label=None):
    
    test_emos_indexes = [i for i in range(dataframe.shape[0]) \
                                if dataframe[emo1][i] == 1 or dataframe[emo2][i] == 1]
    
    emos_class_list = []
    for i in test_emos_indexes:
        if dataframe[emo1][i] == 1:
            emos_class_list.append(1)
        else:
            emos_class_list.append(0) 
    
    emos_class = np.array(emos_class_list)
    emos_tfs = tfs[test_emos_indexes]
    
    
    ml_test_classifier(emos_tfs, emos_class, [emo2, emo1], classifier, corpus, label=label)
    
    
def test_one_vs_all(classifier, dataframe, tfs, emo, corpus, label=None):
    emos_class = dataframe[emo]   
    
    ml_test_classifier(tfs, emos_class, ['no-%s' % emo, emo], classifier, corpus, label=label)
            

def test_emo_benchmark(dataframe_path):
    dataframe = pd.read_csv(dataframe_path, engine='c')

    try:
        tfs = pickle.load(open(dataframe_path.replace('csv', 'pkl'), "rb" ) )
    except:               
        try:
            tfidf = pickle.load(open('experiments/tfidf_vectorizer.pkl', 'rb'))
        except:
            print("Vectorizer object not found... Please train your model...")
            return
        
        tfs = tfidf.transform(dataframe['text'])
        
        # Saving vectorized data
        with open(dataframe_path.replace('csv', 'pkl'), 'wb') as handle:
            pickle.dump(tfs, handle)
        handle.close()
        
    tfs_and_emolex = tfs
    for dim in ["emolex_joy","emolex_sad","emolex_trust",\
                "emolex_disgust","emolex_anger","emolex_fear",\
                "emolex_anticipation","emolex_surprise",\
                "emolex_positive","emolex_negative"]:
        tfs_and_emolex = np.c_[tfs_and_emolex, dataframe[dim]]
                   
    try:
        normalizer = pickle.load(open('experiments/normalizer.pkl', 'rb'))
    except:
        print("Normalizer object not found... Please train your model...")
        return
    
    tfs_and_emolex = normalizer.transform(tfs_and_emolex)
        
    print("Testing 1 vs 1 classifiers...")        
    for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT', 'SUR')]:
        try:
            classifier = pickle.load(open('experiments/classifier_%s_vs_%s.pkl' % (emo[0], emo[1]), 'rb'))
        except:
            continue
        
        print('--> Testing %s vs %s...' % (emo[0], emo[1]))
        
        test_one_vs_one(classifier, dataframe, tfs, emo[0], emo[1], dataframe_path)
    
    print("Testing 1 vs 1 classifiers with Emolex attributes...")        
    for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('ANT', 'SUR')]:
        try:
            classifier = pickle.load(open('experiments/classifier_%s_vs_%s_emolex.pkl' % (emo[0], emo[1]), 'rb'))
        except:
            continue
         
        print('--> Testing %s vs %s...' % (emo[0], emo[1]))
         
        test_one_vs_one(classifier, dataframe, tfs_and_emolex, emo[0], emo[1], dataframe_path, label='emolex')    
        
    print("Testing 1 vs all classifiers...")
    for emo in ['JOY', 'SAD', 'TRU', 'DIS', 'ANG', 'FEA', 'ANT', 'SUR']:
        try:
            classifier = pickle.load(open('experiments/classifier_%s_vs_all.pkl' % emo, 'rb'))
        except:
            continue
        
        print('---> Testing %s vs all...' % emo)
        
        test_one_vs_all(classifier, dataframe, tfs, emo, dataframe_path)
        
        
    print("Testing 1 vs all classifiers with Emolex attributes...")
    for emo in ['JOY', 'SAD', 'TRU', 'DIS', 'ANG', 'FEA', 'ANT', 'SUR']:
        try:
            classifier = pickle.load(open('experiments/classifier_%s_vs_all_emolex.pkl' % emo, 'rb'))
        except:
            continue
         
        print('---> Testing %s vs all with Emolex attributes...' % emo)
         
        test_one_vs_all(classifier, dataframe, tfs_and_emolex, emo, dataframe_path)
        
        
    
    
test_emo_benchmark('experiments/tweets_stocks_emolex.csv')